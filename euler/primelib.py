import math

# Slow but requires less memory
def find_primes(upper_limit):
    primes = []
    if upper_limit > 2:
        primes.append(2)
    if upper_limit > 3:
        primes.append(3)
    if upper_limit < 5:
        return primes
    
    n = 5
    while n < upper_limit:
        is_prime = True
        for i in primes:
            if n % i == 0:
                is_prime = False
                break
        if is_prime:
            primes.append(n)
        n += 2

    return primes

# Preferable method
def prime_sieve(upper_limit):
    primes = []
    numbers = [0] * upper_limit
    
    n = 2
    while n < upper_limit:
        if numbers[n] == 0: 
            for i in range(2*n, upper_limit, n):
                numbers[i] = 1
        n = n + 1

    for i in range(2, upper_limit):
        if numbers[i] == 0:
            primes.append(i);

    return primes

# Slow, unknown upper bound
def find_nth_prime(nth):
    if nth == 1:
        return 2
    if nth == 2: 
        return 3

    primes = []

    size = 2*nth
    while True:
        primes = prime_sieve(size)
        if len(primes) < nth:
            size *= 2
        else:
            break

    return primes[nth-1]                

def prime_factor(number):
    n = number
    x = int(math.sqrt(n))
    x += 1
    primes = prime_sieve(x)
    powers = [0] * len(primes)
    total = 1
    length = 0

    for i, prime in enumerate(primes):
        while n % prime == 0:
            powers[i] += 1
            total *= prime
            n //= prime
        # This will occur if number is an exponent
        # of a prime (2**3, 5**4, etc)
        if n == 1 or total*prime == number:
            powers[i] = 1
            length = i
            break

    return primes[:length+1], powers[:length+1]

