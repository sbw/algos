#include "euler.h"

unsigned long long p15_lattice_paths(size_t rows, size_t cols)
{
    std::vector<std::vector<unsigned long long>> paths(rows+1, 
                                      std::vector<unsigned long long>(cols+1));

    for (size_t i = 1; i <= rows; ++i)
        paths[i][0] = 1;

    for (size_t i = 1; i <= cols; ++i)
        paths[0][i] = 1;

    for (size_t i = 1; i <= rows; ++i)
        for (size_t j = 1; j <= cols; ++j)
            paths[i][j] = paths[i-1][j] + paths[i][j-1];
             
    return paths[rows][cols];
}

unsigned p18_and_67_max_path_sum_i(std::vector<std::vector<unsigned>>& triangle)
{
    if (triangle.empty()) return 0;
    
    for (size_t i = 0; i < triangle.size(); ++i) {
        for (size_t j = 0; j < triangle[i].size(); ++j)
            std::cout << triangle[i][j] << ' ';
        std::cout << std::endl;

    }

    std::vector<std::vector<unsigned>> path_sum;

    for (size_t i = 0; i < triangle.size(); ++i)
        path_sum.push_back(std::vector<unsigned>(i+1));

    path_sum[0][0] = triangle[0][0];

    for (size_t i = 1; i < path_sum.size(); ++i) {
        for (size_t j = 0; j < path_sum[i].size(); ++j) {
            if (j == 0)
            {
                path_sum[i][0] = triangle[i][0] + path_sum[i-1][0];    
                continue;
            }
            if (j == i)
            {
                path_sum[i][j] = triangle[i][j] + path_sum[i-1][j-1];
                continue;
            }
            path_sum[i][j] = triangle[i][j] +
                          std::max(path_sum[i-1][j-1], path_sum[i-1][j]);
        }
    }

    return *std::max_element(path_sum[triangle.size()-1].begin(),
                             path_sum[triangle.size()-1].end());
}

int p31_coin_sums(const std::vector<int>& coins, int sum)
{    
    if (coins.empty()) return 0;
    
    std::vector<int> ways(sum+1, 1);
    
    for (int i = 1; i < coins.size(); ++i) 
        for (int j = 0; j <= sum; ++j) {
            if (j - coins[i] < 0) continue;
            ways[j] += ways[j-coins[i]];
        }
    
    return ways[sum];
}

std::vector<int> dfs(std::unordered_map<int, std::unordered_set<int>>& graph, 
                                                                   int origin)
{
    std::vector<int> nodes;
    std::stack<int> stack;
    std::unordered_set<int> gray;
    std::unordered_set<int> black;

    stack.push(origin);
    gray.insert(origin);

    while (!stack.empty()) {
        int current = stack.top();
        int len = stack.size();

        for (auto& neighbor : graph[current]) {
            if (gray.find(neighbor) == gray.end()) 
            {
               gray.insert(neighbor);
               stack.push(neighbor);
               break;    
            }
        }

        if (stack.size() == len) 
        {
            nodes.push_back(current);
            black.insert(current);
            stack.pop();
        }
    }

    return nodes;
}


int p79_passcode_derivation(const std::vector<int>& input)
{
    std::unordered_set<int> incoming;
    std::unordered_set<int> no_incoming;
    std::unordered_map<int, std::unordered_set<int>> graph;

    for (auto& pc : input) {
        int code = pc;  
        for (size_t i = 0; i < 2; ++i) {
            int prev = code % 10;
            code /= 10;
            graph[code % 10].insert(prev);
            incoming.insert(prev);
            no_incoming.erase(prev);
        }
        if (incoming.find(code) == incoming.end())
            no_incoming.insert(code);
    }

    std::vector<int> results = dfs(graph, *no_incoming.begin()); 
    std::reverse(results.begin(), results.end());

    int passcode = 0;
    for (size_t i = 0; i < results.size(); ++i) 
        passcode = 10*passcode + results[i];

    return passcode;
}


int p81_path_sum_two_ways(const std::vector<std::vector<int> >& matrix, 
                                                                int n, int m)
{
    std::vector<std::vector<int> > s(matrix.size(), 
                                        std::vector<int>(matrix[0].size()));

    s[0][0] = matrix[0][0];
    for (size_t i = 1; i < s[0].size(); ++i)
        s[0][i] = matrix[0][i] + s[0][i-1];

    for (size_t i = 1; i < s.size(); ++i)
        s[i][0] = matrix[i][0] + s[i-1][0];

    for (size_t i = 1; i < s.size(); ++i)
        for (size_t j = 1; j < s[0].size(); ++j)
            s[i][j] = matrix[i][j] + std::min(s[i-1][j], s[i][j-1]);
    
    return s[n][m];
}

int p83_path_sum_four_ways(const std::vector<std::vector<int> >& matrix, 
                                                                int n, int m)
{
    if (matrix.empty()) return 0;
    
    size_t row_width = matrix.size();
    size_t col_width = matrix[0].size();
     
    int sum[row_width][col_width];
    for (size_t i = 0; i < row_width; ++i)
        for (size_t j = 0; j < col_width; ++j)
            sum[i][j] = std::numeric_limits<int>::max();   

    struct Node {
        Node(int i, int j, int& s) : x(i), y(j), sum(s) {}
        Node() {}    
        int x;
        int y;
        int sum;
        bool operator<(const Node& rhs) const { return sum > rhs.sum; }
    };

    std::unordered_set<int> visited;
    std::priority_queue<Node, std::vector<Node>> minheap;
    sum[0][0] = 131;
    minheap.push(Node(0, 0, sum[0][0]));
    visited.insert(0);

    while (!minheap.empty()) {
        Node node = minheap.top();
        minheap.pop();

        for (int i = -1; i < 2; ++i)
            for (int j = -1; j < 2; ++j) {
                if (i == j || -1*i == j) continue;
                int row = node.x+i;
                int col = node.y+j;
                if (row < 0 || row >= row_width) continue;
                if (col < 0 || col >= col_width) continue;

                sum[row][col] = 
                    std::min(sum[row][col],
                             matrix[row][col] + sum[node.x][node.y]);
                
                if (visited.find(row*col_width + col) == visited.end()) {
                    visited.insert(row*col_width + col);
                    minheap.push(Node(row, col, sum[row][col]));
                }
            }
    }

    return sum[n][m];
}

int p106_minimal_network(const std::vector<std::vector<int> >& matrix)
{
    if (matrix.empty()) return 0;

    struct Neighbor {
        Neighbor(int k, int w) : key(k), weight(w) {}
        Neighbor() {}
        int key;
        int weight;
    };

    struct Node {
        Node(int k) : key(k) {} 
        Node() {}
        int key;
        std::vector<Neighbor> neighbors;
    };
 
    struct Edge {
        Edge(int f, int t, int w) : from(t), to(t), weight(w) {}
        Edge() {}
        int from;
        int to;
        int weight; 
        bool operator<(const Edge& rhs) const { return weight > rhs.weight; }
    };

    std::unordered_map<int, Node> graph;

    for (size_t i = 0; i < matrix.size(); ++i) {
        graph[i] = Node(i);
        for (size_t j = 0; j < matrix[0].size(); ++j) { 
            if (matrix[i][j] != -1)
                graph[i].neighbors.push_back(Neighbor(j, matrix[i][j]));
        }
    }

    int sum = 0;
    std::priority_queue<Edge, std::vector<Edge>> minheap;
    std::unordered_set<int> visited;

    for (auto& neighbor : graph[0].neighbors) {
        minheap.push(Edge(0, neighbor.key, neighbor.weight));
    }
    visited.insert(0);

    while (!minheap.empty()) {
        Edge current = minheap.top();
        minheap.pop();

        if (visited.find(current.to) == visited.end())
            visited.insert(current.to);
        else
            continue;
    
        for (auto& neighbor : graph[current.to].neighbors) 
            minheap.push(Edge(current.to, neighbor.key, neighbor.weight)); 

        sum += current.weight;
    }

    return sum;
}
