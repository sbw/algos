# By starting at the top of the triangle below and moving to adjacent numbers 
# on the row below, the maximum total from top to bottom is 23.
# 
# 3
# 7 4
# 2 4 6
# 8 5 9 3

# That is, 3 + 7 + 4 + 9 = 23.
# Find the maximum total from top to bottom of the 100 row triangle.

def max_path_sum(filename):
    with open(filename) as f:
        prev_row = [ int(f.readline().rstrip()) ]
        
        for line in f:
            row = [ int(x) for x in line.split() ]
            # sides of triangle only have one choice
            row[0] += prev_row[0]
            row[-1] += prev_row[-1]  
            # choose the max between the parents
            for i in range(1, len(row)-1):
                row[i] += max(prev_row[i-1], prev_row[i])
            prev_row = row    

    return max(prev_row) 


def main():
    filename = 'p067_triangle.txt'
    try:
        print(max_path_sum(filename))
    except Exception as e:
        print(e)

    print('Exiting in controlled fashion')

#--call main--#
main()
