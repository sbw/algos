#Coin partitions
# Problem 78
#
# Let p(n) represent the number of different ways in which n coins can be separated into piles. For example, five coins can be separated into piles in exactly seven different ways, so p(5)=7.
# OOOOO
# OOOO   O
# OOO   OO
# OOO   O   O
# OO   OO   O
# OO   O   O   O
# O   O   O   O   O
#
# Find the least value of n for which p(n) is divisible by one million

def different_ways(n):
    ways = (n+1) * [1]            
    
    for i in range(2, n):
        for k in range(i, n+1):
            ways[k] = ways[k] + ways[k-i] 
            if k == i and ways[k] % 1000000 == 0:
                print(i, ways[k])
    return ways[-1]

def main():
    n = 57500

    try:
        print(different_ways(n))        
    except Exception as e:
        print(e)
    print('Exiting in a controlled fashion.')


#--invoke main--#
main()
