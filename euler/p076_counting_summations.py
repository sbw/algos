# Counting summations
# Problem 76
#
# It is possible to write five as a sum in exactly six different ways:
#
# 4 + 1
# 3 + 2
# 3 + 1 + 1
# 2 + 2 + 1
# 2 + 1 + 1 + 1
# 1 + 1 + 1 + 1 + 1
#
# How many different ways can one hundred be written as a sum of at least two positive integers?

def different_ways(n):
    ways = (n+1) * [1]            
    
    for i in range(2, n):
	print(i)
        for k in range(i, n+1):
            ways[k] = ways[k] + ways[k-i] 
    return ways[-1]

def main():
    n = 100

    try:
        print(different_ways(n))        
    except Exception as e:
        print(e)
    print('Exiting in a controlled fashion.')


#--invoke main--#
main()
