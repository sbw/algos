# Longest Collatz sequence
# Problem 14
#
# The following iterative sequence is defined for the set of positive integers:
#
# n → n/2 (n is even)
# n → 3n + 1 (n is odd)
#
# Using the rule above and starting with 13, we generate the following sequence:
# 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
#
# It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. 
# Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
#
# Which starting number, under one million, produces the longest chain?
#
# NOTE: Once the chain starts the terms are allowed to go above one million.


def longest_collatz_sequence(upper):
    seq = {2: 2}
    length = 0    
    max_val = 2
    max_num = 2

    for i in range(2, upper):
        if i not in seq.keys():
            stack = []
            n = i
            while n not in seq.keys():
                stack.append(n)
                n = n // 2 if n % 2 == 0 else 3*n + 1
            
            length = seq[n]
            while stack:
                length += 1
                n = stack.pop()
                seq[n] = length
                if n < upper and length > max_val:
                    max_num = n
                    max_val = length
    return max_num, max_val

def main():
    print(longest_collatz_sequence(1000000))

#--invoke main--
main()
