# Counting Sundays
# Problem 19
#
# You are given the following information, but you may prefer to do some research for yourself.
#
#    1 Jan 1900 was a Monday.
#    Thirty days has September,
#    April, June and November.
#    All the rest have thirty-one,
#    Saving February alone,
#    Which has twenty-eight, rain or shine.
#    And on leap years, twenty-nine.
#    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

# How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

def is_leap_year(year):
    if year % 400 == 0: return True
    if year % 100 == 0: return False
    if year % 4 == 0: return True
    return False
    
def x_days_in_year(day_of_week, first_day_of_year, leap_year):
    x_days = set()
    num_days = 366 if leap_year else 365
    
    for day in range(num_days):
        if (day + first_day_of_year) % 7 == day_of_week:
            x_days.add(day)
    return x_days

def first_day_of_month(leap_year):
    first_of_month = set()
    days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if leap_year:
        days_in_month[1] += 1

    current = 0
    for month in days_in_month:
        first_of_month.add(current)
        current += month
    return first_of_month

def total_firsts_per_year(day_of_week, first_day_of_year, leap_year):
    total = 0
    x_days = x_days_in_year(day_of_week, first_day_of_year, leap_year)
    firsts = first_day_of_month(True) if leap_year else first_day_of_month(False)
    return len(x_days & firsts)
        
def total_days(day_of_week, lower, first_day, upper):
    total = 0
    leap_year = is_leap_year(lower-1)

    for year in range(lower, upper+1): 
        num_days = 366 if leap_year else 365
        first_day = (first_day + num_days) % 7        

        leap_year = is_leap_year(year)
        total += total_firsts_per_year(day_of_week, first_day, leap_year)
    return total

def main():
    print(total_days(0, 1901, 1, 2000))


#--invoke main--
main()
