#Lattice paths
#Problem 15
#
#Starting in the top left corner of a 2×2 grid, and only being able to move 
# to the right and down, there are exactly 6 routes to the bottom right corner.
# How many such routes are there through a 20×20 grid?

import pprint

def num_lattice_paths(n):
    matrix = [ [0] * (n+1) for i in range(n+1)]    
    
    for i in range(len(matrix[0])):
        matrix[0][i] = 1

    for row in matrix:
        row[0] = 1

    for i in range(1, len(matrix[0])):
        for j in range(1, len(matrix)):
            matrix[i][j] = matrix[i-1][j] + matrix[i][j-1]

    return matrix[n][n]

def main():
    n = 20
    ans = num_lattice_paths(n)
    print(ans)


#--invoke main
main()    
