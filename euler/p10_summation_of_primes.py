# Summation of primes
# Problem 10
#
# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
#
# Find the sum of all the primes below two million.

import primelib

def main():
    primes = primelib.prime_sieve(2000000)
    print(sum(primes))

main()
