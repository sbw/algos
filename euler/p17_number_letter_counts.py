# Number letter counts
# Problem 17
#
# If the numbers 1 to 5 are written out in words: one, two, three, four, five, 
# then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
#
# If all the numbers from 1 to 1000 (one thousand) inclusive were written out 
# in words, how many letters would be used? 

# TODO fix crappy code
# 

ones_map = { 
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
    5: 'five',
    6: 'six',
    7: 'seven',
    8: 'eight',
    9: 'nine'
}

teens_map = {
    10: 'ten',
    11: 'eleven',
    12: 'twelve',
    13: 'thirteen',
    14: 'fourteen',
    15: 'fifteen',
    16: 'sixteen',
    17: 'seventeen',
    18: 'eighteen',
    19: 'nineteen'
}

twenty_up_map = {
    20: 'twenty',
    30: 'thirty',
    40: 'forty',
    50: 'fifty',
    60: 'sixty',
    70: 'seventy',
    80: 'eighty',
    90: 'ninety'
}

ones_len = { ones_map[x]: len(ones_map[x]) for x in ones_map }
teens_len = { teens_map[x]: len(teens_map[x]) for x in teens_map }
twenty_up_len = { twenty_up_map[x]: len(twenty_up_map[x]) for x in twenty_up_map }

def convert_number(n):
    s = ''
    pos = []
    length = 0

    # handle teens case
    x = n % 100
    if 10 <= x <= 19:
        s = teens_map[x]
        length = teens_len[s]
        n = n // 100
        pos.append(-1)
        pos.append(-1)

    while n > 0:
        r = n % 10
        n = n // 10
        pos.append(r)
    
    while len(pos) != 4:
        pos.append(0)
        
    if pos[0] > 0:
        tmp = ones_map[pos[0]]
        length += ones_len[tmp]
        s = s + ones_map[pos[0]]
    if pos[1] > 0:
        tmp = twenty_up_map[pos[1]*10]
        length += twenty_up_len[tmp]
        s = tmp + ' ' + s
    if pos[2] > 0:
        if pos[0] or pos[1] != 0:
            tmp = ones_map[pos[2]]
            length += ones_len[tmp]
            length += 10
            s = ones_map[pos[2]] + ' hundred and ' + s
        else:
            tmp = ones_map[pos[2]]
            length += ones_len[tmp]
            length += 7
            s = tmp + ' hundred'
    if pos[3] > 0:
        tmp = ones_map[pos[3]]
        length += ones_len[tmp]
        length += 8
        s = tmp + ' thousand ' + s    

    return length, s
    
def main():
    total = 0
    for i in range(1,1001):
        length, s = convert_number(i)
        total += length

    print(total) 
#
main()
