#include "euler.h"
#include "gtest/gtest.h"

namespace {

class EulerTest : public ::testing::Test {
protected:
    EulerTest() {}
    virtual ~EulerTest() {}

    virtual void SetUp() 
    {
    }

    virtual void TearDown() 
    {
    }

};


TEST_F(EulerTest, p15_lattice_paths_demo1)
{
    ASSERT_EQ(70, p15_lattice_paths(4, 4));
}

TEST_F(EulerTest, p15_lattice_paths_demo2)
{
    ASSERT_EQ(35, p15_lattice_paths(3, 4));
}

TEST_F(EulerTest, p15_lattice_paths)
{
    ASSERT_EQ(137846528820, p15_lattice_paths(20, 20));
}

TEST_F(EulerTest, p18_maximum_path_sum_i_demo)
{
    std::vector<std::vector<unsigned>> triangle(4);
    triangle[0].push_back(3);
    triangle[1].push_back(7);
    triangle[1].push_back(4);
    triangle[2].push_back(2);
    triangle[2].push_back(4);
    triangle[2].push_back(6);
    triangle[3].push_back(8);
    triangle[3].push_back(5);
    triangle[3].push_back(9);
    triangle[3].push_back(3);
    
    ASSERT_EQ(23, p18_max_path_sum_i(triangle));
}

TEST_F(EulerTest, p18_maximum_path_sum_i)
{
    std::vector<std::vector<unsigned>> triangle(4);
    
    ASSERT_EQ(0, p18_max_path_sum_i(triangle));
}

TEST_F(EulerTest, p31_coin_sums)
{
    std::vector<int> coins = { 1, 2, 5, 10, 20, 50, 100, 200 };
    
    ASSERT_EQ(73682, p31_coin_sums(coins, 200));

}

TEST_F(EulerTest, p67_maximum_path_sum_ii)
{
    std::vector<std::vector<unsigned>> triangle(100);
    
    ASSERT_EQ(0, p18_max_path_sum_i(triangle));
}

TEST_F(EulerTest, p79_password_derivation)
{
    std::vector<int> input;

    std::ifstream fin;
    fin.open("p079_keylog.txt");
    int temp = 0;     
    while (fin >> temp) 
        input.push_back(temp);
    fin.close();

    ASSERT_EQ(73162890, p79_passcode_derivation(input));
}

TEST_F(EulerTest, p81_path_sum_two_ways_demo)
{
    std::vector<std::vector<int> > matrix(5, std::vector<int>(5));
    matrix[0][0] = 131;
    matrix[0][1] = 673;
    matrix[0][2] = 234;
    matrix[0][3] = 103;
    matrix[0][4] = 18;
     
    matrix[1][0] = 201;
    matrix[1][1] = 96;
    matrix[1][2] = 342;
    matrix[1][3] = 965;
    matrix[1][4] = 150;
     
    matrix[2][0] = 630;
    matrix[2][1] = 803;
    matrix[2][2] = 746;
    matrix[2][3] = 422;
    matrix[2][4] = 111;
    
    matrix[3][0] = 537;
    matrix[3][1] = 699;
    matrix[3][2] = 497;
    matrix[3][3] = 121;
    matrix[3][4] = 956;
    
    matrix[4][0] = 805;
    matrix[4][1] = 732;
    matrix[4][2] = 524;
    matrix[4][3] = 37;
    matrix[4][4] = 331;

    ASSERT_EQ(2427, p81_path_sum_two_ways(matrix, matrix.size()-1, 
                                                        matrix[0].size()-1));
}

TEST_F(EulerTest, p81_path_sum_two_ways_demo)
{
    std::vector<std::vector<int> > matrix(5, std::vector<int>(5));

    ASSERT_EQ(2427, p81_path_sum_two_ways(matrix, matrix.size()-1, 
                                                        matrix[0].size()-1));
}

TEST_F(EulerTest, p83_path_sum_four_ways_demo)
{
    std::vector<std::vector<int> > matrix(5, std::vector<int>(5));
    matrix[0][0] = 131;
    matrix[0][1] = 673;
    matrix[0][2] = 234;
    matrix[0][3] = 103;
    matrix[0][4] = 18;
     
    matrix[1][0] = 201;
    matrix[1][1] = 96;
    matrix[1][2] = 342;
    matrix[1][3] = 965;
    matrix[1][4] = 150;
     
    matrix[2][0] = 630;
    matrix[2][1] = 803;
    matrix[2][2] = 746;
    matrix[2][3] = 422;
    matrix[2][4] = 111;
    
    matrix[3][0] = 537;
    matrix[3][1] = 699;
    matrix[3][2] = 497;
    matrix[3][3] = 121;
    matrix[3][4] = 956;
    
    matrix[4][0] = 805;
    matrix[4][1] = 732;
    matrix[4][2] = 524;
    matrix[4][3] = 37;
    matrix[4][4] = 331;

    ASSERT_EQ(2297, p83_path_sum_four_ways(matrix, matrix.size()-1,                                                     matrix[0].size()-1));
}

TEST_F(EulerTest, p83_path_sum_four_ways_demo)
{
    std::vector<std::vector<int> > matrix(5, std::vector<int>(5));

    ASSERT_EQ(2297, p83_path_sum_four_ways(matrix, matrix.size()-1,                                                     matrix[0].size()-1));
}

TEST_F(EulerTest, p106_minimal_network_demo)
{
    std::vector<std::vector<int> > matrix(7, std::vector<int>(7));
    matrix[0][0] = -1;
    matrix[0][1] = 16;
    matrix[0][2] = 12;
    matrix[0][3] = 21;
    matrix[0][4] = -1;
    matrix[0][5] = -1;
    matrix[0][6] = -1;
    
    matrix[1][0] = 16;
    matrix[1][1] = -1;
    matrix[1][2] = -1;
    matrix[1][3] = 17;
    matrix[1][4] = 20;
    matrix[1][5] = -1;
    matrix[1][6] = -1;

    matrix[2][0] = 12;
    matrix[2][1] = -1;
    matrix[2][2] = -1;
    matrix[2][3] = 28;
    matrix[2][4] = -1;
    matrix[2][5] = 31;
    matrix[2][6] = -1;

    matrix[3][0] = 21;
    matrix[3][1] = 17;
    matrix[3][2] = 28;
    matrix[3][3] = -1;
    matrix[3][4] = 18;
    matrix[3][5] = 19;
    matrix[3][6] = 23;
    
    matrix[4][0] = -1;
    matrix[4][1] = 20;
    matrix[4][2] = -1;
    matrix[4][3] = 18;
    matrix[4][4] = -1;
    matrix[4][5] = -1;
    matrix[4][6] = 11;

    matrix[5][0] = -1;
    matrix[5][1] = -1;
    matrix[5][2] = 31;
    matrix[5][3] = 19;
    matrix[5][4] = -1;
    matrix[5][5] = -1;
    matrix[5][6] = 27;

    matrix[6][0] = -1;
    matrix[6][1] = -1;
    matrix[6][2] = -1;
    matrix[6][3] = 23;
    matrix[6][4] = 11;
    matrix[6][5] = 27;
    matrix[6][6] = -1;

    ASSERT_EQ(93, p106_minimal_network(matrix));
}

TEST_F(EulerTest, p106_minimal_network)
{
    std::vector<std::vector<int> > matrix(7, std::vector<int>(7));

    ASSERT_EQ(0, p106_minimal_network(matrix));
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
