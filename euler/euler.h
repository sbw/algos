#include <memory>
#include <queue>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <fstream>
#include <iostream>
#include <limits>
#include <queue>
#include <algorithm>

void p002_even_fibonacci_numbers();

void p003_largest_prime_factor();

void p005_smallest_multiple();

void p007_10001st_prime();

void p008_largest_product_in_a_series();

void p010_summation_of_primes();

void p014_longest_collatz_sequence();

unsigned long long p015_lattice_paths(size_t rows, size_t cols);

void p017_number_letter_counts();

unsigned p018_and_067_max_path_sum(std::vector<std::vector<unsigned>>& triangle);

void p019_counting_sundays();

void p022_names();

void p028_spiral_diagonals();

int p031_coin_sums(const std::vector<int>& coins, int sum);

void p076_counting_summations();

void p077_prime_summations();

void p078_coin_partitions();

int p079_passcode_derivation(const std::vector<int>& input);

int p081_path_sum_two_ways(const std::vector<std::vector<int> >& matrix,
                                                                int n, int m);

int p082_path_sum_two_ways(const std::vector<std::vector<int> >& matrix,
                                                                int n, int m);

int p083_path_sum_four_ways(const std::vector<std::vector<int> >& matrix,
                                                                int n, int m);

int p107_minimal_network(const std::vector<std::vector<int> >& matrix);
