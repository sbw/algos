# Smallest multiple
# Problem 5
#
# 2520 is the smallest number that can be divided by each of the numbers 
# from 1 to 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible by all 
# of the numbers from 1 to 20?

import primelib

def main():
    number = 20
    primes = primelib.prime_sieve(number+1)
    powers = []
    
    for p in primes:
        n = number
        powers.append(0)
        while n > 1:
            n //= p
            if n > 0:
                powers[-1] += 1
        
    total = 1
    for b, p in zip(primes, powers):
        while p > 0:
            total *= b
            p -= 1

    print(total)

main()
