# Coin sums
# Problem 31
#
# In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
#
#    1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
#
# It is possible to make £2 in the following way:
#
#    1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
#
# How many different ways can £2 be made using any number of coins?

def different_ways(n, coins):
    ways = (n+1) * [0]
    ways[0] = 1
    for i in range(0, n+1):
        if i % coins[0] == 0:
            ways[i] = 1

    for coin in coins[1:]:    
        for i in range(coin, n+1):
            ways[i] += ways[i-coin]

    return ways[-1]

def main():
    # note coins need to be passed in already sorted
    print(different_ways(200, (1, 2, 5, 10, 20, 50, 100, 200)))

#--main--
main()
