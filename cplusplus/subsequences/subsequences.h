#include <vector>
#include <set>
#include <algorithm>
#include <numeric>
#include <limits>

// ----------------------- Subsequence Problems ---------------------

/*  Find the missing value.
    Suppose you are given a set of size n-1. The set is unordered,
    and it is missing exactly one value from the sequence of 1 to n.
    Find the missing element. */
unsigned find_missing_value_by_sort(const std::set<unsigned>& values);

unsigned find_missing_value_by_tally(const std::set<unsigned>& values);

unsigned find_missing_value_by_summation(const std::set<unsigned>& values);


/*  Find two members that sum to k
    Suppose we have a set of integers, and we are given a target k.
    How can we efficiently find two values in the set that sum to k. */
struct Tuple {
    Tuple() : w(0), x(0), y(0), z(0) {}
/*    Tuple& operator=(const Tuple& rhs)
    { 
         w = rhs.w; x = rhs.x; y = rhs.y; z = rhs.z;
    }
*/
    int w;
    int x;
    int y;
    int z;
};

Tuple find_sum_to_k_by_sort(const std::set<int>& values, int k);
Tuple find_sum_to_k_by_linear(std::set<int> values, int k);

/*  Find two members with sum closet to zero
    Suppose we have a set S of integers, and we are given a target k.
    How can we efficiently find two values in the set whose sum is
    closest to zero. */
Tuple find_sum_closest_to_0(std::set<int>& values);

/*  Find two members that sum to a third
    Suppose we have a set of integers, and we are given a target k.
    How can we efficiently find two values in the set whose sum is
    a third */
Tuple find_sum_to_c(std::set<int>& values);

/*  Find the maximum difference
    Suppose we have a vector S of integers.
    Now considering finding a pair i,j of indices with i < j that 
    maximize the difference of s[j] - s[i] 
    NOTE i < j so max and min of S are not enough */
int find_max_difference(const std::vector<int> values);

/*  Pivot closest to equal sums
    Suppose we have a vector S of integers.
    Find a pivot element such that the difference between each side is
    minimized */
size_t partition_index(std::vector<int>& vec);


/*  Maximum sub-array
    Suppose we have a vector S of integers.
    Find the maximum of the sum of any sub-array
    minimized */
int max_subarray(const std::vector<int>& vec, std::vector<int>& result);

// Future
typedef std::vector<std::vector<int> > matrix;

int max_submatrix(matrix m);

int max_submatrix_cubic(matrix m);

bool subseq(const std::string& text, const std::string& sub);

