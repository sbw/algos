#include "subsequences.h"
#include <iostream>

// ----------------------- Subsequence Problems ---------------------

/*  Find the missing value.
    Suppose you are given a set of size n-1. The set is unordered,
    and it is missing exactly one value from the sequence of 1 to n.
    Find the missing element. */
unsigned find_missing_value_by_sort(const std::set<unsigned>& values)
{
    if (values.empty()) return 0;
 
    std::vector<unsigned> numbers(values.begin(), values.end());
    std::sort(numbers.begin(), numbers.end());
    for (size_t i = 0; i < numbers.size(); ++i)
        if (numbers[i] != i+1)
            return i+1;
    return 0;
}

unsigned find_missing_value_by_tally(const std::set<unsigned>& values)
{
    if (values.empty()) return 0;
    
    std::vector<bool> is_present(values.size()+1);
    
    std::set<unsigned>::const_iterator iter = values.begin();
    for (; iter != values.end(); ++iter)
        is_present[*iter-1] = true;

    for (size_t i = 0; i < is_present.size(); ++i)
        if (!is_present[i]) return i+1;    

    return 0;
}

unsigned find_missing_value_by_summation(const std::set<unsigned>& values)
{
    if (values.empty()) return 0;

    unsigned n = values.size() + 1;
    unsigned ideal_sum = (n*(n+1))/2;
    unsigned real_sum = std::accumulate(values.begin(), values.end(), 0);
    
    return ideal_sum - real_sum;
}

/*  Find two members that sum to k
    Suppose we have a set of integers, and we are given a target k.
    How can we efficiently find two values in the set that sum to k. */

Tuple find_sum_to_k_by_sort(const std::set<int>& values, int k)
{
    Tuple result;
    if (values.size() < 2) return result;

    std::set<int> diff;
    std::vector<int> intersect(2);

    std::set<int>::const_iterator iter = values.begin();
    for (; iter != values.end(); ++iter) 
        diff.insert(k - *iter);
    
    // note this requires a sorted collection, sets in STL\
    // are sorted by default
    std::set_intersection(values.begin(), values.end(), diff.begin(), 
                            diff.end(), intersect.begin());
    result.x = intersect[0];
    result.y = intersect[1]; 
    
    return result;
}

Tuple find_sum_to_k_by_linear(std::set<int> values, int k)
{
    Tuple result;
    if (values.size() < 2) return result;
    
    // note this requires a sorted collection, STL sets
    // are sorted by default
   
    std::vector<int> sorted(values.begin(), values.end());
    
    size_t lo = 0;
    size_t hi = sorted.size()-1; 
   
    while (lo < hi) {
        int sum = sorted[lo] + sorted[hi];
        if (sum == k) 
        {
            result.x = sorted[lo];
            result.y = sorted[hi];
            break;
        }

        if (sum > k) 
            --hi;
        else
            ++lo;
    }

    return result;
}

/*  Find two members with sum closet to zero
    Suppose we have a set S of integers.
    How can we efficiently find two values in the set whose sum is
    closest to zero. */
Tuple find_sum_closest_to_0(std::set<int>& values)
{
    Tuple result;
    if (values.size() < 2) return result;

    std::vector<int> sorted(values.begin(), values.end());
    size_t lo = 0;
    size_t hi = sorted.size()-1;

    while (sorted[lo] < 0 && lo < hi) ++lo; 
    lo = (lo > 1) ? lo-2 : 0;
    
    for (int i = 0, 
        min = std::numeric_limits<int>::max(); i < 3 && lo < hi; ++i) {
        int sum = std::abs(sorted[lo+i] + sorted[lo+i+1]);
        if (sum < min) 
        {
            min = sum;
            result.x = sorted[lo+i];
            result.y = sorted[lo+i+1];
        }
    }
        
    return result;
}

/*  Find the maximum difference
    Suppose we have a vector S of integers.
    Now considering finding a pair i,j of indices with i < j that 
    maximize the difference of s[j] - s[i] 
    NOTE i < j so max and min of S are not enough */
int find_max_difference(const std::vector<int> values)
{
    if (values.size() < 2) return 0;

    int min = values[0];
    int max_diff = std::numeric_limits<int>::min();
    for (size_t k = 1; k < values.size(); k++) {
        int current_diff = values[k] - min;
        if (current_diff < 0) 
                min = values[k];
        else if (max_diff < current_diff)
                max_diff = current_diff;
    }

    return max_diff;
}

/*  Pivot closest to equal sums
    Suppose we have a vector S of integers.
    Find a pivot element such that the difference between each side is
    minimized */
size_t partition_index(std::vector<int>& vec)
{
    int total = std::accumulate(vec.begin(), vec.end(), 0);
    int current_sum = 0;

    size_t index = 0;
    int min = std::abs(total);

    for (size_t i = 0; i < vec.size(); ++i) {
        current_sum += vec[i];
        int difference = std::abs(total - 2*current_sum);
        if (difference < min) 
        {
            min = difference;
            index = i;
        }
    }

    return index;
}


/*  Maximum sub-array
    Suppose we have a vector S of integers.
    Find the maximum of the sum of any sub-array
    minimized */
int max_subarray(const std::vector<int>& vec, std::vector<int>& result)
{
    if (vec.empty()) return 0;

    int sum = 0;
    int max = std::numeric_limits<int>::min();
    int max_begin = 0;
    int max_end = 0;
    bool first = true;

    for (size_t i = 0; i < vec.size(); ++i) {
        sum += vec[i];
        if (max < sum)
        {
            max = sum;
            if (first)
            {
                max_begin = i;
                first = false;
            }
            else
            {
                max_end = i;
            }
        }
        
        if (sum < 0) 
        {
            sum = 0;
            first = true;
        }

    }

    result.assign(vec.begin()+max_begin, vec.begin()+max_end+1);
    return max;
}

// Future
typedef std::vector<std::vector<int> > matrix;

int max_submatrix(matrix m);

int max_submatrix_cubic(matrix m);


