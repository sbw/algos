#include "subsequences.h"
#include "gtest/gtest.h"

namespace {

class SubsequencesTest : public ::testing::Test {
protected:
    SubsequencesTest() {}
    virtual ~SubsequencesTest() {}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(SubsequencesTest, find_missing_value_by_sort)
{
    std::set<unsigned> numbers = { 7, 3, 9, 2, 4, 1, 6, 5, 10 };
    ASSERT_EQ(8, find_missing_value_by_sort(numbers));
}

TEST_F(SubsequencesTest, find_missing_value_by_tally)
{
    std::set<unsigned> numbers = { 7, 3, 9, 2, 4, 1, 6, 5, 10 };
    ASSERT_EQ(8, find_missing_value_by_tally(numbers));
}

TEST_F(SubsequencesTest, find_missing_value_by_summation)
{
    std::set<unsigned> numbers = { 7, 3, 9, 2, 4, 1, 6, 5, 10 };
    ASSERT_EQ(8, find_missing_value_by_summation(numbers));
}

TEST_F(SubsequencesTest, find_sum_to_k_by_sort)
{
    std::set<int> numbers = { 13, 3, 7, 6, 4, 15, 1 };
    
    Tuple ans = find_sum_to_k_by_sort(numbers, 11);
    
    ASSERT_EQ(ans.w, 0);
    ASSERT_EQ(ans.x, 4);
    ASSERT_EQ(ans.y, 7);
    ASSERT_EQ(ans.z, 0);
}

TEST_F(SubsequencesTest, find_sum_to_k_by_linear)
{
    std::set<int> numbers = { 13, 3, 7, 6, 4, 15, 1 };
    
    Tuple ans = find_sum_to_k_by_linear(numbers, 11);
   
    ASSERT_EQ(ans.w, 0);
    ASSERT_EQ(ans.x, 4);
    ASSERT_EQ(ans.y, 7);
    ASSERT_EQ(ans.z, 0);
}

TEST_F(SubsequencesTest, find_sum_closest_to_0Positive)
{
    std::set<int> numbers = { 13, 3, 7, 6, 4, 15, 1 };
    
    Tuple ans = find_sum_closest_to_0(numbers);
   
    ASSERT_EQ(ans.w, 0);
    ASSERT_EQ(ans.x, 1);
    ASSERT_EQ(ans.y, 3);
    ASSERT_EQ(ans.z, 0);
}

TEST_F(SubsequencesTest, find_sum_closest_to_0Mix)
{
    std::set<int> numbers = { 13, 0, 7, 6, -4, 15, -1 };
    
    Tuple ans = find_sum_closest_to_0(numbers);
    
    ASSERT_EQ(ans.w, 0);
    ASSERT_EQ(ans.x, -1);
    ASSERT_EQ(ans.y, 0);
    ASSERT_EQ(ans.z, 0);
}

TEST_F(SubsequencesTest, find_max_difference)
{
    std::vector<int> numbers = { 13, -2, 7, 6, -4, 15, -1 };
    ASSERT_EQ(19, find_max_difference(numbers));
}

TEST_F(SubsequencesTest, partition_index)
{
    std::vector<int> numbers = 
        { -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 7, 6, 5 };
    ASSERT_EQ(11, partition_index(numbers));
}

TEST_F(SubsequencesTest, max_subarray)
{
    std::vector<int> numbers = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
    std::vector<int> expected = { 4, -1, 2, 1 };
    std::vector<int> results;
    ASSERT_EQ(6, max_subarray(numbers, expected));
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
