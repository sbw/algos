#include "fibonacci_and_primes.h"
#include <iostream>

unsigned fib_naive(unsigned n)
{
   if (n == 0) return 0;
   if (n == 1) return 1;
   
   return fib_naive(n-1) + fib_naive(n-2); 
}

unsigned fib_memo(unsigned n);

unsigned fib_iter(unsigned n)
{
    if (n == 0) return 0;
    if (n == 1) return 1;

    unsigned cur = 1;
    unsigned prev = 0;

    for (size_t i = 2; i <= n; ++i) {
        cur = cur + prev;    
        prev = cur - prev;
        //std::cout << cur << '\t' << i << std::endl;
    }

    return cur;
}

unsigned fib_tail_recursion(unsigned n, unsigned cur, unsigned prev)
{
    if (n == 1) return cur;
    return fib_tail_recursion(n-1, cur + prev, cur);
}

unsigned fib(unsigned n)
{
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fib_tail_recursion(n, 1, 0);
}

bool is_prime_naive(unsigned n);

bool is_prime_sieve(unsigned n);

bool is_prime_sieve_heap(unsigned n);

bool is_prime_miller_rabin(unsigned n);


