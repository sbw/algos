#include "fibonacci_and_primes.h"
#include "gtest/gtest.h"

namespace {

class FibonacciAndPrimesTest : public ::testing::Test {
protected:
    FibonacciAndPrimesTest() {}
    virtual ~FibonacciAndPrimesTest() {}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(FibonacciAndPrimesTest, fibonacci_naive10)
{
    ASSERT_EQ(55, fib_naive(10));
}

TEST_F(FibonacciAndPrimesTest, fibonacci_naive12)
{
    ASSERT_EQ(144, fib_naive(12));
}

TEST_F(FibonacciAndPrimesTest, fibonacci_iter10)
{
    ASSERT_EQ(55, fib_iter(10));
}

TEST_F(FibonacciAndPrimesTest, fibonacci_iter12)
{
    ASSERT_EQ(144, fib_iter(12));
}

TEST_F(FibonacciAndPrimesTest, fib10)
{
    ASSERT_EQ(55, fib(10));
}

TEST_F(FibonacciAndPrimesTest, fib12)
{
    ASSERT_EQ(144, fib(12));
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
