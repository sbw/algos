
int pow_naive(int, unsigned y);

int pow_recursive(int x, unsigned y);

int pow_iter(int x, unsigned y);

unsigned sqrt(unsigned x);

double sqrt(double x);

double sqrt_newton(double x);


