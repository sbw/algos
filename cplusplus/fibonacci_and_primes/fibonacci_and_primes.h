#include <cstddef>

unsigned fib_naive(unsigned n);

unsigned fib_memo(unsigned n);

unsigned fib_iter(unsigned n);

unsigned fib(unsigned n);

unsigned fib_tail_recursion(unsigned n, unsigned fib0, unsigned fib1);

bool is_prime_naive(unsigned n);

bool is_prime_sieve(unsigned n);

bool is_prime_sieve_heap(unsigned n);

bool is_prime_miller_rabin(unsigned n);


