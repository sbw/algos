#include "binary_search.h"
#include "gtest/gtest.h"

namespace {

class BinarySearchTest : public ::testing::Test {
protected:
    BinarySearchTest() {}
    virtual ~BinarySearchTest() {}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(BinarySearchTest, NotFoundLow)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(-1, find(vec, -11));
}

TEST_F(BinarySearchTest, NotFoundHigh)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(-1, find(vec, 16));
}

TEST_F(BinarySearchTest, NotFoundMid)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(-1, find(vec, 4));
}

TEST_F(BinarySearchTest, FoundLow)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(0, find(vec, -10));
}

TEST_F(BinarySearchTest, FoundHigh)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(10, find(vec, 15));
}

TEST_F(BinarySearchTest, FoundMid)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(6, find(vec, 7));
}

TEST_F(BinarySearchTest, NotFoundLowRec)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(-1, find_rec(vec, -11));
}

TEST_F(BinarySearchTest, NotFoundHighRec)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(-1, find_rec(vec, 16));
}

TEST_F(BinarySearchTest, NotFoundMidRec)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(-1, find_rec(vec, 4));
}

TEST_F(BinarySearchTest, FoundLowRec)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(0, find_rec(vec, -10));
}

TEST_F(BinarySearchTest, FoundHighRec)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(10, find_rec(vec, 15));
}

TEST_F(BinarySearchTest, FoundMidRec)
{
    int temp[] = { -10, -5, 0, 2, 5, 6, 7, 9, 10, 12, 15 };
    std::vector<int> vec(temp, temp + sizeof(temp)/sizeof(int));

    ASSERT_EQ(6, find_rec(vec, 7));
}
} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
