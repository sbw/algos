#include "binary_search.h"

int find(const std::vector<int>& vec, int key)
{
    int lo = 0;
    int hi = vec.size()-1;
    
    while (lo <= hi) {
        int mid = lo + (hi-lo)/2;
        if (vec[mid] == key)
            return mid;
        if (vec[mid] < key)
            lo = mid+1;
        else
            hi = mid-1;
    }
    return -1;
}

int find_rec(const std::vector<int>& vec, int key)
{
    return find_helper(vec, 0, vec.size()-1, key);
}

int find_helper(const std::vector<int>& vec, int lo, int hi, int key)
{
    if (lo > hi) return -1;

    int mid = lo + (hi-lo)/2;
    if (vec[mid] == key) 
        return mid;
    
    if (vec[mid] < key)
        return find_helper(vec, mid+1, hi, key);
    return find_helper(vec, lo, mid-1, key);
}

size_t find_first(const std::vector<int>& vec, int key);

size_t find_rotated(const std::vector<int>& vec, int key);

size_t find_rotated(const std::vector<int>& vec, int key, size_t pivot);

size_t find_pivot(const std::vector<int>& vec);

// ------------------------ Problems --------------------------------
size_t fixed_point(const std::vector<int>& vec);

size_t find_duplicates(const std::vector<int>& vec);

coordinate find(const matrix& mat, const int key);

