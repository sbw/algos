#include <cstdlib>
#include <vector>


int find(const std::vector<int>& vec, int key);

int find_rec(const std::vector<int>& vec, int key);

int find_helper(const std::vector<int>& vec, int left, int right, int key);

size_t find_first(const std::vector<int>& vec, int key);

size_t find_rotated(const std::vector<int>& vec, int key);

size_t find_rotated(const std::vector<int>& vec, int key, size_t pivot);

size_t find_pivot(const std::vector<int>& vec);

// ------------------------ Problems --------------------------------
size_t fixed_point(const std::vector<int>& vec);

size_t find_duplicates(const std::vector<int>& vec);

typedef std::pair<size_t, size_t> coordinate;
typedef std::vector<std::vector<int> > matrix;

coordinate find(const matrix& mat, const int key);

