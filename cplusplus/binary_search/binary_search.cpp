#include "binary_search.h"

int find(const std::vector<int>& vec, int target)
{
	int lo = 0;
	int hi = (int) vec.size() - 1;

	while (lo <= hi) {
		int mid = lo + (hi-lo)/2;
		if (vec[mid] == target) return mid;
		if (vec[mid] < target)
			lo = mid + 1;
		else
			hi = mid - 1;
	}

	return -1;
}

int find_rec(const std::vector<int>& vec, int target)
{
	int hi = (int) vec.size() - 1;
	return find_helper(vec, 0, hi, target);
}

int find_helper(const std::vector<int>& vec, int lo, int hi, int target)
{
	if (lo > hi) return -1;

	int mid = lo + (hi-lo)/2;
	if (vec[mid] == target) return mid;
	if (vec[mid] < target)
		return find_helper(vec, mid+1, hi, target);
	return find_helper(vec, lo, mid-1, target);
}

size_t find_first(const std::vector<int>& vec, int key)
{
	return 0;
}

size_t find_rotated(const std::vector<int>& vec, int key)
{
	return 0;
}

size_t find_rotated(const std::vector<int>& vec, int key, size_t pivot)
{
	return 0;
}

size_t find_pivot(const std::vector<int>& vec)
{
	return 0;
}

// ------------------------ Problems --------------------------------
size_t fixed_point(const std::vector<int>& vec);

size_t find_duplicates(const std::vector<int>& vec);

coordinate find(const matrix& mat, const int key);

