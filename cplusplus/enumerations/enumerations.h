#include <set>
#include <vector>
#include <deque>
#include <string>

std::string print_set(const std::set<char>& s);

std::string print_set_of_sets(const std::set<std::set<char> >& s);

std::set<std::set<char> > powerset(std::set<char>& items);

std::set<std::set<char> > powerset_iter(const std::set<char>& items);

std::string print_queue(const std::deque<char>& d);

void permutations(std::deque<char>&, std::deque<char>&,
    std::vector<std::string>&);

std::set<std::vector<char> > permutations_iter(const std::set<char>& items);

std::set<std::set<std::set<char> > > partitions(const std::set<char>& base_set);

std::vector<std::string> segment_word(const std::string& tokens);
