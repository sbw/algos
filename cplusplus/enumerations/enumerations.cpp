#include "enumerations.h"
#include <iostream>

std::string print_set(const std::set<char>& s)
{
    std::string output("{");
    typename std::set<char>::const_iterator iter = s.begin();
    for (; iter != s.end(); ++iter)
        output = output + *iter + ", ";

    size_t pos = output.find_last_of(",");
    return output.substr(0, pos) + "}";   
}

std::string print_set_of_sets(const std::set<std::set<char> >& s)
{
    std::string output("{");
    typename std::set<std::set<char> >::const_iterator iter = s.begin();
    for (; iter != s.end(); ++iter) 
        output = output + print_set(*iter) + ", ";        
    
    size_t pos = output.find_last_of(",");
    return output.substr(0, pos) + "}";

    return output;
}

std::set<std::set<char> > powerset(std::set<char>& items)
{
    if (items.empty()) 
    {
        std::set<std::set<char> > temp;
        temp.insert(std::set<char>());
        return temp;
    }

    char item = *items.begin();
    items.erase(item);
    std::set<std::set<char> > tempSets = powerset(items);
    std::set<std::set<char> > sets(tempSets);

    auto iter = tempSets.begin();
    for (; iter != tempSets.end(); ++iter) { 
        std::set<char> temp(*iter);
        temp.insert(item);
        sets.insert(temp);
    }

    return sets;
}

std::set<std::set<char> > powerset_iter(const std::set<char>& items)
{
    std::set<std::set<char> > pset;
    pset.insert(std::set<char>());

    std::vector<char> ordered(items.begin(), items.end());

    for (size_t i = 0; i < ordered.size(); ++i) {
        std::set<std::set<char> > tempSets;
        for (auto iter = pset.begin(); iter != pset.end(); ++iter) {
            std::set<char> temp(iter->begin(), iter->end());
            temp.insert(ordered[i]);
            tempSets.insert(temp);   
        }
        pset.insert(tempSets.begin(), tempSets.end());
        pset.insert(std::set<char>());
    }

    return pset;
}

std::string print_queue(const std::deque<char>& d)
{
    std::string output("[");
    auto iter = d.begin();
    for (; iter != d.end(); ++iter)
        output = output + *iter + ", ";

    size_t pos = output.find_last_of(",");
    return output.substr(0, pos) + "]";   
}

void permutations(std::deque<char>& items, std::deque<char>& prefix,
    std::vector<std::string>& perm_set)
{
    if (items.empty()) 
    {
        perm_set.push_back(print_queue(prefix));
        return;
    }

    for (size_t i = 0; i < items.size(); ++i) {
        char front = items.front();
        items.pop_front();
        prefix.push_back(front);
        permutations(items, prefix, perm_set);
        prefix.pop_back();
        items.push_back(front);
    }
}

std::set<std::vector<char> > permutations_iter(const std::set<char>& items);

std::set<std::set<std::set<char> > > partitions(const std::set<char>& base_set);

std::vector<std::string> segment_word(const std::string& tokens);
