#include "enumerations.h"
#include "gtest/gtest.h"

namespace {

class EnumerationsTest : public ::testing::Test {
protected:
    EnumerationsTest() {}
    virtual ~EnumerationsTest() {}

    virtual void SetUp() 
    {
    }

    virtual void TearDown() 
    {
    }
};

TEST_F(EnumerationsTest, print_set)
{
    const char input[] = "wxyz";
    std::string expected = "{w, x, y, z}";
    std::set<char> s;

    for (const char* c = input; *c; ++c)
        s.insert(*c);

    EXPECT_EQ(print_set(s), expected);
}

TEST_F(EnumerationsTest, print_set_of_sets)
{
    const char* sets[] =
    {
        "xyz",
        "xy",
        "xz",
        "yz",
        "x",
        "y",
        "z",
        "",
        0
    };
    std::string expected =
        "{{}, {x}, {x, y}, {x, y, z}, {x, z}, {y}, {y, z}, {z}}";
    
    std::set<std::set<char> > s;

    for (const char** set = sets; *set; ++set) {
        std::set<char> temp;
        for (const char* c = *set; *c; ++c)
            temp.insert(*c);
        s.insert(temp);
    }
        
    EXPECT_EQ(print_set_of_sets(s), expected);
}

TEST_F(EnumerationsTest, powerset_iter)
{
    const char input[] = "xyz";
    std::string expected =
        "{{}, {x}, {x, y}, {x, y, z}, {x, z}, {y}, {y, z}, {z}}";

    std::set<char> s;
    for (const char* c = input; *c; ++c)
        s.insert(*c);
        
    std::set<std::set<char> > pset = powerset_iter(s);
    EXPECT_EQ(print_set_of_sets(pset), expected);
}

TEST_F(EnumerationsTest, powerset)
{
    const char input[] = "xyz";
    std::string expected =
        "{{}, {x}, {x, y}, {x, y, z}, {x, z}, {y}, {y, z}, {z}}";
   
    std::set<char> s; 
    for (const char* c = input; *c; ++c)
        s.insert(*c);
        
    std::set<std::set<char> > pset = powerset(s);
    EXPECT_EQ(print_set_of_sets(pset), expected);
}

TEST_F(EnumerationsTest, permutations)
{
    char input[] = {'a', 'b', 'c', 'd'};
    std::deque<char> items(input, input + sizeof(input)/sizeof(char));
    std::deque<char> single_perm;
    std::vector<std::string> perm_set;
    permutations(items, single_perm, perm_set);
    
    for (size_t i = 0; i < perm_set.size(); ++i)
        std::cout << perm_set[i] << std::endl;
}


} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
