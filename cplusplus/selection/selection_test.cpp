#include <iostream>
#include "selection.h"
#include "gtest/gtest.h"

namespace {

class SelectionTest : public ::testing::Test {
protected:
    SelectionTest() {}
    virtual ~SelectionTest() {}

    virtual void SetUp()
    {
        int n[] = { 45, 4, -16, 32, -9, -7, 9, 3, 5, 6 };
        unsorted.assign(n, n + sizeof(n)/sizeof(int));
        sorted.assign(n, n + sizeof(n)/sizeof(int));
        std::sort(sorted.begin(), sorted.end()); 
    }

    virtual void TearDown() 
    {
    }

    std::vector<int> unsorted;
    std::vector<int> sorted;
};

TEST_F(SelectionTest, bubble_sort)
{
}

TEST_F(SelectionTest, selection_sort)
{
}

TEST_F(SelectionTest, insertion_sort)
{
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
