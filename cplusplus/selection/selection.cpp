#include "selection.h"

// ------------------------ Core Selection --------------------------

int select_with_sort(std::vector<int>& vec, size_t k);

int median(std::vector<int>& vec);

size_t partition(std::vector<int>& vec, size_t pos, size_t offset,
                    size_t length);
size_t pivot_index(const std::vector<int>& vec, size_t offset, size_t size);

int select_with_pivot(std::vector<int>& vec, size_t k);

int median_of_3(const std::vector<int>& vec, size_t offset, size_t length);

// ------------------ Selection and Sorting Problems ----------------

// selection on large data
/*
void initialize_data(Arrays* arrays, ArrayBoundsList* data, 
                        size_t* total_size);

int median(Arrays* arrays);

void partition(const size_t index, const int value, ArrayBoundsList* data,
                Pivots* pivots, size_t* next_pos);

void update_search_bounds(const size_t target_pos, const size_t current_pos,
                            const size_t data_index, ArrayBoundsList* data,
                                Pivots* pivots);

*/
