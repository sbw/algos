#include <iostream>
#include "sorting.h"
#include "gtest/gtest.h"

namespace {

class SortingTest : public ::testing::Test {
protected:
    SortingTest() {}
    virtual ~SortingTest() {}

    virtual void SetUp()
    {
        int n[] = { 45, 4, -16, 32, -9, -7, 9, 3, 5, 6 };
        unsorted.assign(n, n + sizeof(n)/sizeof(int));
        sorted.assign(n, n + sizeof(n)/sizeof(int));
        std::sort(sorted.begin(), sorted.end()); 
    }

    virtual void TearDown() 
    {
    }

    std::vector<int> unsorted;
    std::vector<int> sorted;
};

TEST_F(SortingTest, bubble_sort)
{
    bubble_sort(unsorted);
    ASSERT_EQ(unsorted, sorted);
}

TEST_F(SortingTest, selection_sort)
{
    selection_sort(unsorted);
    ASSERT_EQ(unsorted, sorted);
}

TEST_F(SortingTest, insertion_sort)
{
    insertion_sort(unsorted);
    ASSERT_EQ(unsorted, sorted);
}

TEST_F(SortingTest, merge)
{
    std::vector<int> a = { 1, 3, 5, 7, 2, 4, 6, 8 };
    std::vector<int> output = { 1, 2, 3, 4, 5, 6, 7, 8 };
    merge(a, 0, 7);

    ASSERT_EQ(output, a);
}

TEST_F(SortingTest, merge_sort)
{
    merge_sort(unsorted, 0, unsorted.size()-1);
    ASSERT_EQ(unsorted, sorted);
}

TEST_F(SortingTest, quick_sort_nth_element)
{
    quick_sort(unsorted, 0, unsorted.size()-1, partition_a);
    ASSERT_EQ(unsorted, sorted);
}

TEST_F(SortingTest, quick_sort_custom)
{
    quick_sort(unsorted, 0, unsorted.size()-1, partition_b);
    ASSERT_EQ(unsorted, sorted);
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
