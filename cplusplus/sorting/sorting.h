#include <vector>
#include <algorithm>
#include <iostream>

// ------------------------ Core Sorting ----------------------------
void bubble_sort(std::vector<int>& vec);

void insertion_sort(std::vector<int>& vec);

void selection_sort(std::vector<int>& vec);

void shell_sort(std::vector<int>& vec);

void merge_sort(std::vector<int>& vec, int begin, int end);
void merge(std::vector<int>& vec, int begin, int end);

void quick_sort(std::vector<int>& vec, int begin, int end,
    void (*partition)(std::vector<int>& vec, int begin, int end, int& pivot));
void partition_a(std::vector<int>& vec, int begin, int end, int& pivot);
void partition_b(std::vector<int>& vec, int begin, int end, int& pivot);

void radix_sort(std::vector<int>& vec);

