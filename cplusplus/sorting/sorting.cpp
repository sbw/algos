#include "sorting.h"

// ------------------------- Core Sorting ---------------------------
/* Time  :  O(N^2) Quadratic
   Space :  in place
   Swaps :  up to N-1 per pass
   Stable:  yes
   Notes :  */
void bubble_sort(std::vector<int>& vec)
{
    if (vec.empty()) return;

    for (size_t right = vec.size()-1; right > 0; --right) 
        for (size_t left = 0; left < right; ++left)
            if (vec[left] > vec[right])
                std::swap(vec[left], vec[right]);
}

/* Time  : O(N^2) Quadratic
   Space : in place
   Swaps : one per pass, potentially lots of shifting
   Stable: yes
   Notes: pretty good on almost sorted stuff          */
void insertion_sort(std::vector<int>& vec)
{
    if (vec.empty()) return;
    
    for (int k = 1; k < vec.size(); ++k) {
        int current = vec[k];
        int m = k-1;
        for (; m >= 0 && vec[m] > current; --m) {
            vec[m+1] = vec[m]; 
        }
        vec[m+1] = current;
    }
}

/* Time  : O(N^2) Quadratic
   Space : in place
   Swaps : one per pass
   Stable: no
   Notes :               */
void selection_sort(std::vector<int>& vec)
{
    if (vec.empty()) return;
    
    /*
    std::vector<int>::iterator max_iter;

    for (size_t k = vec.size()-1; k > 0; --k) {
        max_iter = std::max_element(vec.begin(), vec.begin()+k+1);
        std::swap(*max_iter, vec[k]);
    
    }
    */

    for (size_t k = vec.size()-1; k > 0; --k) {
        size_t max_idx = 0;
        for (size_t m = 1; m <= k; ++m) 
            if (vec[max_idx] < vec[m]) 
                max_idx = m;   
        std::swap(vec[max_idx], vec[k]);
    }
}

/* Time:
   Space:
   Notes:               */
void shell_sort(std::vector<int>& vec);

/* Time  : O(N log N)
   Space : 2N (not in place) 
   Stable: yes
   Notes :               */
void merge_sort(std::vector<int>& vec, int begin, int end)
{
    if (end - begin < 1) return;
    
    int mid = begin + (end-begin)/2;
    merge_sort(vec, begin, mid);
    merge_sort(vec, mid+1, end);
    merge(vec, begin, end);            
}

void merge(std::vector<int>& vec, int begin, int end)
{
    int mid = begin + (end-begin)/2;
    std::vector<int> tmp(vec.begin()+mid+1, vec.begin()+end+1);

    int k = mid;
    int m = tmp.size()-1;

    while (k >= begin && m >= 0) {
         
        if (vec[k] > tmp[m])
        {
            vec[m+k+1] = vec[k];
            --k;
        }
        else 
        {   
            vec[m+k+1] = tmp[m];
            --m;
        }
    }   

    while (k >= begin) {
        vec[m+k+1] = vec[k];
        --k;
    }
  
    while (m >= 0) {
        vec[m+k+1] = tmp[m];
        --m;
    }
}

/* Time  : O(N log N)
   Space : in place
   Stable: typical in place, no
   Notes: Remember worst case can be quadratic   */
void quick_sort(std::vector<int>& vec, int begin, int end,
    void (*partition)(std::vector<int>& vec, int begin, int end, int& pivot))
{
    if (end - begin < 1) return;
    
    int pivot = begin + (end-begin)/2;
    partition(vec, begin, end, pivot);
    quick_sort(vec, begin, pivot-1, partition);
    quick_sort(vec, pivot+1, end, partition);
}

void partition_a(std::vector<int>& vec, int begin, int end, int& pivot)
{   
    std::nth_element(vec.begin()+begin, vec.begin()+pivot, vec.begin()+end+1);
}

void partition_b(std::vector<int>& vec, int begin, int end, int& pivot)
{
    int g = begin;
    for (int u = begin; u < end; ++u) 
        if (vec[u] <= vec[end])
        {
            std::swap(vec[u], vec[g]);
            ++g;
        }

    std::swap(vec[g], vec[end]);
    pivot = g;
}

/* Time:
   Space:
   Notes:               */
void radix_sort(std::vector<int>& vec);

