#include <vector>

// unsigned
typedef std::vector<unsigned char> bigint;

bool msb_set(const bigint& b);

bigint add(const bigint& lhs, const bigint& rhs);

bigint twos_complement(const bigint& b);

bigint multiply(const bigint& lhs, const bigint& rhs);

bigint karatsuba(bigint lhs, bigint rhs);

bigint karatsuba_recursive(const bigint& lhs, const bigint& rhs);

// signed
struct signed_bigint {
    bool negative = false;
    bigint value;
};

signed_bigint add(const signed_bigint& lhs, const signed_bigint& rhs);

signed_bigint subtract(bigint lhs, bigint rhs);


