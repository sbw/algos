#include <random>

std::random_device rd;
std::mt19937 rng(rd());

void deal(std::vector<int> deck, size_t hand_size, std::vector<int>& hand);

void shuffle_riffle(size_t num, std::vector<int>& deck);

void shuffle_selection(std::vector<int>& deck);

int select(std::istream& stream);

std::vector<int> select_n_elements(std::istream& stream, size_t m);


