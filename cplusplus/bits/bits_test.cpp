#include "bits.h"
#include "gtest/gtest.h"

namespace {

class BitsTest : public ::testing::Test {
protected:
    BitsTest() {}
    virtual ~BitsTest() {}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(BitsTest, power_of_two_iter8192)
{
    ASSERT_TRUE(power_of_two_iter(8192));    
}

TEST_F(BitsTest, power_of_two_iter10017)
{
    ASSERT_FALSE(power_of_two_iter(10017));    
}

TEST_F(BitsTest, power_of_two_shifting8192)
{
    ASSERT_TRUE(power_of_two_shifting(8192));    
}

TEST_F(BitsTest, power_of_two_shfiting10017)
{
    ASSERT_FALSE(power_of_two_shifting(10017));    
}

TEST_F(BitsTest, clear_last_set_bit64)
{
    ASSERT_EQ(0, clear_last_set_bit(64));
}

TEST_F(BitsTest, clear_last_set_bit96)
{
    ASSERT_EQ(64, clear_last_set_bit(96));
}

TEST_F(BitsTest, power_of_two_clear8192)
{
    ASSERT_TRUE(power_of_two_clear(8192));    
}

TEST_F(BitsTest, power_of_two_clear10017)
{
    ASSERT_FALSE(power_of_two_clear(10017));    
}

TEST_F(BitsTest, lowest_set_bit416)
{
    ASSERT_EQ(32, lowest_set_bit(416));
}

TEST_F(BitsTest, count_bits_set_iter255)
{
    ASSERT_EQ(8, count_bits_set_iter(255));
}

TEST_F(BitsTest, count_bits_set_iter257)
{
    ASSERT_EQ(2, count_bits_set_iter(257));
}

TEST_F(BitsTest, count_bits_set255)
{
    ASSERT_EQ(8, count_bits_set(255));
}

TEST_F(BitsTest, count_bits_set257)
{
    ASSERT_EQ(2, count_bits_set(257));
}

TEST_F(BitsTest, fold_over128)
{
    ASSERT_EQ(255, fold_over(128));
}

TEST_F(BitsTest, highest_set_bit)
{
    ASSERT_EQ(128, highest_set_bit(255));
}

TEST_F(BitsTest, reverse_bits1)
{
    unsigned char x = 240;
    ASSERT_EQ(15, reverse_bits(x));
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
