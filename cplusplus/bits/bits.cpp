#include "bits.h"
#include <iostream>


// ------------------------- Core Bit Tricks ------------------------
unsigned clear_last_set_bit(unsigned x)
{
    return x & x-1;
}

unsigned lowest_set_bit(unsigned x)
{
    return x & ~(x-1);
}

unsigned count_bits_set(unsigned x)
{
    unsigned count = 0;
    while (x) {
        x = clear_last_set_bit(x);
        ++count;
    }
    
    return count;    
}

unsigned count_bits_set_iter(unsigned x)
{
    unsigned count = 0;
    unsigned y = 1;

    for (size_t i = 0; i < 32; ++i) {
        if (x & y) ++count;
        y <<= 1;
    }

    return count;
}

unsigned fold_over(unsigned x)
{
    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);

    return x;
}

unsigned int highest_set_bit(unsigned x)
{
    x = fold_over(x);
    return x & ~(x >> 1);    
}

// --------------------------- Bit Problems -------------------------

/* Determine if value is a power of 2 */
bool power_of_two_iter(unsigned x)
{
    unsigned ones = 0;
    unsigned y = 1;

    for (size_t i = 0; i < 32; ++i) {
        if (x & y) ++ones;    
        y <<= 1;
    }

    return ones == 1;
}

bool power_of_two_shifting(unsigned x)
{
    unsigned p2 = 1;
    
    while (p2 != 0) {
        if (x == p2) return true;
        p2 <<= 1;
    }

    return false;
}

bool power_of_two_clear(unsigned x)
{
    return clear_last_set_bit(x) == 0;    
}

/* Determine next highest log of 2 */
unsigned log_x(unsigned x);

/* Determine next higher power of two */
unsigned next_power_of_two(unsigned x);

unsigned char reverse_bits(unsigned char x)
{
    int i = 0x1 << 7;
    int j = 0x1;

    while (i > j) {
        if ((i & x) ^ (j & x)) 
        {
           if (i & x)
           {
                x &= ~i;
                x |= j;
           }
           else
           {
                x &= ~j;
                x |= i;
           }
        }

        i >>= 1;
        j <<= 1;
    }
       
    return x;
}

/* Implement multiplication without * operator */
int multiply(int x, int y);

/* Implement division without / operator */
int divide(int x, int y);

/*  You are given two 32-bit numbers, N and M, and two bit positions, i and j.
    Write a method to set all bits between i and j in N equal to M (e.g. M
    becomes a substring of N located at i and starting at j). */
unsigned bit_setting(unsigned x, unsigned y);

/*  Given a (decimal -- e.g 3.72) number that is passed in as a string, print
    the binary representation. If the number cannot be represented accurately
    in binary, print "Error" */
std::string convert_to_binary(const std::string& x);

/*  Given an integer, print the next smallest and next largest number that have
    the same number of 1 bits in their binary representation. */
std::pair<unsigned, unsigned> up_down(unsigned x);

/*  Write a function to determine the number of bits required to convert
    integer A to integer B. */
int bit_difference(int x, int y);

/*  Write a program to swap odd and even bits in an integer with as few
    instructions as possible(e.g bit 0 and bit 1 are swapped, bit 2 and
    bit 3 are swapped. */
unsigned swap_bits(unsigned x);

/*  Write a function to swap a number in place without temporary 
    variables */
int swap_in_place(int x, int y);

/*  Write a method which finds the maximum of two numbers. You should not use
    if-else or any other comparison operator. */
int max(int x, int y);

/*  Write a program to find whether a machine is big endian or little
    endian */
bool is_big_endian();

/*  You have an array with all the numbers from 1 to N, where is at most
    32,000. The array may have duplicate entries and you do not know what
    N is. With only 4KB of memory available, how would you print all
    duplicate elements in the array? */
void print_duplicates(const std::vector<int>& vec);



