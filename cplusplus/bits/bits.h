#include <vector>

bool power_of_two_iter(unsigned x);

bool power_of_two_shifting(unsigned x);

bool power_of_two_clear(unsigned x);

unsigned clear_last_set_bit(unsigned x);

unsigned lowest_set_bit(unsigned x);

unsigned count_bits_set(unsigned x);

unsigned count_bits_set_iter(unsigned x);

unsigned fold_over(unsigned x);

unsigned int highest_set_bit(unsigned x);

unsigned log_x(unsigned x);

unsigned next_power_of_two(unsigned x);

unsigned char reverse_bits(unsigned char x);

int multiply(int x, int y);

int divide(int x, int y);


