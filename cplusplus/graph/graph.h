#include <map>
#include <vector>
#include <set>

namespace simple {
	struct Node;
}

struct Node;

// ----------------------- Core Graph Algorithms --------------------
void bfs(simple::Node* origin, simple::Node* dest, std::vector<simple::Node*>&);

void dfs(simple::Node* origin, simple::Node* dest, std::vector<simple::Node*>&);

void dfs(simple::Node* origin, std::vector<simple::Node*>&);

std::vector<simple::Node*> topological_sort(const std::set<simple::Node*>& set);

std::map<Node*, double> dijkstra(Node* origin);

double prim(Node* origin);

int minimal_network(const std::vector<std::vector<int> >& matrix);

int path_sum_four_ways(const std::vector<std::vector<int> >& matrix,
                                                                int n, int m);
