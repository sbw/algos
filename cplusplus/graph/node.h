#pragma once

namespace simple {

struct Node {
	Node(const std::string& key) : key_(key) {}

	std::string key_;
	std::vector<Node*> neighbors_;
};

} //end namespace simple


struct Node {
    Node(const std::string& key) : key_(key), distance_(0.0) {}

    std::string key_;
    double distance_;

    struct Neighbor {
    	Neighbor(Node* n, double weight=0.0) : node_(n), weight_(weight) {}
    	Node* node_;
    	double weight_;
    };

    std::vector<Neighbor> neighbors_;
};

struct CompareNode {
   bool operator()(const Node* a, const Node* b)
   {
       return a->distance_ > b->distance_;
   }
};

struct Edge {
    Edge(Node* u, Node* v, double weight) : u(u), v(v), weight(weight) {}

    Node* u;
    Node* v;
    double weight;
};

struct CompareEdge {
bool operator()(const Edge& a, const Edge& b)
    {
        return a.weight > b.weight;
    }
};

