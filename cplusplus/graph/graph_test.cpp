#include "gtest/gtest.h"
#include "node.h"
#include "graph.h"

namespace {

class GraphTest : public ::testing::Test {
protected:
    GraphTest() {}
    virtual ~GraphTest() {}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(GraphTest, DFS)
{
    std::string keys[] = { "A", "B", "C", "D", "E", "F", "G", "H" };
    std::string ans[] = { "A", "C", "G", "F", "H", "E", "D", "B" };
    std::map<std::string, simple::Node*> nodes;
    
    for (size_t i = 0; i < 8; ++i) 
        nodes[keys[i]] = new simple::Node(keys[i]);
    
    nodes["A"]->neighbors_.emplace_back(nodes["B"]);
    nodes["A"]->neighbors_.emplace_back(nodes["C"]);

    nodes["B"]->neighbors_.emplace_back(nodes["A"]);
    nodes["B"]->neighbors_.emplace_back(nodes["D"]);

    nodes["C"]->neighbors_.emplace_back(nodes["A"]);
    nodes["C"]->neighbors_.emplace_back(nodes["D"]);
    nodes["C"]->neighbors_.emplace_back(nodes["G"]);

    nodes["D"]->neighbors_.emplace_back(nodes["B"]);
    nodes["D"]->neighbors_.emplace_back(nodes["C"]);
    nodes["D"]->neighbors_.emplace_back(nodes["E"]);

    nodes["E"]->neighbors_.emplace_back(nodes["D"]);
    nodes["E"]->neighbors_.emplace_back(nodes["F"]);

    nodes["F"]->neighbors_.emplace_back(nodes["E"]);
    nodes["F"]->neighbors_.emplace_back(nodes["G"]);
    nodes["F"]->neighbors_.emplace_back(nodes["H"]);

    nodes["G"]->neighbors_.emplace_back(nodes["C"]);
    nodes["G"]->neighbors_.emplace_back(nodes["F"]);
    
    nodes["H"]->neighbors_.emplace_back(nodes["F"]);

    std::vector<simple::Node*> output;
    dfs(nodes["A"], nullptr, output);
    ASSERT_TRUE(output.size());

    for (size_t i = 0; i < output.size(); ++i) {
        std::cout << output[i]->key_ << '\t';
        ASSERT_EQ(output[i]->key_, ans[i]);
    }

    std::cout << std::endl;

    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter) {
    	delete(iter->second);
    }
}

TEST_F(GraphTest, DFS2)
{
    std::string keys[] = { "A", "B", "C", "D", "E", "F", "G", "H" };
    std::string ans[] = { "E", "H", "F", "G", "C", "D", "B", "A" };
    std::map<std::string, simple::Node*> nodes;

    for (size_t i = 0; i < 8; ++i)
        nodes[keys[i]] = new simple::Node(keys[i]);

    nodes["A"]->neighbors_.emplace_back(nodes["B"]);
    nodes["A"]->neighbors_.emplace_back(nodes["C"]);

    nodes["B"]->neighbors_.emplace_back(nodes["A"]);
    nodes["B"]->neighbors_.emplace_back(nodes["D"]);

    nodes["C"]->neighbors_.emplace_back(nodes["A"]);
    nodes["C"]->neighbors_.emplace_back(nodes["D"]);
    nodes["C"]->neighbors_.emplace_back(nodes["G"]);

    nodes["D"]->neighbors_.emplace_back(nodes["B"]);
    nodes["D"]->neighbors_.emplace_back(nodes["C"]);
    nodes["D"]->neighbors_.emplace_back(nodes["E"]);

    nodes["E"]->neighbors_.emplace_back(nodes["D"]);
    nodes["E"]->neighbors_.emplace_back(nodes["F"]);

    nodes["F"]->neighbors_.emplace_back(nodes["E"]);
    nodes["F"]->neighbors_.emplace_back(nodes["G"]);
    nodes["F"]->neighbors_.emplace_back(nodes["H"]);

    nodes["G"]->neighbors_.emplace_back(nodes["C"]);
    nodes["G"]->neighbors_.emplace_back(nodes["F"]);

    nodes["H"]->neighbors_.emplace_back(nodes["F"]);

    std::vector<simple::Node*> output;
    dfs(nodes["A"], output);
    ASSERT_TRUE(output.size());

    for (size_t i = 0; i < output.size(); ++i) {
        std::cout << output[i]->key_ << '\t';
        ASSERT_EQ(output[i]->key_, ans[i]);
    }

    std::cout << std::endl;

    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter) {
    	delete(iter->second);
    }
}

TEST_F(GraphTest, BFS)
{
    std::string keys[] = { "A", "B", "C", "D", "E", "F", "G", "H" };
    std::string ans[] = { "A", "B", "C", "D", "G", "E", "F", "H" };
    std::map<std::string, simple::Node*> nodes;
    
    for (size_t i = 0; i < 8; ++i) 
        nodes[keys[i]] = new simple::Node(keys[i]);
    
    nodes["A"]->neighbors_.emplace_back(nodes["B"]);
    nodes["A"]->neighbors_.emplace_back(nodes["C"]);

    nodes["B"]->neighbors_.emplace_back(nodes["A"]);
    nodes["B"]->neighbors_.emplace_back(nodes["D"]);

    nodes["C"]->neighbors_.emplace_back(nodes["A"]);
    nodes["C"]->neighbors_.emplace_back(nodes["D"]);
    nodes["C"]->neighbors_.emplace_back(nodes["G"]);

    nodes["D"]->neighbors_.emplace_back(nodes["B"]);
    nodes["D"]->neighbors_.emplace_back(nodes["C"]);
    nodes["D"]->neighbors_.emplace_back(nodes["E"]);

    nodes["E"]->neighbors_.emplace_back(nodes["D"]);
    nodes["E"]->neighbors_.emplace_back(nodes["F"]);

    nodes["F"]->neighbors_.emplace_back(nodes["E"]);
    nodes["F"]->neighbors_.emplace_back(nodes["G"]);
    nodes["F"]->neighbors_.emplace_back(nodes["H"]);

    nodes["G"]->neighbors_.emplace_back(nodes["C"]);
    nodes["G"]->neighbors_.emplace_back(nodes["F"]);
    
    nodes["H"]->neighbors_.emplace_back(nodes["F"]);

    std::vector<simple::Node*> output;
    bfs(nodes["A"], nullptr, output);
    ASSERT_TRUE(output.size());

    for (size_t i = 0; i < output.size(); ++i) {
		std::cout << output[i]->key_ << '\t';
		ASSERT_EQ(output[i]->key_, ans[i]);
    }

    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter) {
    	delete(iter->second);
    }
}

TEST_F(GraphTest, TopologicalSort)
{
    const char* keys[] = { "A", "B", "C", "D", "E", "F", "G", "H" };
    std::map<std::string, simple::Node*> nodes;
    
    for (size_t i = 0; i < 8; ++i) 
        nodes[keys[i]] = new simple::Node(keys[i]);
    
    nodes["A"]->neighbors_.emplace_back(nodes["B"]);
    nodes["A"]->neighbors_.emplace_back(nodes["D"]);

    nodes["B"]->neighbors_.emplace_back(nodes["C"]);

    nodes["C"]->neighbors_.emplace_back(nodes["D"]);
    nodes["C"]->neighbors_.emplace_back(nodes["E"]);

    nodes["D"]->neighbors_.emplace_back(nodes["E"]);

    nodes["H"]->neighbors_.emplace_back(nodes["D"]);
    nodes["H"]->neighbors_.emplace_back(nodes["G"]);
    
    std::set<simple::Node*> set;
    std::map<std::string, simple::Node*>::const_iterator iter = nodes.begin();
    for (; iter != nodes.end(); ++iter) 
        set.insert(iter->second);    
    
    std::vector<simple::Node*> output = topological_sort(set);

    for (size_t i = 0; i < output.size(); ++i)
        std::cout << output[i]->key_ << '\t';
    std::cout << std::endl;

    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter) {
		delete(iter->second);
	}
}

TEST_F(GraphTest, Prim)
{
    std::string keys[] = { "0", "1", "2", "3", "4", "5", "6", "7" };
    std::map<std::string, Node*> nodes;
    
    for (size_t i = 0; i < 8; ++i) {
        nodes[keys[i]] = new Node(keys[i]);
        nodes[keys[i]]->distance_ = std::numeric_limits<double>::infinity();
    }
    
    nodes["0"]->neighbors_.emplace_back(nodes["2"], 0.26);
    nodes["0"]->neighbors_.emplace_back(nodes["4"], 0.38);
    nodes["0"]->neighbors_.emplace_back(nodes["7"], 0.16);
    
    nodes["1"]->neighbors_.emplace_back(nodes["2"], 0.36);
    nodes["1"]->neighbors_.emplace_back(nodes["3"], 0.29);
    nodes["1"]->neighbors_.emplace_back(nodes["5"], 0.32);
    nodes["1"]->neighbors_.emplace_back(nodes["7"], 0.19);
    
    nodes["2"]->neighbors_.emplace_back(nodes["3"], 0.17);
    nodes["2"]->neighbors_.emplace_back(nodes["7"], 0.34);
    
    nodes["3"]->neighbors_.emplace_back(nodes["6"], 0.52);
    
    nodes["4"]->neighbors_.emplace_back(nodes["5"], 0.35);
    nodes["4"]->neighbors_.emplace_back(nodes["7"], 0.37);

    nodes["5"]->neighbors_.emplace_back(nodes["7"], 0.28);
    
    nodes["6"]->neighbors_.emplace_back(nodes["0"], 0.58);
    nodes["6"]->neighbors_.emplace_back(nodes["2"], 0.40);
    nodes["6"]->neighbors_.emplace_back(nodes["4"], 0.93);

    //
    nodes["2"]->neighbors_.emplace_back(nodes["0"], 0.26);
    nodes["4"]->neighbors_.emplace_back(nodes["0"], 0.38);
    nodes["7"]->neighbors_.emplace_back(nodes["0"], 0.16);
    
    nodes["2"]->neighbors_.emplace_back(nodes["1"], 0.36);
    nodes["3"]->neighbors_.emplace_back(nodes["1"], 0.29);
    nodes["5"]->neighbors_.emplace_back(nodes["1"], 0.32);
    nodes["7"]->neighbors_.emplace_back(nodes["1"], 0.19);
    
    nodes["3"]->neighbors_.emplace_back(nodes["2"], 0.17);
    nodes["7"]->neighbors_.emplace_back(nodes["2"], 0.34);
    
    nodes["6"]->neighbors_.emplace_back(nodes["3"], 0.52);
    
    nodes["5"]->neighbors_.emplace_back(nodes["4"], 0.35);
    nodes["7"]->neighbors_.emplace_back(nodes["4"], 0.37);

    nodes["7"]->neighbors_.emplace_back(nodes["5"], 0.28);
    
    nodes["0"]->neighbors_.emplace_back(nodes["6"], 0.58);
    nodes["2"]->neighbors_.emplace_back(nodes["6"], 0.40);
    nodes["4"]->neighbors_.emplace_back(nodes["6"], 0.93);
    

    ASSERT_EQ(1.81, prim(nodes["0"]));

    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter) {
        	delete(iter->second);
    }
}

TEST_F(GraphTest, Dijkstra)
{
    std::string keys[] = { "0", "1", "2", "3", "4", "5", "6", "7" };
    std::map<std::string, Node*> nodes;
    
    for (size_t i = 0; i < 8; ++i) {
        nodes[keys[i]] = new Node(keys[i]);
        nodes[keys[i]]->distance_ = std::numeric_limits<double>::infinity();
    }
    
    nodes["0"]->neighbors_.emplace_back(nodes["2"], 0.26);
    nodes["0"]->neighbors_.emplace_back(nodes["4"], 0.38);

    nodes["1"]->neighbors_.emplace_back(nodes["3"], 0.29);
    nodes["2"]->neighbors_.emplace_back(nodes["7"], 0.34);
    nodes["3"]->neighbors_.emplace_back(nodes["6"], 0.52);

    nodes["4"]->neighbors_.emplace_back(nodes["5"], 0.35);
    nodes["4"]->neighbors_.emplace_back(nodes["7"], 0.37);

    nodes["5"]->neighbors_.emplace_back(nodes["1"], 0.32);
    nodes["5"]->neighbors_.emplace_back(nodes["4"], 0.35);
    nodes["5"]->neighbors_.emplace_back(nodes["7"], 0.28);
    
    nodes["6"]->neighbors_.emplace_back(nodes["0"], 0.58);
    nodes["6"]->neighbors_.emplace_back(nodes["2"], 0.40);
    nodes["6"]->neighbors_.emplace_back(nodes["4"], 0.93);

    nodes["7"]->neighbors_.emplace_back(nodes["3"], 0.39);
    nodes["7"]->neighbors_.emplace_back(nodes["5"], 0.28);

    std::map<Node*, double> spt = dijkstra(nodes["0"]);
    
    std::map<Node*, double>::const_iterator iter = spt.begin();
    for (; iter != spt.end(); ++iter) {
        Node* current = iter->first;
        std::cout.precision(2);
        std::cout << std::fixed;
        std::cout << current->key_ << ":\t" << iter->second << std::endl;
    }

    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter) {
        	delete(iter->second);
    }

}

TEST_F(GraphTest, path_sum_four_ways_demo)
{
    std::vector<std::vector<int> > matrix(5, std::vector<int>(5));

    ASSERT_EQ(2297, path_sum_four_ways(matrix, matrix.size()-1,                                                     matrix[0].size()-1));
}

TEST_F(GraphTest, minimal_network_demo)
{
    std::vector<std::vector<int> > matrix(7, std::vector<int>(7));
    matrix[0][0] = -1;
    matrix[0][1] = 16;
    matrix[0][2] = 12;
    matrix[0][3] = 21;
    matrix[0][4] = -1;
    matrix[0][5] = -1;
    matrix[0][6] = -1;

    matrix[1][0] = 16;
    matrix[1][1] = -1;
    matrix[1][2] = -1;
    matrix[1][3] = 17;
    matrix[1][4] = 20;
    matrix[1][5] = -1;
    matrix[1][6] = -1;

    matrix[2][0] = 12;
    matrix[2][1] = -1;
    matrix[2][2] = -1;
    matrix[2][3] = 28;
    matrix[2][4] = -1;
    matrix[2][5] = 31;
    matrix[2][6] = -1;

    matrix[3][0] = 21;
    matrix[3][1] = 17;
    matrix[3][2] = 28;
    matrix[3][3] = -1;
    matrix[3][4] = 18;
    matrix[3][5] = 19;
    matrix[3][6] = 23;

    matrix[4][0] = -1;
    matrix[4][1] = 20;
    matrix[4][2] = -1;
    matrix[4][3] = 18;
    matrix[4][4] = -1;
    matrix[4][5] = -1;
    matrix[4][6] = 11;

    matrix[5][0] = -1;
    matrix[5][1] = -1;
    matrix[5][2] = 31;
    matrix[5][3] = 19;
    matrix[5][4] = -1;
    matrix[5][5] = -1;
    matrix[5][6] = 27;

    matrix[6][0] = -1;
    matrix[6][1] = -1;
    matrix[6][2] = -1;
    matrix[6][3] = 23;
    matrix[6][4] = 11;
    matrix[6][5] = 27;
    matrix[6][6] = -1;

    ASSERT_EQ(93, minimal_network(matrix));
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
