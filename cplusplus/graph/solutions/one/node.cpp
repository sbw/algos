#ifndef NODE_H
#define NODE_H

#include <vector>
#include <map>
#include <string>

#include "basic_node.h"

enum Color { white, gray, black };

template <typename T>
class Node : public BasicNode {
public:
    Node(const std::string& key);
    virtual ~Node();
    
    Color getColor() const; 
    void setColor(Color color);

    T getDistance() const;
    void setDistance();

    Node* getPredecessor() const;
    void setPredecessor(Node* predecessor);

    T getWeight(const std::string& key) const;
    void setWeight(const std::string& key, T weight);
 
private:
    T getWeight(Node* neighbor);
    
    Color color;
    T distance;
    std::map<Node*, T> weights;
};
#endif

template <typename T>
Node<T>::Node(const std::string& key) : BasicNode(key), 
                color(white), distance(0.0) {}
    
template <typename T>    
Color Node<T>::getColor() const 
{ 
    return color; 
}
 
template <typename T>
void Node<T>::setColor(Color color) 
{ 
    this->color = color; 
}

template <typename T>
T Node<T>::getWeight(const std::string& key) const
{
    typename std::vector<Node<T>*>::const_iterator vter = neighbors.begin();
    for (; vter != neighbors.end(); ++vter)
        if (vter->first == key) break;

    typename std::map<Node<T>*, T>::const_iterator mter = weights.find(*vter);
    return (mter != weights.end()) ? mter->second : 0;
}

template <typename T>
void Node<T>::setWeight(const std::string& key, T weight) 
{
    typename std::vector<Node<T>*>::const_iterator vter = neighbors.begin();
    for (; vter != neighbors.end(); ++vter)
        if (vter->first == key) break;

    typename std::map<Node<T>*, T>::const_iterator mter = weights.find(*vter);
    
    if (mter != weights.end())
        mter->second = weight;
}

template <typename T>
T Node<T>::getDistance() const 
{ 
    return distance; 
}

template <typename T>
void Node<T>::setDistance() 
{ 
    this->distance = distance; 
}

