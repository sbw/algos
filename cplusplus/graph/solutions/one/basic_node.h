#ifndef BASIC_NODE_H
#define BASIC_NODE_H

#include <vector>

struct BasicNode  {
    BasicNode(const std::string& key) : key(key) {}
    
    std::string key;
    std::vector<BasicNode*> neighbors;
};
#endif

