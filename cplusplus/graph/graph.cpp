#include "graph.h"
#include "node.h"

#include <queue>
#include <stack>
#include <iostream>

void bfs(simple::Node* origin, simple::Node* dest, std::vector<simple::Node*>& nodes)
{
	std::queue<simple::Node*> queue;
	std::set<simple::Node*> visited; // keep track of visitors to avoid cycles

	queue.push(origin);
	visited.insert(origin);

	while (!queue.empty()) {
		simple::Node* current = queue.front();
		queue.pop();

		// to keep search path
		nodes.push_back(current);
		// to determine when to stop searching
		if (current == dest) break;

		for (auto neighbor : current->neighbors_) {
			if (visited.find(neighbor) == visited.end()) {
				queue.push(neighbor);
				visited.insert(neighbor);
			}
		}
	}
}

void dfs(simple::Node* origin, simple::Node* dest, std::vector<simple::Node*>& nodes)
{
	std::stack<simple::Node*> stack;
	std::set<simple::Node*> visited;

	stack.push(origin);
	visited.insert(origin);

	while (!stack.empty()) {
		simple::Node* current = stack.top();
		stack.pop();

		// to keep search path
		nodes.push_back(current);
		// to determine when to stop searching
		if (current == dest) break;

		for (auto neighbor : current->neighbors_) {
			if (visited.find(neighbor) == visited.end()) {
				stack.push(neighbor);
				visited.insert(neighbor);
			}
		}
	}
}

void dfs(simple::Node* origin, std::vector<simple::Node*>& nodes)
{
	std::stack<simple::Node*> stack;
	std::set<simple::Node*> visited;

	stack.push(origin);
	visited.insert(origin);

	while (!stack.empty()) {
		simple::Node* current = stack.top();

		bool pushed = false;
		for (auto neighbor : current->neighbors_) {
			if (visited.find(neighbor) == visited.end()) {
				stack.push(neighbor);
				visited.insert(neighbor);
				pushed = true;
				break;
			}
		}

		if (!pushed) {
			stack.pop();
			nodes.push_back(current);
		}
	}
}

std::map<Node*, double> dijkstra(Node* origin)
{

    return std::map<Node*, double>();
}


double prim(Node* origin)
{
	return 0.0;
}

std::vector<simple::Node*> topological_sort(const std::set<simple::Node*>& set)
{
    return std::vector<simple::Node*>();
}

int minimal_network(const std::vector<std::vector<int> >& matrix)
{
	return 0;
}

int path_sum_four_ways(const std::vector<std::vector<int> >& matrix, int n, int m)
{
	return 0;
}
