TEST_EXE = $(TYPE)_test
OBJS = $(TEST_EXE).o $(TYPE).o

all:    $(TEST_EXE)

$(TEST_EXE): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LDFLAGS) $(GTEST_LIB)  -o $(TEST_DIR)/$(TEST_EXE)

%.o:%.cpp
	mkdir -p $(TEST_DIR)
	$(CC) $(CFLAGS) -o $@ -c $<

clean: 
	rm -f *.o
	rm -f $(TEST_DIR)/$(TEST_EXE)



