#include "dynamic.h"
#include <iostream>

// ------------------- Dynamic Programming Problems -----------------

unsigned fib_brute(unsigned nth)
{
    if (nth == 0) return 0;
    if (nth == 1) return 1;

    std::cout << "n: " << nth << std::endl;

    return fib_brute(nth-2) + fib_brute(nth-1);
}

int fib_memo(int nth)
{
	std::vector<int> memo(nth+1, -1);
	return fib_memo_helper(memo, nth);
}

int fib_memo_helper(std::vector<int>& memo, int nth)
{
	if (memo[nth] < 0) {
		if (nth == 0)
			memo[nth] = 0;
	    else if (nth == 1)
			memo[nth] = 1;
		else
			memo[nth] = fib_memo_helper(memo, nth-2) + fib_memo_helper(memo, nth-1);
	}
	return memo[nth];
}

void pascals_triangle(unsigned nth)
{

}

bool subset_sum(const std::vector<int>& s, int k, std::vector<int>& result)
{
    return true;
}

bool partition_problem(const std::vector<int>& set, std::vector<int>& result)
{
    return true;
}

int lcs_brute(const char* s, const char* t)
{
	if (!s || !t) return 0;
	if (!*s || !*t) return 0;

	if (*s == *t)
		return 1 + lcs_brute(s+1, t+1);
	return std::max(lcs_brute(s, t+1),
			        lcs_brute(s+1, t));
}

int lcs_recur(const std::string& s, const std::string& t)
{
	// create lookup table
	std::vector<std::vector<int>> L(s.size()+1, std::vector<int>(t.size()+1, -1));

	return lcs_recur_helper(L, s, 0, t, 0);
}

int lcs_recur_helper(std::vector<std::vector<int>>& L,
		             const std::string& s,
					 size_t s_index,
					 const std::string& t,
					 size_t t_index)
{
	if (L[s_index][t_index] < 0) {
		if (s_index == s.size() || t_index == t.size())
			return L[s_index][t_index] = 0;
		else if (s[s_index] == t[t_index])
			L[s_index][t_index] = 1 + lcs_recur_helper(L, s, s_index+1, t, t_index+1);
		else L[s_index][t_index] = std::max(lcs_recur_helper(L, s, s_index, t, t_index+1),
		                                    lcs_recur_helper(L, s, s_index+1, t, t_index));
	}

	return L[s_index][t_index];
}

int lcs(const std::string& s, const std::string& t)
{
    return -1;
}


std::string lcs_pattern(const std::string& s, const std::string& t)
{ 
    return std::string();
}

// longest increasing subsequence
int longest_increasing_subsequence(const std::vector<int>& s)
{    
	std::vector<int> longest(s.size(), 1);

	for (auto i = 1; i < s.size(); ++i) {
		for (auto k = 0; k < i; ++k) {
			if (s[i] > s[k]) {
				longest[i] = std::max(longest[k], 1 + longest[k]);
			}
		}
	}

	return *std::max_element(longest.begin(), longest.end());
}

int longest_increasing_subsequence2(const std::vector<int>& s);

int longest_increasing_subsequence3(const std::vector<int>& set, 
                                        std::vector<int>& result);
// Levenshtein
int edit_distance_l(const std::string& s, const std::string& t);

// Damerau-Levenshtein
int edit_distance_dl(const std::string& s, const std::string& t);


std::vector<int> zero_one_knapsack(const std::vector<std::pair<int,int>>& items, int limit)
{
	// create lookup table
	std::vector<std::vector<int>> V(items.size()+1, std::vector<int>(limit+1));

	for (auto i = 0; i < items.size(); ++i) {
		// treat row separately
		auto row = i + 1;

		// copy previous row
		for (auto k = 0; k <= limit; ++k)
			V[row][k] = V[row-1][k];

		for (auto k = items[i].second; k <= limit; ++k) {
			V[row][k] = std::max(
							   V[row-1][k],
							   items[i].first + V[row-1][k - items[i].second]);
		}
	}


	// need to put in backtracking
	size_t ans_size = V[items.size()][limit];
	std::vector<int> ans(ans_size);

	return ans;
}

std::multiset<int> equal_partitions(const std::vector<int>& s);

/*  Suppose you are a programmer for a vending machine manufacturer. Your
    company wants to streamline effort by giving out the fewest possible coins
    in change for each transaction
    Coins: 1, 5, 10, 21(!), and 25 */

int minimum_coins(std::vector<int>& types, int limit, int change)
{
	std::sort(types.begin(), types.end());

	std::vector<int> ctable(limit+1);
	for (auto k = 0; k <= limit; ++k) {
		if (k % types[0] == 0) ctable[k] = k/types[0];
	}

	for (auto i = 1; i < types.size(); ++i) {
		for (auto k = types[i]; k <= limit; ++k) {
			ctable[k] = std::min(ctable[k],
					             ctable[k-types[i]]+1);
		}
	}

	return ctable[change];
}

int coin_sums(std::vector<int>& types, int limit)
{
	std::vector<int> ctable(limit+1);

	for (auto k = 0; k <= limit; ++k) {
		if (k % types[0] == 0) ctable[k] = 1;
	}

	for (auto i = 1; i < types.size(); ++i) {
		for (auto k = types[i]; k <= limit; ++k) {
			ctable[k] += ctable[k-types[i]];
		}
	}

	return ctable[limit];
}

int path_sum_two_ways(const std::vector<std::vector<int> >& matrix, int r, int c)
{
	// create matrix to hold path sums
	std::vector<std::vector<int>> s(r, std::vector<int>(c));

	s[0][0] = matrix[0][0];
	for (auto k = 1; k < c; ++k)
		s[0][k] = matrix[0][k] + s[0][k-1];

	for (auto k = 1; k < r; ++k) {
		s[k][0] = matrix[k][0] + s[k-1][0];
	}

	for (auto row = 1; row < r; ++row) {
		for (auto col = 1; col < c; ++col) {
			s[row][col] = matrix[row][col] + std::min(s[row-1][col], s[row][col-1]);
		}
	}

	return s[r-1][c-1];
}

unsigned max_path_sum_tri(std::vector<std::vector<unsigned>>& triangle)
{
	std::vector<unsigned> prev(triangle[0]);

	for (auto k = 1; k < triangle.size(); ++k) {
		auto len = prev.size() + 1;
		std::vector<unsigned> current(len);
		current[0] = prev[0] + triangle[k][0];
		current[len-1] = prev[len-2] + triangle[k][len-1];

		for (auto i = 1; i < len-1; ++i) {
			current[i] = triangle[k][i] + std::max(prev[i-1], prev[i]);
		}

		prev = current;
	}

	return *std::max_element(prev.begin(), prev.end());
}
