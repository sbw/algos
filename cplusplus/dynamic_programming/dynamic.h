#include <vector>
#include <set>
#include <string>
#include <algorithm>
#include <numeric>

// ------------------- Dynamic Programming Problems -----------------

bool subset_sum(const std::vector<int>& set, int k, std::vector<int>& result);

bool partition_problem(const std::vector<int>& set, 
                                        std::vector<int>& result);
void pascals_triangle(unsigned nth);

unsigned fib_brute(unsigned nth);

int fib_memo(int nth);

int fib_memo_helper(std::vector<int>& FN, int nth);

int lcs_brute(const char* s, const char* t);

int lcs_recur(const std::string& s, const std::string& t);

int lcs_recur_helper(std::vector<std::vector<int>>& L,
		             const std::string& s,
					 size_t s_index,
					 const std::string& t,
					 size_t t_index);

int lcs(const std::string& s, const std::string& t);

std::string lcs_pattern(const std::string& s, const std::string& t);

int longest_increasing_subsequence(const std::vector<int>& s);

int longest_increasing_subsequence2(const std::vector<int>& s);

int longest_increasing_subsequence3(const std::vector<int>& s, 
                                        std::vector<int>& result);
// Levenshtein
int edit_distance_l(const std::string& s, const std::string& t);

// Damerau-Levenshtein
int edit_distance_dl(const std::string& s, const std::string& t);

std::vector<int> zero_one_knapsack(const std::vector<std::pair<int,int>>& items, int limit);

std::multiset<int> equal_partitions(const std::vector<int>& s);

/*  Suppose you are a programmer for a vending machine manufacturer. Your
    company wants to streamline effort by giving out the fewest possible coins
    in change for each transaction
    Coins: 1, 5, 10, 21(!), and 25 */

int minimum_coins(std::vector<int>& types, int limit, int change);

int coin_sums(std::vector<int>& types, int limit);

int path_sum_two_ways(const std::vector<std::vector<int> >& matrix, int n, int m);

unsigned max_path_sum_tri(std::vector<std::vector<unsigned>>& triangle);
