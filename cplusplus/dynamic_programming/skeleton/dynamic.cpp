#include "dynamic.h"

// ------------------- Dynamic Programming Problems -----------------

unsigned fib(unsigned nth)
{
    return 0;
}

void pascals_triangle(unsigned nth)
{

}

bool subset_sum(const std::vector<int>& s, int k, std::vector<int>& result)
{
    return true;
}

bool partition_problem(const std::vector<int>& set, std::vector<int>& result)
{
    return true;
}

std::string lcs(const std::string& s, const std::string& t)
{ 
    return std::string();
}

int lcs2(const std::vector<int>& s, const std::vector<int>& t);

// longest increasing subsequence
int longest_increasing_subsequence(const std::vector<int>& s)
{    
    return 0;
}

int longest_increasing_subsequence2(const std::vector<int>& s);

int longest_increasing_subsequence3(const std::vector<int>& set, 
                                        std::vector<int>& result);
// Levenshtein
int edit_distance_l(const std::string& s, const std::string& t);

// Damerau-Levenshtein
int edit_distance_dl(const std::string& s, const std::string& t);

// knapsack
std::vector<int> zero_one_knapsack(const std::vector<int>& v,
              const std::vector<int>& w, const int& limit, int& greatest_value)
{
    return std::vector<int>();
}

std::multiset<int> equal_partitions(const std::vector<int>& s);

/*  Suppose you are a programmer for a vending machine manufacturer. Your
    company wants to streamline effort by giving out the fewest possible coins
    in change for each transaction
    Coins: 1, 5, 10, 21(!), and 25 */

int coins(const std::vector<int>& types, int limit, int change)
{
    return 0;
}
