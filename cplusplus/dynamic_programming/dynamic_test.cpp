#include "dynamic.h"
#include "gtest/gtest.h"

namespace {

class DynamicTest : public ::testing::Test {
protected:
    DynamicTest() {}
    virtual ~DynamicTest() {}

    void SetUp() override
    {       
    }

    void TearDown() override
    {
    }

};

TEST_F(DynamicTest, subset_sumPass)
{
    std::vector<int> set = { -4, 8, -3, 5, -2, -7, -12 };
    std::vector<int> expected = { -3, -2, 5 };
    std::vector<int> result;

    ASSERT_TRUE(subset_sum(set, 0, result));

    for (auto item : result)
        std::cout << item << std::endl;

    ASSERT_TRUE(expected == result);
}

TEST_F(DynamicTest, subset_sumFail)
{
    std::vector<int> set = { -4, 32, -3, 19, -2, -7, -1 };
    std::vector<int> result;

    ASSERT_FALSE(subset_sum(set, 0, result));
    ASSERT_TRUE(result.empty());
}

TEST_F(DynamicTest, lcs_bruteFive)
{
    std::string s = "bestarwalker";
    std::string t = "cccstarbucks";
    int length = lcs_brute(s.c_str(), t.c_str());
    ASSERT_EQ(5, length);
}

TEST_F(DynamicTest, lcs_bruteSeven)
{
    std::string s = "nematode knowledge";
    std::string t = "empty bottle";
    int length = lcs_brute(s.c_str(), t.c_str());
    ASSERT_EQ(7, length);
}

TEST_F(DynamicTest, lcs_bruteNoCommon)
{
    std::string s = "xxxxxxxx";
    std::string t = "yyyyyy";
    int length = lcs_brute(s.c_str(), t.c_str());
    ASSERT_EQ(0, length);
}

TEST_F(DynamicTest, lcs_bruteEmpty)
{
    std::string s = "";
    std::string t = "yyyyyy";
    int length = lcs_brute(s.c_str(), t.c_str());
    ASSERT_EQ(0, length);
}

TEST_F(DynamicTest, lcs_recurFive)
{
    std::string s = "bestarwalker";
    std::string t = "cccstarbucks";
    int length = lcs_recur(s, t);
    ASSERT_EQ(5, length);
}

TEST_F(DynamicTest, lcs_recurSeven)
{
    std::string s = "nematode knowledge";
    std::string t = "empty bottle";
    int length = lcs_recur(s, t);
    ASSERT_EQ(7, length);
}

TEST_F(DynamicTest, lcs_recurNoCommon)
{
    std::string s = "xxxxxxxx";
    std::string t = "yyyyyy";
    int length = lcs_recur(s, t);
    ASSERT_EQ(0, length);
}

TEST_F(DynamicTest, lcs_recurEmpty)
{
    std::string s = "";
    std::string t = "yyyyyy";
    int length = lcs_recur(s, t);
    ASSERT_EQ(0, length);
}

TEST_F(DynamicTest, lcsFive)
{
    std::string s = "bestarwalker";
    std::string t = "cccstarbucks";
    int length = lcs(s, t);
    ASSERT_EQ(5, length);
}

TEST_F(DynamicTest, lcsSeven)
{
    std::string s = "nematode knowledge";
    std::string t = "empty bottle";
    int length = lcs(s, t);
    ASSERT_EQ(7, length);
}

TEST_F(DynamicTest, lcsNoCommon)
{
    std::string s = "xxxxxxxx";
    std::string t = "yyyyyy";
    int length = lcs(s, t);
    ASSERT_EQ(0, length);
}

TEST_F(DynamicTest, lcsEmpty)
{
    std::string s = "";
    std::string t = "yyyyyy";
    int length = lcs(s, t);
    ASSERT_EQ(0, length);
}

TEST_F(DynamicTest, lcs_pattern_stark)
{
    std::string s = "bestarwalker";
    std::string t = "cccstarbucks";
    std::string expected = "stark";

    std::string result = lcs_pattern(s, t);
    ASSERT_EQ(expected, result);
}

TEST_F(DynamicTest, lcs_pattern_emt_ole)
{
    std::string s = "nematode knowledge";
    std::string t = "empty bottle";
    std::string expected = "emt ole";

    std::string result = lcs_pattern(s, t);
    ASSERT_EQ(expected, result);
}

TEST_F(DynamicTest, partition_problemPass)
{
    std::vector<int> set = { 6, 2, 3, 7, 5, 1, 4 };
    std::vector<int> expected = { 2, 5, 7 };
    std::vector<int> result;

    ASSERT_TRUE(partition_problem(set, result));
    for (size_t i = 0; i < result.size(); ++i)
        std::cout << result[i] << std::endl;
    
    ASSERT_TRUE(result == expected);
}

TEST_F(DynamicTest, pascals_triangle)
{
    pascals_triangle(5);
}

TEST_F(DynamicTest, fib_brute)
{
    ASSERT_EQ(55, fib_brute(10));
}

TEST_F(DynamicTest, fib_memo)
{
    ASSERT_EQ(55, fib_memo(10));
}

TEST_F(DynamicTest, longest_increasing_subsequence)
{
    std::vector<int> set = { 1, 0, 7, 2, 8, 3, 4, 9 };
    ASSERT_EQ(5, longest_increasing_subsequence(set));
}

TEST_F(DynamicTest, minimum_coins)
{
    std::vector<int> types = { 5, 10, 1, 21, 25 };
    ASSERT_EQ(2, minimum_coins(types, 100, 6));
    ASSERT_EQ(3, minimum_coins(types, 100, 63));
    ASSERT_EQ(3, minimum_coins(types, 100, 47));
    ASSERT_EQ(3, minimum_coins(types, 100, 71));
}

TEST_F(DynamicTest, coin_sums)
{
    std::vector<int> coins = { 10, 2, 50, 1, 20, 5, 100, 200 };

    ASSERT_EQ(73682, coin_sums(coins, 200));
}

TEST_F(DynamicTest, zero_one_knapsackSeven)
{
    std::vector<std::pair<int,int>> items = {
    						{ 3, 2 },
							{ 4, 3 },
							{ 5, 4 },
							{ 6, 5 },
						 };
    int limit = 5;
    std::vector<int> ans = zero_one_knapsack(items, limit);

    ASSERT_EQ(7, ans.size());
}

TEST_F(DynamicTest, zero_one_knapsackNinty)
{
    std::vector<std::pair<int,int>> items = {
    						{ 10, 5 },
							{ 40, 4 },
							{ 30, 6 },
							{ 50, 3 },
						 };
    int limit = 10;
    std::vector<int> ans = zero_one_knapsack(items, limit);

    ASSERT_EQ(90, ans.size());
}

TEST_F(DynamicTest, maximum_path_sum_demo)
{
    std::vector<std::vector<unsigned>> triangle(4);
    triangle[0].push_back(3);
    triangle[1].push_back(7);
    triangle[1].push_back(4);
    triangle[2].push_back(2);
    triangle[2].push_back(4);
    triangle[2].push_back(6);
    triangle[3].push_back(8);
    triangle[3].push_back(5);
    triangle[3].push_back(9);
    triangle[3].push_back(3);

    ASSERT_EQ(23, max_path_sum_tri(triangle));
}

TEST_F(DynamicTest, path_sum_two_ways_demo)
{
    std::vector<std::vector<int> > matrix(5, std::vector<int>(5));
    matrix[0][0] = 131;
    matrix[0][1] = 673;
    matrix[0][2] = 234;
    matrix[0][3] = 103;
    matrix[0][4] = 18;

    matrix[1][0] = 201;
    matrix[1][1] = 96;
    matrix[1][2] = 342;
    matrix[1][3] = 965;
    matrix[1][4] = 150;

    matrix[2][0] = 630;
    matrix[2][1] = 803;
    matrix[2][2] = 746;
    matrix[2][3] = 422;
    matrix[2][4] = 111;

    matrix[3][0] = 537;
    matrix[3][1] = 699;
    matrix[3][2] = 497;
    matrix[3][3] = 121;
    matrix[3][4] = 956;

    matrix[4][0] = 805;
    matrix[4][1] = 732;
    matrix[4][2] = 524;
    matrix[4][3] = 37;
    matrix[4][4] = 331;

    ASSERT_EQ(2427, path_sum_two_ways(matrix, matrix.size(), matrix[0].size()));
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
