#include <iostream>
#include <vector>


// this looks to be a dynamic programming problem
// note that maximum number of homes along a street is not specified
// maximum home value is not specificed either
// this would not matter if done in Python as it would just switch
// to bigint mode behind the scenes
// the assumption here is that all values will fit into a 32-bit signed
// integer
// another assumption is 1-D array (houses only along one side of street)


// compiled as g++ --std=c++11 switchco.cpp -o switchco
// test on Linux in manner similar to uva online judge 
// array is single line in text file that is redirected to stdin
// 
// ./switchco < array.txt
//

// test cases and (expected)
// 20 10 50 5 1         (71)
// 20 50 10 1 5         (55)
// 20 20                (20)
// 40 40 30 20          (70)
// 20 20 20             (40)
// 10 50 20             (50)
// 5 10                 (10)
// 10 5                 (10)
// 10                   (10)

// final note the input is not sanitized so the expectation is
// data file is correct

int max_ill_gotten_gains(const std::vector<int>& home_values)
{
    if (home_values.empty()) return 0;
    
    std::vector<int> total(home_values.size());
    total[0] = home_values[0];
    bool robbed_prev = true;
   
    for (size_t i = 1; i < home_values.size(); ++i) { 
        if (robbed_prev) {
            total[i] = std::max(total[i-1], 
                                total[i-1] - home_values[i-1] + home_values[i]);
            if (total[i] == total[i-1]) 
                robbed_prev = false;
        }
        else {
            total[i] = total[i-1] + home_values[i];
            robbed_prev = true;
        } 
    }
    return total[home_values.size()-1];
}

int main(int argc, char* argv[])
{
    int n;
    std::vector<int> home_values;
    while (std::cin >> n) 
        home_values.push_back(n);

    std::cout << "Loot: " << max_ill_gotten_gains(home_values) << std::endl;

    return 0;  
}
