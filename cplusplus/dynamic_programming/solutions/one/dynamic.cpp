#include "dynamic.h"

// ------------------- Dynamic Programming Problems -----------------

unsigned fib(unsigned nth)
{
    if (nth == 0) return 0;
    if (nth == 1) return 1;

    std::vector<unsigned> memo(nth+1);
    memo[0] = 0;
    memo[1] = 1;

    for (size_t i = 2; i <= nth; ++i) {
        memo[i] = memo[i-2] + memo[i-1];
    }

    return memo[nth];
}

void pascals_triangle(unsigned nth)
{
    std::vector<std::vector<unsigned> > triangle;
    for (size_t n = 0; n <= nth; ++n) {
        triangle.push_back(std::vector<unsigned>(n+1, 1));
        for (size_t k = 1; k < n; ++k)
            triangle[n][k] = triangle[n-1][k-1] + triangle[n-1][k];
    }

    for (size_t n = 0; n <= nth; ++n) {
        for (size_t k = 0; k <= n; ++k)
            std::cout << triangle[n][k] << '\t';
        std::cout << std::endl;
    }
}

bool subset_sum(const std::vector<int>& s, int k, std::vector<int>& result)
{
    // check size -- assumption need at least two values
    if (s.size() < 2) return false;

    int pos = 0;
    int neg = 0;

    for (size_t i = 0; i < s.size(); ++i) {
        if (s[i] < 0) 
            neg += s[i];
        else
            pos += s[i];
    }
    
    int col_size = pos - neg + 1;
    int offset = -1*neg;

    std::vector< std::vector<bool> > table(s.size()+1, 
                                           std::vector<bool>(col_size));

    for (size_t i = 1; i < s.size(); ++i) {
        table[i] = std::vector<bool>(table[i-1]);
        table[i][offset+s[i]] = true; 
        for (int j = neg; j <= pos; ++j) {
            // remember to bounds check
            if (!table[i][offset+j] && j-s[i] >= neg && j-s[i] <= pos)
                table[i][offset+j] = table[i-1][offset+j-s[i]];
        }
    }

    // backtracking
    bool found = false;
    size_t i = 0;
    int sum = k;
    while (i < s.size()) {
        if (table[i][offset+sum]) 
        {
            sum -= s[i];
            result.push_back(s[i]);
            if (sum == 0) 
            { 
                found = true;
                std::sort(result.begin(), result.end());
                break;
            }

            i = 0;
            continue;
        }
        ++i;
    }

    return found;
}

bool partition_problem(const std::vector<int>& set, std::vector<int>& result)
{
    int sum = std::accumulate(set.begin(), set.end(), 0);
    if (sum % 2 != 0) return false;

    return subset_sum(set, sum/2, result);
}

std::string lcs(const std::string& s, const std::string& t)
{
    std::string result;

    std::vector<std::vector<size_t> > L(s.size()+1, 
                                        std::vector<size_t>(t.size()+1));

    for (size_t i = 1; i <= s.size(); ++i) 
        for (size_t j = 1; j <= t.size(); ++j)        
            L[i][j] = (s[i-1] == t[j-1]) ? 1 + L[i-1][j-1] :
                                           std::max(L[i-1][j], L[i][j-1]);
            
    for (size_t i = 0; i <= s.size(); ++i) {
        for (size_t j = 0; j <= t.size(); ++j)
            std::cout << L[i][j] << ' ';
        std::cout << std::endl;
    }
     
    for (size_t i = s.size(), j = t.size(); i > 0 && j > 0; ) {
        if (L[i-1][j] == L[i][j-1]) 
        {
            if (L[i-1][j-1] != L[i][j]) result += s[i-1];
            --i;
            --j;
        }
        else if (L[i-1][j] < L[i][j-1])
        {
            --j;
        }
        else
        {
            --i;
        }
    }

    std::reverse(result.begin(), result.end()); 
    return result;
}

int lcs2(const std::vector<int>& s, const std::vector<int>& t);

// longest increasing subsequence
int longest_increasing_subsequence(const std::vector<int>& s)
{    
    if (s.empty()) return -1;

    std::vector<int> longest(s.size(), 1);

    for (size_t i = 1; i < s.size(); ++i) {
        for (size_t j = 0; j < i; ++j) {
            if (s[j] < s[i])
                longest[i] = std::max(longest[j], 1 + longest[j]);
        }
    }

    return *std::max_element(longest.begin(), longest.end());
}

int longest_increasing_subsequence2(const std::vector<int>& s);

int longest_increasing_subsequence3(const std::vector<int>& set, 
                                        std::vector<int>& result);
// Levenshtein
int edit_distance_l(const std::string& s, const std::string& t);

// Damerau-Levenshtein
int edit_distance_dl(const std::string& s, const std::string& t);

// knapsack
std::vector<int> zero_one_knapsack(const std::vector<int>& v,
              const std::vector<int>& w, const int& limit, int& greatest_value)
{
    // check size, etc?
    
    std::vector<int> results;
    greatest_value = 0;
    std::pair<size_t, size_t> final_item;
    std::vector<std::vector<int> > value_table(v.size()+1, 
                                                std::vector<int>(limit+1));
    std::vector<std::vector<bool> > keep(v.size()+1, 
                                                std::vector<bool>(limit+1));

    for (size_t i = 1; i <= v.size(); ++i) {
        value_table[i] = value_table[i-1];
        for (size_t j = 1; j <= limit; ++j) {

            if (j < w[i-1]) continue;
            
            value_table[i][j] = std::max(value_table[i-1][j],
                                         v[i-1] + value_table[i-1][j-w[i-1]]);
            if (greatest_value < value_table[i][j])
            {
                greatest_value = value_table[i][j];
                final_item = std::make_pair(i, j);
                keep[i][j] = true;
            }
        }
    }
  
    // backtracking to get set 
    int gv = greatest_value;
    size_t i = final_item.first;
    size_t j = final_item.second;
    while (i > 0 && gv != 0) {
        if (keep[i][j])
        {
            results.push_back(i);
            gv -= v[i-1]; 
            j -= w[i-1];
        }
        --i;
    }
    
    return results;
}

std::multiset<int> equal_partitions(const std::vector<int>& s);

/*  Suppose you are a programmer for a vending machine manufacturer. Your
    company wants to streamline effort by giving out the fewest possible coins
    in change for each transaction
    Coins: 1, 5, 10, 21(!), and 25 */

int minimum_coins(const std::vector<int>& types, int limit, int change)
{
    std::vector<std::pair<int, int> > num_coins(limit+1);
    num_coins[0] = std::make_pair(0, 0);

    for (int i = 1; i < num_coins.size(); ++i) {
        int min = num_coins.size();
        int min_index = 0;

        for (size_t j = 0; j < types.size(); ++j) {
            int prev = i - types[j];
            if (prev < 0) break;
      
            if (min > 1 + num_coins[prev].first)
            {
                min = 1 + num_coins[prev].first;
                min_index = j;
            }
        }
        num_coins[i] = std::make_pair(min, types[min_index]);
    }


    for (size_t i = 0; i < num_coins.size(); ++i)
    {
        std::cout << i << ", " << num_coins[i].first << ", " 
                << num_coins[i].second << std::endl; 
    }

    return num_coins[change].first;
}


