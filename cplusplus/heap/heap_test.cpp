#include "heap.h"
#include "gtest/gtest.h"

namespace {

class HeapTest : public ::testing::Test {
protected:
    HeapTest() {}
    virtual ~HeapTest() {}

    virtual void SetUp() 
    {
    }

    virtual void TearDown() 
    {
    }

    std::vector<int> heap;

};

TEST_F(HeapTest, heapify1)
{
    int input[] = { 20, 30, 50, 60, 70, 80, 90, 100 };
    std::vector<int> vec(input, input + sizeof(input)/sizeof(int));

    heapify(vec);

    for (size_t i = 0; i < vec.size(); ++i)
        std::cout << vec[i] << ' ';
    std::cout << std::endl;    
}

TEST_F(HeapTest, insert1)
{
    int input[] = { 20, 30, 50, 60, 70, 80, 90, 100 };
    std::vector<int> vec(input, input + sizeof(input)/sizeof(int));

    heapify(vec);
    insert(vec, 65);

    for (size_t i = 0; i < vec.size(); ++i)
        std::cout << vec[i] << ' ';
    std::cout << std::endl;    
}

TEST_F(HeapTest, insert2)
{
    int input[] = { 20, 30, 50, 60, 70, 80, 90, 100 };
    std::vector<int> vec(input, input + sizeof(input)/sizeof(int));

    heapify(vec);
    insert(vec, 110);

    for (size_t i = 0; i < vec.size(); ++i)
        std::cout << vec[i] << ' ';
    std::cout << std::endl;    
}

TEST_F(HeapTest, insert3)
{
    int input[] = { 20, 30, 50, 60, 70, 80, 90, 100 };
    std::vector<int> vec(input, input + sizeof(input)/sizeof(int));

    heapify(vec);
    insert(vec, 10);

    for (size_t i = 0; i < vec.size(); ++i)
        std::cout << vec[i] << ' ';
    std::cout << std::endl;    
}

TEST_F(HeapTest, remove_max)
{
    int input[] = { 20, 30, 50, 60, 70, 80, 90, 100 };
    std::vector<int> vec(input, input + sizeof(input)/sizeof(int));
    std::vector<int> expected(vec.begin(), vec.end());
    std::sort(expected.begin(), expected.end(), std::greater<int>());

    heapify(vec);
    
    for (size_t i = 0; i < vec.size(); ++i) {
        ASSERT_EQ(expected[i], find_max(vec));
        remove_max(vec);   
    }
}

TEST_F(HeapTest, heap_sort)
{
    int input[] = { 20, 30, 50, 60, 70, 80, 90, 100 };
    std::vector<int> vec(input, input + sizeof(input)/sizeof(int));
    std::vector<int> expected(vec.begin(), vec.end());

    heap_sort(vec);
    
    ASSERT_EQ(vec.size(), expected.size());
    ASSERT_TRUE(std::equal(vec.begin(), vec.end(), expected.begin()));
}


} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
