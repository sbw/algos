#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>

// ------------------------- Core Heap ------------------------------

size_t left_child(size_t parent);
size_t right_child(size_t parent);
size_t parent(size_t child);

template <typename T>
void heapify(std::vector<T>& vec);

template <typename T>
void insert(std::vector<T>& heap, T data);

template <typename T>
T find_max(const std::vector<T>& heap);

template <typename T>
void remove_max(std::vector<T>& heap);

// ----------------------- Heap Problems ----------------------------

void enumerate_powers(const std::set<unsigned>& set,
    size_t num_powers,
    std::vector<int>& out);

/*  Numbers are randomly generated and passed to a method. Write a program
    to find and maintain the median value as new values are generated. */
int maintain_median();

/*  Describe an algorithm to find the largest k numbers in >k numbers,
    say 1 million numbers in 1 billion numbers */

void find_topk(std::istream& in, size_t k, std::vector<int>& heap);

/* HeapSort */
void heap_sort(std::vector<int>& vec); 
