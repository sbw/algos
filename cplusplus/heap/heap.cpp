#include "heap.h"

// ------------------------- Core Heap ------------------------------

size_t left_child(size_t parent)
{
    return 2*parent + 1;
}

size_t right_child(size_t parent)
{
    return 2*parent + 2;
}

size_t parent(size_t child)
{
    return (child) ? (child-1)/2 : 0;
}

template <typename T>
void heapify(std::vector<T>& vec)
{
    for (size_t i = 0; i < vec.size(); ++i) {
        size_t j = i;
        while (vec[parent(j)] < vec[j])
        {
            std::swap(vec[parent(j)], vec[j]);
            j = parent(j);
        }
    }
}

template <typename T>
void insert(std::vector<T>& heap, T data)
{
    heap.push_back(data);
    
    size_t i = heap.size()-1;
    while (heap[parent(i)] < heap[i]) {
        std::swap(heap[parent(i)], heap[i]);
        i = parent(i);
    }
}

template <typename T>
T find_max(const std::vector<T>& heap)
{
    return heap[0];
}

template <typename T>
void remove_max(std::vector<T>& heap)
{   
    heap[0] = heap[heap.size()-1];
    heap.resize(heap.size()-1);

    size_t i = 0;
    while (i < heap.size()) {
        size_t child = left_child(i);
        size_t right = right_child(i);

        if (child >= heap.size()) break;
        if (right < heap.size() && heap[right] > heap[child])
            child = right;

        if (heap[i] >= heap[child]) break;

        std::swap(heap[child], heap[i]);   
        i = child;
    }
}

// ----------------------- Heap Problems ----------------------------

void enumerate_powers(const std::set<unsigned>& set,
    size_t num_powers,
    std::vector<int>& out);

/*  Numbers are randomly generated and passed to a method. Write a program
    to find and maintain the median value as new values are generated. */
int maintain_median();

/*  Describe an algorithm to find the largest k numbers in >k numbers,
    say 1 million numbers in 1 billion numbers */

void find_topk(std::istream& in, size_t k, std::vector<int>& heap);

/* HeapSort */
void heap_sort(std::vector<int>& vec)
{
    if (vec.empty()) return;

    std::make_heap(vec.begin(), vec.end());
    for (size_t i = vec.size(); i > 0; --i) 
        std::pop_heap(vec.begin(), vec.begin()+i);
}

template void heapify(std::vector<int>& vec); 
template void insert(std::vector<int>& heap, int data);
template int find_max(const std::vector<int>& heap);
template void remove_max(std::vector<int>& heap);
