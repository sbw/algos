#include "hash.h"
#include <vector>
#include <string>
#include <list>
#include <map>

size_t hash_simple(const std::string& str);

size_t hash_not_constant(const std::string& str);

size_t hash_complex(const std::string& str);

size_t hash_func(const std::string& str)
{
    if (str.empty()) return 0;
    return str[0] - 'a';
}

void insert_no_collision(std::vector<Item*>& table, Item* entry)
{
    size_t hash = hash_func(entry->key) % table.size();
    table[hash] = entry;
}

Item* find_no_collision(std::vector<Item*>& table, const std::string& key)
{
    size_t hash = hash_func(key) % table.size();
    if (table[hash] && table[hash]->key == key)
        return table[hash];
    return NULL;
}

void resize_no_collision(std::vector<Item*>& table, size_t size)
{
    std::vector<Item*> new_table(size);
    for (size_t i = 0; i < table.size(); ++i) {
        if (!table[i]) continue;
        size_t new_hash = hash_func(table[i]->key) % new_table.size();
        new_table[new_hash] = table[i];
    }

    table.swap(new_table);
}

// separate chaining

void insert(std::vector<std::vector<Item*> >&  table, Item* entry)
{
    size_t hash = hash_func(entry->key) % table.size();
    table[hash].push_back(entry);
}

Item* find(std::vector<std::vector<Item*>>& table, const std::string& key)
{
    size_t hash = hash_func(key) % table.size();
    for (size_t i = 0; i < table[hash].size(); ++i)
       if (table[hash][i]->key == key)
           return  table[hash][i];
    return NULL;
}

// open addressing

void insert_open(std::vector<Item*>& table, Item* entry)
{
    size_t hash = hash_func(entry->key) % table.size();
    
    while (true) {
        for (size_t i = 0; i < table.size(); ++i) {
            size_t offset = (i+hash) % table.size();
            if (!table[offset]) 
            {
                table[offset] = entry;
                return;
            }
        }
    
        table.resize(2*table.size());
    }
}

Item* find_open(std::vector<Item*>& table, const std::string& key)
{
    size_t hash = hash_func(key) % table.size();
    for (size_t i = 0; i < table.size(); ++i) {
        size_t offset = (i+hash) % table.size();
        if (table[offset]->key == key)
            return table[offset];
    }
    
    return NULL;
}

std::string find_most_common_element(std::istream& stream, 
                                         std::map<std::string, size_t>& table);
