#include "hash.h"
#include "gtest/gtest.h"

namespace {

class HashTest : public ::testing::Test {
protected:
    HashTest() {}
    virtual ~HashTest() {}

    virtual void SetUp() 
    {
    }

    virtual void TearDown() 
    {
    }

};

/*
TEST_F(HashTest, insert_no_collision)
{
    std::map<std::string, int> items;
    items["a"] = 0;
    items["b"] = 4;
    items["c"] = 7;
    items["d"] = 3;
    items["e"] = 1;
    items["f"] = 5;
    items["g"] = 6;
    items["h"] = 2;

    std::vector<Item*> hash_table(26);

    std::map<std::string, int>::const_iterator iter = items.begin();
    for (; iter != items.end(); ++iter) {
        Item* item = new Item(iter->first, iter->second);
        insert_no_collision(hash_table, item);
    }

    //ASSERT_EQ(hash_table.size(), items.size());

    for (iter = items.begin(); iter != items.end(); ++iter) {
        int n = hash_table[hash_func(iter->first)]->value;
        ASSERT_EQ(iter->second, n);
    }

    // cleanup memory
    for (size_t i = 0; i < hash_table.size(); ++i) 
        delete hash_table[i];
}

TEST_F(HashTest, find_no_collision)
{
    std::map<std::string, int> items;
    items["a"] = 0;
    items["b"] = 4;
    items["c"] = 7;
    items["d"] = 3;
    items["e"] = 1;
    items["f"] = 5;
    items["g"] = 6;
    items["h"] = 2;

    std::vector<Item*> hash_table(26);

    std::map<std::string, int>::const_iterator iter = items.begin();
    for (; iter != items.end(); ++iter) {
        Item* item = new Item(iter->first, iter->second);
        insert_no_collision(hash_table, item);
    }

    for (iter = items.begin(); iter != items.end(); ++iter) {
        Item* item = find_no_collision(hash_table, iter->first);
        ASSERT_EQ(iter->second, item->value);
    }

    // cleanup memory
    for (size_t i = 0; i < hash_table.size(); ++i) 
        delete hash_table[i];
}

TEST_F(HashTest, no_collisions_resize)
{
    std::map<std::string, int> items;
    items["a"] = 0;
    items["b"] = 4;
    items["c"] = 7;
    items["d"] = 3;
    items["e"] = 1;
    items["f"] = 5;
    items["g"] = 6;
    items["h"] = 2;

    std::vector<Item*> hash_table(10);

    std::map<std::string, int>::const_iterator iter = items.begin();
    for (; iter != items.end(); ++iter) {
        Item* item = new Item(iter->first, iter->second);
        insert_no_collision(hash_table, item);
    }

    resize_no_collision(hash_table, 26);

    for (iter = items.begin(); iter != items.end(); ++iter) {
        Item* item = find_no_collision(hash_table, iter->first);
        ASSERT_EQ(iter->second, item->value);
    }

    // cleanup memory
    for (size_t i = 0; i < hash_table.size(); ++i) 
        delete hash_table[i];
}
*/

TEST_F(HashTest, insert_separate_chaining)
{
    std::map<std::string, int> items;
    items["a"] = 0;
    items["b"] = 4;
    items["c"] = 7;
    items["d"] = 3;
    items["e"] = 1;
    items["f"] = 5;
    items["g"] = 6;
    items["h"] = 2;

    items["a2"] = 20 + 0;
    items["b2"] = 20 + 4;
    items["c2"] = 20 + 7;
    items["d2"] = 20 + 3;
    items["e2"] = 20 + 1;
    items["f2"] = 20 + 5;
    items["g2"] = 20 + 6;
    items["h2"] = 20 + 2;
    
    items["a3"] = 30 + 0;
    items["b3"] = 30 + 4;
    items["c3"] = 30 + 7;
    items["d3"] = 30 + 3;
    items["e3"] = 30 + 1;
    items["f3"] = 30 + 5;
    items["g3"] = 30 + 6;
    items["h3"] = 30 + 2;

    std::vector<std::vector<Item*> > hash_table(10);

    std::map<std::string, int>::const_iterator iter = items.begin();
    for (; iter != items.end(); ++iter) {
        Item* item = new Item(iter->first, iter->second);
        insert(hash_table, item);
    }

    // cleanup memory
    for (size_t i = 0; i < hash_table.size(); ++i) 
        for (size_t j = 0; j < hash_table[i].size(); ++j)
            delete hash_table[i][j];
}

TEST_F(HashTest, find_separate_chaining)
{
    std::map<std::string, int> items;
    items["a"] = 0;
    items["b"] = 4;
    items["c"] = 7;
    items["d"] = 3;
    items["e"] = 1;
    items["f"] = 5;
    items["g"] = 6;
    items["h"] = 2;

    items["a2"] = 20 + 0;
    items["b2"] = 20 + 4;
    items["c2"] = 20 + 7;
    items["d2"] = 20 + 3;
    items["e2"] = 20 + 1;
    items["f2"] = 20 + 5;
    items["g2"] = 20 + 6;
    items["h2"] = 20 + 2;
    
    items["a3"] = 30 + 0;
    items["b3"] = 30 + 4;
    items["c3"] = 30 + 7;
    items["d3"] = 30 + 3;
    items["e3"] = 30 + 1;
    items["f3"] = 30 + 5;
    items["g3"] = 30 + 6;
    items["h3"] = 30 + 2;

    std::vector<std::vector<Item*> > hash_table(10);

    std::map<std::string, int>::const_iterator iter = items.begin();
    for (; iter != items.end(); ++iter) {
        Item* item = new Item(iter->first, iter->second);
        insert(hash_table, item);
    }

    for (iter = items.begin(); iter != items.end(); ++iter) {
        Item* item = find(hash_table, iter->first);
        ASSERT_TRUE(item);
        ASSERT_EQ(iter->second, item->value);
    }

    // cleanup memory
    for (size_t i = 0; i < hash_table.size(); ++i) 
        for (size_t j = 0; j < hash_table[i].size(); ++j)
        delete hash_table[i][j];
}

TEST_F(HashTest, insert_open_linear_probing)
{
    std::map<std::string, int> items;
    items["a"] = 0;
    items["b"] = 4;
    items["c"] = 7;
    items["d"] = 3;
    items["e"] = 1;
    items["f"] = 5;
    items["g"] = 6;
    items["h"] = 2;

    items["a2"] = 20 + 0;
    items["b2"] = 20 + 4;
    items["c2"] = 20 + 7;
    items["d2"] = 20 + 3;
    items["e2"] = 20 + 1;
    items["f2"] = 20 + 5;
    items["g2"] = 20 + 6;
    items["h2"] = 20 + 2;
    
    items["a3"] = 30 + 0;
    items["b3"] = 30 + 4;
    items["c3"] = 30 + 7;
    items["d3"] = 30 + 3;
    items["e3"] = 30 + 1;
    items["f3"] = 30 + 5;
    items["g3"] = 30 + 6;
    items["h3"] = 30 + 2;

    std::vector<Item*> hash_table(10);

    std::map<std::string, int>::const_iterator iter = items.begin();
    for (; iter != items.end(); ++iter) {
        Item* item = new Item(iter->first, iter->second);
        insert_open(hash_table, item);
    }

    // cleanup memory
    for (size_t i = 0; i < hash_table.size(); ++i) 
            delete hash_table[i];
}

TEST_F(HashTest, find_open_linear_probing)
{
    std::map<std::string, int> items;
    items["a"] = 0;
    items["b"] = 4;
    items["c"] = 7;
    items["d"] = 3;
    items["e"] = 1;
    items["f"] = 5;
    items["g"] = 6;
    items["h"] = 2;

    items["a2"] = 20 + 0;
    items["b2"] = 20 + 4;
    items["c2"] = 20 + 7;
    items["d2"] = 20 + 3;
    items["e2"] = 20 + 1;
    items["f2"] = 20 + 5;
    items["g2"] = 20 + 6;
    items["h2"] = 20 + 2;
    
    items["a3"] = 30 + 0;
    items["b3"] = 30 + 4;
    items["c3"] = 30 + 7;
    items["d3"] = 30 + 3;
    items["e3"] = 30 + 1;
    items["f3"] = 30 + 5;
    items["g3"] = 30 + 6;
    items["h3"] = 30 + 2;

    std::vector<Item*> hash_table(10);

    std::map<std::string, int>::const_iterator iter = items.begin();
    for (; iter != items.end(); ++iter) {
        Item* item = new Item(iter->first, iter->second);
        insert_open(hash_table, item);
    }

    std::cout << hash_table.size() << std::endl;

    for (iter = items.begin(); iter != items.end(); ++iter) {
        Item* item = find_open(hash_table, iter->first);
        ASSERT_TRUE(item);
        ASSERT_EQ(iter->second, item->value);
    }

    // cleanup memory
    for (size_t i = 0; i < hash_table.size(); ++i) 
        delete hash_table[i];
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
