#include <vector>
#include <string>
#include <list>
#include <map>

size_t hash_simple(const std::string& str);

size_t hash_not_constant(const std::string& str);

size_t hash_complex(const std::string& str);

size_t hash_func(const std::string& str);

struct Item {
    Item(std::string k, int v) : key(k), value(v) {}
    std::string key;
    int value;
};

void insert_no_collision(std::vector<Item*>& table, Item* entry);

Item* find_no_collision(std::vector<Item*>& table, const std::string& key);

void resize_no_collision(std::vector<Item*>& table, size_t size);

// separate chaining

void insert(std::vector<std::vector<Item*> >&  table, Item* entry);

Item* find(std::vector<std::vector<Item*> >& table, const std::string& key);

// open addressing

void insert_open(std::vector<Item*>& table, Item* entry);

Item* find_open(std::vector<Item*>& table, const std::string& key);

std::string find_most_common_element(std::istream& stream, 
                                         std::map<std::string, size_t>& table);

// most recently used cache

struct MRUCache {



};
