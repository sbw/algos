#include <vector>


struct vector {
    


};

typedef vector point;

struct line {
    vector a;
    vector u;
};

// kd-tree
struct node {
    


};

vector unit_vector(const vector& vect);

double distance_two_points(const vector& a, const vector& b);

double distance(const vector& p, const line& l);

bool parallel(const line& s, const line& t);

bool equals(const line& p, const line& q);

vector intersection(const line& p, const line& q);

class fixed_dim_compartor {


};

node* initialize(std::vector<vector> points);

vector find(node* kd_tree, const vector& point);

double area(const triangle& t);

bool point_in_triangle(const triangle& t, const point& d);

point center(const std::vector<point>& poly);

std::vector<point> convex_hull(const std::vector<point>& poly);

double sine_of_angle(point a, point b, point c);

double area(const std::vector<point>& p, const point& d);

bool point_in_polygon(const std::vector<point>& p. const point& d);


