

// ----------------- Threading Problems without shared state --------
void merging();

void quicksort();

void matrix_mult();

void max_find();

void first_pos();

// --------------- Threadings problems needing synchronization -----

void dining_philosophers();

void smokers_problem();

void producer_consumer();

void roller_coaster();
