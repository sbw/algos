#include <iostream>
#include <pthread.h>


int** arrA;
int** arrB;
int** arrC;

struct mult_args {
    size_t nth_row;
    size_t pth_col;
    size_t size;
};

void* single_mult(void* arg)
{
    struct mult_args* m = (struct mult_args*) arg;
    int sum = 0;

    for (size_t i = 0; i < m->size; ++i)
            sum += arrA[m->nth_row][i] * arrB[i][m->pth_col];
    
    arrC[m->nth_row][m->pth_col] = sum;
}

int** create_array(size_t rows, size_t cols)
{
    int** arr = new int*[rows];
    for (size_t row = 0; row < rows; ++row)
        arr[row] = new int[cols];

    return arr;
}

void print_array(int** arr, size_t rows, size_t cols)
{
    for (size_t row = 0; row < rows; ++row) {
        for (size_t col = 0; col < cols; ++col)
            std::cout << arr[row][col] << '\t';
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


void delete_array(int** arr, size_t rows)
{
    for (size_t row = 0; row < rows; ++row)
        delete [] arr[row];
    delete [] arr;
    arr = NULL;
}


int main(int argc, char* argv[])
{
    size_t rowsA = 5;
    size_t colsA = 5;
    size_t rowsB = 5;
    size_t colsB = 5;
   
    // two input arrays and output array
    arrA = create_array(rowsA, colsA);
    arrB = create_array(rowsB, colsB);
    arrC = create_array(rowsA, colsB);
    
    // fill array
    arrA[0][0] = -3;
    arrA[0][1] = -1;
    arrA[0][2] = -1;
    arrA[0][3] = -5;
    arrA[0][4] = 1;

    arrA[1][0] = -3;
    arrA[1][1] = -3;
    arrA[1][2] = -4;
    arrA[1][3] = -5;
    arrA[1][4] = 3;
    
    arrA[2][0] = -1;
    arrA[2][1] = -5;
    arrA[2][2] = 3;
    arrA[2][3] = -1;
    arrA[2][4] = -3;

    arrA[3][0] = 3;
    arrA[3][1] = 2;
    arrA[3][2] = -1;
    arrA[3][3] = -4;
    arrA[3][4] = -4;
    
    arrA[4][0] = -5;
    arrA[4][1] = 3;
    arrA[4][2] = -2;
    arrA[4][3] = -1;
    arrA[4][4] = -1;

    arrB[0][0] = 0;
    arrB[0][1] = 5;
    arrB[0][2] = 3;
    arrB[0][3] = -3;
    arrB[0][4] = 0;

    arrB[1][0] = 5;
    arrB[1][1] = 5;
    arrB[1][2] = 2;
    arrB[1][3] = 0;
    arrB[1][4] = -1;
    
    arrB[2][0] = 3;
    arrB[2][1] = 0;
    arrB[2][2] = -4;
    arrB[2][3] = -1;
    arrB[2][4] = -4;

    arrB[3][0] = 4;
    arrB[3][1] = 0;
    arrB[3][2] = -3;
    arrB[3][3] = 2;
    arrB[3][4] = 4;
    
    arrB[4][0] = 4;
    arrB[4][1] = -2;
    arrB[4][2] = 0;
    arrB[4][3] = -1;
    arrB[4][4] = 3;
    
    // print arrays
    print_array(arrA, rowsA, colsA);
    print_array(arrB, rowsB, colsB);

    // create thread arguments
    size_t new_elem = rowsA*colsB;
    struct mult_args* ma = new struct mult_args[new_elem];     
    for (size_t row = 0; row < rowsA; ++row) 
        for (size_t col = 0; col < colsB; ++col) {
            size_t index = row*colsB + col;
            ma[index].nth_row = row;
            ma[index].pth_col = col;
            ma[index].size = colsB;
        }

    // -------------- THREAD STUFF ------------------
    // create and join
    pthread_t threads[new_elem];
    for (size_t i = 0; i < new_elem ; ++i) {
        pthread_create(&threads[i], NULL, single_mult, &ma[i]);
        pthread_join(threads[i], NULL);
    }
        
    // --------------- END THREAD STUFF -------------

    print_array(arrC, rowsA, colsB);

    // heap memory management cleanup
    delete_array(arrA, rowsA);
    delete_array(arrB, rowsB);
    delete_array(arrC, rowsB);

    delete [] ma;

    return 0;
}


