#include <iostream>
#include <pthread.h>
#include <cstdlib>
#include <deque>

const size_t BUFFER_SIZE = 10;
const int LIMIT = 200;

int n = 0;
std::deque<int> buffer;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_wakeup_producer = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_wakeup_consumer = PTHREAD_COND_INITIALIZER;

void* producer(void* arg)
{
   
   while (true) { 
        pthread_mutex_lock(&mutex);

        while (buffer.size() > 6)
            pthread_cond_wait(&cond_wakeup_producer, &mutex);

        if (n >= LIMIT)
        {
            pthread_mutex_unlock(&mutex);
            pthread_exit(0);

        }

        std::cout << "Adding: " << n << std::endl;
        buffer.push_back(n++);

        pthread_cond_signal(&cond_wakeup_consumer);
        pthread_mutex_unlock(&mutex);
   }
}

void* consumer(void* arg)
{
   while (true) {
        pthread_mutex_lock(&mutex);

        while (buffer.empty())
            pthread_cond_wait(&cond_wakeup_consumer, &mutex);
        
        if (n >= LIMIT) 
        {
            pthread_mutex_unlock(&mutex);
            pthread_exit(NULL);
        }

        std::cout << "Removing: " << buffer.front() << std::endl;
        buffer.pop_front();

        pthread_cond_signal(&cond_wakeup_producer);
        pthread_mutex_unlock(&mutex);
    }
}

int main(int argc, char* argv[])
{
    size_t num_threads = 5;
    pthread_t producer_thread[num_threads];
    pthread_t consumer_thread[num_threads];

    for (size_t i = 0; i < num_threads; ++i) {
        pthread_create(&producer_thread[i], NULL, producer, NULL);
        pthread_create(&consumer_thread[i], NULL, consumer, NULL);
    }
    
    for (size_t i = 0; i < num_threads; ++i) {
        pthread_join(producer_thread[i], NULL);
        pthread_join(consumer_thread[i], NULL);
    }
    
    std::cout << "All done" << std::endl;

    return 0;
}
