#include <iostream>
#include <pthread.h>
#include <cstdlib>

const size_t BUFFER_SIZE = 6;
int buffer[BUFFER_SIZE];
size_t in = 0;
size_t out = 0;

int n = 0;
int LIMIT = 100;

//
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_wakeup_producer = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_wakeup_consumer = PTHREAD_COND_INITIALIZER;

void* producer(void* arg)
{
    while (n < LIMIT) {
    // ----------------------- critical region ----------------------
        pthread_mutex_lock(&mutex);
        while ((out+1)%BUFFER_SIZE == in) 
            pthread_cond_wait(&cond_wakeup_producer, &mutex);
            
        std::cout << "Adding: " << n << std::endl;

        buffer[in] = n++;
        in = (in+1) % BUFFER_SIZE;

        pthread_cond_signal(&cond_wakeup_consumer);
        pthread_mutex_unlock(&mutex);
    // ---------------------- end critical region -------------------
    }
    //pthread_exit(0);
}

void* consumer(void* arg)
{
    while (n < LIMIT) {
    // ----------------------- critical region ----------------------
        pthread_mutex_lock(&mutex);
        while (in == out)
            pthread_cond_wait(&cond_wakeup_consumer, &mutex);

        std::cout << "Removing: " << buffer[out] << std::endl;
        out = (out+1) % BUFFER_SIZE;

        pthread_cond_signal(&cond_wakeup_producer);
        pthread_mutex_unlock(&mutex);
    // --------------------- end critical region --------------------
    }
}

int main(int argc, char* argv[])
{        
    pthread_t producer_thread;
    pthread_t consumer_thread;
    pthread_create(&producer_thread, NULL, producer, NULL);
    pthread_create(&consumer_thread, NULL, consumer, NULL);

    pthread_join(producer_thread, NULL);
    pthread_join(consumer_thread, NULL);        

    return 0;
}
