#include <iostream>
#include <pthread.h>
#include <cstdlib>
#include <deque>

const size_t NUM_THREADS = 5;


const size_t LIMIT = 200;
int n = 0;

std::deque<int> buffer;
const size_t BUFFER_SIZE = 5;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t wakeup_producer = PTHREAD_COND_INITIALIZER;
pthread_cond_t wakeup_consumer = PTHREAD_COND_INITIALIZER;

void* producer(void* arg)
{
    while (true) {
        pthread_mutex_lock(&mutex);
    
        while (buffer.size() == BUFFER_SIZE)
            pthread_cond_wait(&wakeup_producer, &mutex);

        if (n > LIMIT)
        {
            pthread_mutex_unlock(&mutex);
            std::cout << "Producer breaking..." << std::endl;
            break;
        }

        buffer.push_back(n++);
        std::cout << "Added: " << n-1 << std::endl;

        pthread_cond_signal(&wakeup_consumer);
        pthread_mutex_unlock(&mutex);
    }
}

void* consumer(void* arg)
{
    while (true) {
        pthread_mutex_lock(&mutex);
        
        while (buffer.empty())
            pthread_cond_wait(&wakeup_consumer, &mutex);

        std::cout << "Removing: " << buffer.front() << std::endl;
        buffer.pop_front();

        if (buffer.empty() && n > LIMIT)
        {
            pthread_mutex_unlock(&mutex);
            break;
        }

        pthread_cond_signal(&wakeup_producer);
        pthread_mutex_unlock(&mutex);
    }
}

int main(int argc, char* argv[])
{
    // define producer
    pthread_t producer_thread[NUM_THREADS];
    pthread_t consumer_thread[NUM_THREADS];
    
    for (size_t i = 0; i < NUM_THREADS; ++i) {
        pthread_create(&producer_thread[i], NULL, producer, NULL);
        pthread_create(&consumer_thread[i], NULL, consumer, NULL);
    }

    for (size_t i = 0; i < NUM_THREADS; ++i)
        pthread_join(producer_thread[i], NULL);
   
    for (size_t i = 0; i < NUM_THREADS; ++i)
        pthread_join(consumer_thread[i], NULL);

    std::cout << "All done" << std::endl;

    return 0;
}
