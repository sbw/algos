#pragma once

struct Node {
    Node(Node* left, Node* right, int data) :
        left_(left), right_(right), data_(data) {}
    Node(int data) : Node(nullptr, nullptr, data) {}

    Node* left_;
    Node* right_;
    int data_;
};

