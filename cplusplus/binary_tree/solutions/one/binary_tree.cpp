#include "binary_tree.h"
#include <iostream>
#include <cstdlib>
#include <math.h>

template <typename T>
Node<T>* insert(Node<T>* root, T data)
{
    if (!root) return new Node<T>(data);
    
    if (data < root->data)
        root->left = insert(root->left, data);
    else
        root->right = insert(root->right, data);
        
    return root;  
}

template <typename T>
Node<T>* find(Node<T>* root, T data)
{
    if (!root) return nullptr;
    if (data == root->data) return root;

    if (data < root->data)
        return find(root->left, data);
    return find(root->right, data);
}

template <typename T>
Node<T>* find_parent(Node<T>* root, Node<T>* child)
{
    if (root == child) return nullptr;
    if (child == root->left || child == root->right)
        return root;

    if (child->data < root->data)
        return find_parent(root->left, child);
    return find_parent(root->right, child);
}

template <typename T>
Node<T>* find_successor(Node<T>* root, Node<T>* node)
{
    Node<T>* successor = node->right;
    
    if (successor)
    {
        while (successor->left) 
            successor = successor->left;
        return successor;
    }
    
    while (true) {
        if (successor) node = successor;
        successor = find_parent(root, node);
        if (!successor || successor->right != node)
            break;
    } 
    
    return successor; 
}

template <typename T>
Node<T>* remove(Node<T>* root, Node<T>* node)
{
    if (!node) return root;
    
    if (root == node)
    {
        delete root;
        return nullptr;
    }
    
    if (!node->left && !node->right)
    {
        Node<T>* parent = find_parent(root, node);
        if (parent->right == node)
            parent->right = nullptr;
        else
            parent->left = nullptr;
        delete node;
        return root;
    }
    
    if (!node->left || !node->right)
    {
        if (!node->left)
        {
            node->data = node->right->data;
            delete node->right;
            node->right = nullptr;
        }
        else
        {
            node->data = node->left->data;
            delete node->left;
            node->left = nullptr;
        }

        return root;
    }

    Node<T>* next = find_successor(root, node);
    T data = next->data;
    std::cout << next->data << std::endl;
    root = remove(root, next);
    node->data = data;

    return root;
}

template <typename T>
size_t tree_size(Node<T>* root)
{
    if (!root) return 0;

    return 1 + tree_size(root->left) + tree_size(root->right);
}

template <typename T>
size_t tree_size_iter(Node<T>* root)
{
    if (!root) return 0;

    size_t size = 0;
    std::queue<Node<T>*> queue;
    queue.push(root);

    while (!queue.empty()) {
        Node<T>* current = queue.front();
        queue.pop();
        ++size;
        if (current->left) queue.push(current->left);
        if (current->right) queue.push(current->right);
    }

    return size;
}

template <typename T>
size_t depth(Node<T>* root)
{
    if (!root) return 0;
    
    return 1 + std::max(depth(root->left), depth(root->right));
}


template <typename T>
size_t depth_iter(Node<T>* root)
{
    if (!root) return 0;
       
    size_t max_depth = 0;
    std::queue<Node<T>*> queue;
    queue.push(root);
    queue.push(nullptr);
   
    while (!queue.empty()) {
        Node<T>* current = queue.front();
        queue.pop();
        if (!current) 
        {
            ++max_depth;
            if (!queue.empty()) queue.push(nullptr);
            continue;
        }
        if (current->left) queue.push(current->left);
        if (current->right) queue.push(current->right);
    }

    return max_depth;
}

template <typename T>
bool balanced_brute(Node<T>* root)
{
    if (!root) return true;
    if (!balanced_brute(root->left) ||
            !balanced_brute(root->right))
        return false;

    int left = (int) depth(root->left);
    int right = (int) depth(root->right);
    return std::abs(left-right) <= 1;
}

template <typename T>
bool balanced(Node<T>* root, size_t& depth)
{
    if (!root) return true;
    size_t left = 0;
    size_t right = 0;
    if (!balanced(root->left, left) || 
        !balanced(root->right, right))
        return false;

    depth = 1 + std::max(left, right);
    return std::abs((int) left - (int) right) <= 1;
}

template <typename T>
bool find_path(Node<T>* root, Node<T>* node, std::vector<Node<T> >* path);

template <typename T>
Node<T>* lowest_common_ancestor(Node<T>* root, Node<T>* a, Node<T>* b);

template <typename T>
Node<T>* lowest_common_ancestor_recur(Node<T>* root, Node<T>* a, Node<T>* b);

template <typename T>
Node<T>* lowest_common_ancestor_iter(Node<T>* root, Node<T>* a, Node<T>* b);

template <typename T>
void inorder(Node<T>* root, std::vector<T>& nodes)
{
    if (!root) return;

    inorder(root->left, nodes);
    nodes.push_back(root->data);
    inorder(root->right, nodes);
}

template <typename T>
void postorder(Node<T>* root, std::vector<T>& nodes)
{
    if (!root) return;

    postorder(root->left, nodes);
    postorder(root->right, nodes);
    nodes.push_back(root->data);
}

template <typename T>
void preorder(Node<T>* root, std::vector<T>& nodes)
{
    if (!root) return;

    nodes.push_back(root->data);
    preorder(root->left, nodes);
    preorder(root->right, nodes);
}

template <typename T>
Node<T>* reconstruct_tree();


template <typename T>
void find_huff(std::string prefix, Node<T>* root)
{
    if (!root->left && !root->right)
    {
        std::cout << root->data << ", " << prefix << std::endl;
        return;
    }
    
    find_huff(prefix += "0", root->left);
    find_huff(prefix += "1", root->right);
}

template <typename T>
void find_huff_iter(std::map<T, std::string>& encoding)
{


}

template <typename T>
std::map<T, std::string> huffman_code(const std::vector<T>& symbols, 
                        const std::vector<double>& freqs)
{
    // check size, etc?
    
    std::map<T, std::string> results;

    std::priority_queue<
                        std::pair<double, Node<T>*>, 
                        std::vector< std::pair<double, Node<T>*> >, 
                        std::greater< std::pair<double, Node<T>*> > 
                       > pq;     
   
    for (size_t i = 0; i < symbols.size()-1; ++i) 
        pq.push(std::make_pair(freqs[i], new Node<T>(symbols[i]))); 
    
    while (pq.size() > 1) {
        std::pair<double, Node<T>*> tree1 = pq.top();   
        pq.pop();
        std::pair<double, Node<T>*> tree2 = pq.top();
        pq.pop();


        std::pair<double, Node<T>*> root = 
            std::make_pair(tree1.first + tree2.first, new Node<char>('z'));
       root.second->left = tree1.second;
       root.second->right = tree2.second;
       pq.push(root);


       std::cout << tree1.first + tree2.first << std::endl;
    }
   
    std::pair<double, Node<T>*> root = pq.top();
    pq.pop();

    find_huff("0", root.second->left); 
    find_huff("1", root.second->right);

    return results;
}

template Node<int>* insert(Node<int>* root, int data);
template Node<int>* find(Node<int>* root, int data);
template Node<int>* find_parent(Node<int>* root, Node<int>* child);
template Node<int>* find_successor(Node<int>* root, Node<int>* node);
template Node<int>* remove(Node<int>* root, Node<int>* node);
template void inorder(Node<int>* root, std::vector<int>& nodes);
template void postorder(Node<int>* root, std::vector<int>& nodes);
template void preorder(Node<int>* root, std::vector<int>& nodes);
template size_t tree_size(Node<int>* root);
template size_t tree_size_iter(Node<int>* root);
template size_t depth(Node<int>* root);
template size_t depth_iter(Node<int>* root);
template bool balanced_brute(Node<int>* root);
template bool balanced(Node<int>* root, size_t& depth);
template std::map<char, std::string> huffman_code(
        const std::vector<char>& symbols, const std::vector<double>& freqs);
template void find_huff(std::string prefix, Node<char>* root);
