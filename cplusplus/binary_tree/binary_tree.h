class Node;

#include <map>
//#include <vector>
//#include <string>
//#include <queue>

// -------------------- Core Binary Tree Algorithms -----------------
Node* insert(Node* root, int data);

Node* find(Node* root, int data);

Node* find_parent(Node* root, Node* child);

Node* find_successor(Node* root, Node* node);

Node* remove(Node* root, Node* node);

size_t tree_size(Node* root);

size_t tree_size_iter(Node* root);

size_t depth(Node* root);

size_t depth_iter(Node* root);

bool balanced_brute(Node* root);

bool balanced(Node* root, size_t& depth);

bool find_path(Node* root, Node* node, std::vector<Node*>* path);

Node* lowest_common_ancestor(Node* root, Node* a, Node* b);

Node* lowest_common_ancestor_recur(Node* root, Node* a, Node* b);

Node* lowest_common_ancestor_iter(Node* root, Node* a, Node* b);

void inorder(Node* root, std::vector<int>& nodes);

void postorder(Node* root, std::vector<int>& nodes);

void preorder(Node* root, std::vector<int>& nodes);

Node* reconstruct_tree();

void find_huff(std::string prefix, Node* root);

void find_huff_iter(std::map<int, std::string>& encoding);

std::map<int, std::string> huffman_code(const std::vector<int>& symbols,
                                            const std::vector<double>& freqs);
