class Node;
#include <vector>
#include <string>
#include <map>

// -------------------- Core Binary Tree Algorithms -----------------
Node* insert(Node* root, int data)
{
	return nullptr;
}

Node* find(Node* root, int data)
{
	return nullptr;
}

Node* find_parent(Node* root, Node* child)
{
	return nullptr;
}

Node* find_successor(Node* root, Node* node)
{
	return nullptr;
}

Node* remove(Node* root, Node* node)
{
	return nullptr;
}

size_t tree_size(Node* root)
{
	return 0;
}

size_t tree_size_iter(Node* root)
{
	return 0;
}

size_t depth(Node* root)
{
	return 0;
}

size_t depth_iter(Node* root)
{
	return 0;
}

bool balanced_brute(Node* root)
{
	return true;
}

bool balanced(Node* root, size_t& depth)
{
	return true;
}

bool find_path(Node* root, Node* node, std::vector<Node*>* path)
{
	return true;
}

Node* lowest_common_ancestor(Node* root, Node* a, Node* b)
{
	return nullptr;
}

Node* lowest_common_ancestor_recur(Node* root, Node* a, Node* b)
{
	return nullptr;
}

Node* lowest_common_ancestor_iter(Node* root, Node* a, Node* b)
{
	return nullptr;
}

void inorder(Node* root, std::vector<int>& nodes)
{

}

void postorder(Node* root, std::vector<int>& nodes)
{

}

void preorder(Node* root, std::vector<int>& nodes)
{

}

Node* reconstruct_tree()
{
	return nullptr;
}

void find_huff(std::string prefix, Node* root)
{

}

void find_huff_iter(std::map<int, std::string>& encoding)
{

}

std::map<int, std::string> huffman_code(const std::vector<int>& symbols,
                                            const std::vector<double>& freqs)
		{
			return std::map<int, std::string>();
		}
