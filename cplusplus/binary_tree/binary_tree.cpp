#include "node.h"
#include <vector>
#include <string>
#include <map>
#include <queue>
#include <algorithm>
#include <iostream>

// -------------------- Core Binary Tree Algorithms -----------------
Node* insert(Node* root, int data)
{
	if (!root) return new Node(data);

	if (data < root->data_)
		root->left_ = insert(root->left_, data);
	else
		root->right_ = insert(root->right_, data);

	return root;
}

Node* find(Node* root, int data)
{
	if (!root) return nullptr;
	if (data == root->data_) return root;

	if (data < root->data_)
		return find(root->left_, data);
	return find(root->right_, data);
}

Node* find_parent(Node* root, Node* child)
{
	if (!root || !child) return nullptr;
	if (root == child) return nullptr;
	if (child == root->right_ || child == root->left_)
		return root;

	if (child->data_ < root->data_)
		return find_parent(root->left_, child);
	return find_parent(root->right_, child);
}

Node* find_successor(Node* root, Node* node)
{
	Node* successor = node->right_;

	if (successor)
	{
		while (successor->left_)
			successor = successor->left_;
		return successor;
	}

	successor = find_parent(root, node);
	while (successor && successor->right_ == node) {
		node = successor;
		successor = find_parent(root, node);
	}

	return successor;
}

Node* remove(Node* root, Node* node)
{
	//if (!node) return root;

	if (node->left_ && node->right_) {
		Node* next = find_successor(root, node);
		int data = next->data_;
		std::cout << next->data_ << std::endl;
		remove(root, next);
		node->data_ = data;

		return root;
	}

	if (node->left_ || node->right_) {
		if (!node->left_)
		{
			node->data_ = node->right_->data_;
			delete node->right_;
			node->right_ = nullptr;
		} else {
			node->data_ = node->left_->data_;
			delete node->left_;
			node->left_ = nullptr;
		}
		return root;
	}

	if (root == node)
	{
		delete root;
		return nullptr;
	}

	Node* parent = find_parent(root, node);
	if (parent->right_ == node)
		parent->right_ = nullptr;
	else
		parent->left_ = nullptr;
	delete node;

	return root;
}

size_t tree_size(Node* root)
{
	if (!root) return 0;
	return 1 + tree_size(root->left_) + tree_size(root->right_);
}

size_t tree_size_iter(Node* root)
{
	if (!root) return 0;

	size_t size = 0;
	std::queue<Node*> queue;
	queue.push(root);

	while (!queue.empty()) {
		Node* front = queue.front();
		queue.pop();
		++size;
		if (front->left_) queue.push(front->left_);
		if (front->right_) queue.push(front->right_);
	}

	return size;
}

size_t depth(Node* root)
{
	if (!root) return 0;

	return 1 + std::max(depth(root->left_), depth(root->right_));
}

size_t depth_iter(Node* root)
{
	if (!root) return 0;

	size_t depth = 0;
	std::queue<Node*> queue;
	queue.push(root);
	queue.push(nullptr);

	while (!queue.empty()) {
		Node* current = queue.front();
		queue.pop();
		if (!current) {
			if (!queue.empty()) queue.push(nullptr);
			++depth;
		} else {
			if (current->left_) queue.push(current->left_);
			if (current->right_) queue.push(current->right_);
		}
	}

	return depth;
}

bool balanced_brute(Node* root)
{
	if (!root) return true;
	if (!balanced_brute(root->left_)
			|| !balanced_brute(root->right_))
		return false;

	int diff = (int) depth(root->left_) - (int) depth(root->right_);

	return std::abs(diff) <= 1;
}

bool balanced(Node* root, size_t& depth)
{
	if (!root) return true;

	size_t left = 0;
	size_t right = 0;

	if (!balanced(root->left_, left)
		|| !balanced(root->right_, right))
		return false;

	depth = 1 + std::max(left, right);
	return std::abs((int) left - (int) right) <= 1;
}

bool find_path(Node* root, Node* node, std::vector<Node*>* path)
{
	return true;
}

Node* lowest_common_ancestor(Node* root, Node* a, Node* b)
{
	return nullptr;
}

Node* lowest_common_ancestor_recur(Node* root, Node* a, Node* b)
{
	return nullptr;
}

Node* lowest_common_ancestor_iter(Node* root, Node* a, Node* b)
{
	return nullptr;
}

void inorder(Node* root, std::vector<int>& nodes)
{
	if (!root) return;

	inorder(root->left_, nodes);
	nodes.push_back(root->data_);
	inorder(root->right_, nodes);
}

void postorder(Node* root, std::vector<int>& nodes)
{
	if (!root) return;

	postorder(root->left_, nodes);
	postorder(root->right_, nodes);
	nodes.push_back(root->data_);
}

void preorder(Node* root, std::vector<int>& nodes)
{
	if (!root) return;

	nodes.push_back(root->data_);
	preorder(root->left_, nodes);
	preorder(root->right_, nodes);
}

Node* reconstruct_tree()
{
	return nullptr;
}

void find_huff(std::string prefix, Node* root)
{

}

void find_huff_iter(std::map<int, std::string>& encoding)
{

}

std::map<int, std::string> huffman_code(const std::vector<int>& symbols,
                                            const std::vector<double>& freqs)
		{
			return std::map<int, std::string>();
		}
