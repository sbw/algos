#include <algorithm>
#include "binary_tree.h"
#include "node.h"
#include "gtest/gtest.h"

namespace {

class BinaryTreeTest : public ::testing::Test {
protected:
    BinaryTreeTest() {}
    ~BinaryTreeTest() {}

    void SetUp() override
    {
        root = NULL;
        int input1[] = { 
            31, 27, 13, 11, 19, 29, 28, 30, 33, 51, 60, 32, 40, 38, 45 
        };
        first_tree.assign(input1, input1 + 15);    

        int input2[] = {
            50, 40, 30, 20, 35, 45, 60, 55, 70, 65, 80 
        };
        second_tree.assign(input2, input2 + 11);
        
        int input3[] = {
            20, 10, 30, 5, 15, 25, 35
        };
        third_tree.assign(input3, input3 + 7);
    }

    void TearDown() override
    {
        remove_tree(root);
    }

    void remove_tree(Node* root)
    {
        if (!root) return;
        Node* left = root->left_;
        Node* right = root->right_;

        remove_tree(left);
        delete root;
        remove_tree(right);
    }

    Node* root;
    std::vector<int> first_tree;
    std::vector<int> second_tree;
    std::vector<int> third_tree;
};

TEST_F(BinaryTreeTest, insert)
{
    root = insert(root, 30);
    root = insert(root, 25);
    root = insert(root, 33);
    root = insert(root, 28);
    root = insert(root, 26);

    ASSERT_TRUE(root);
    ASSERT_EQ(30, root->data_);
    ASSERT_EQ(25, root->left_->data_);
    ASSERT_EQ(33, root->right_->data_);
    ASSERT_EQ(28, root->left_->right_->data_);
    ASSERT_EQ(26, root->left_->right_->left_->data_);
}

TEST_F(BinaryTreeTest, inorder)
{
    std::vector<int> sorted = first_tree;
    std::sort(sorted.begin(), sorted.end());
    
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    std::vector<int> output;
    inorder(root, output);
    ASSERT_TRUE(output.size());

    for (size_t i = 0; i < output.size(); ++i) {  
        ASSERT_EQ(sorted[i], output[i]);
        std::cout << output[i] << '\t';
    }
    std::cout << std::endl;
}

TEST_F(BinaryTreeTest, preorder)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    int e[] 
        = { 31, 27, 13, 11, 19, 29, 28, 30, 33, 32, 51, 40, 38, 45, 60 };
    std::vector<int> expected(e, e + sizeof(e)/sizeof(int));

    std::vector<int> output;
    preorder(root, output);
    ASSERT_TRUE(output.size());

    for (size_t i = 0; i < output.size(); ++i) {  
        ASSERT_EQ(expected[i], output[i]);
        std::cout << output[i] << '\t';
    }
    std::cout << std::endl;
}

TEST_F(BinaryTreeTest, postorder)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);
    
    int e[] =
        { 11, 19, 13, 28, 30, 29, 27, 32, 38, 45, 40, 60, 51, 33, 31 };
    std::vector<int> expected(e, e + sizeof(e)/sizeof(int));
    std::vector<int> output;
    postorder(root, output);
    ASSERT_TRUE(output.size());

    for (size_t i = 0; i < output.size(); ++i) {  
        ASSERT_EQ(expected[i], output[i]);
        std::cout << output[i] << '\t';
    }
    std::cout << std::endl;
}

TEST_F(BinaryTreeTest, find40)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* node = find(root, 40);
    ASSERT_TRUE(node);
    ASSERT_EQ(40, node->data_);
}

TEST_F(BinaryTreeTest, find19)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* node = find(root, 19);
    ASSERT_TRUE(node);
    ASSERT_EQ(19, node->data_);
}

TEST_F(BinaryTreeTest, find77)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* node = find(root, 77);
    ASSERT_TRUE(node == NULL);
}

TEST_F(BinaryTreeTest, findNull)
{
    Node* node = find(root, 40);
    ASSERT_TRUE(node == NULL);
}

TEST_F(BinaryTreeTest, find_parent29)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* child = find(root, 29);
    ASSERT_TRUE(child);
    Node* parent = find_parent(root, child);
    ASSERT_TRUE(parent);
    ASSERT_EQ(27, parent->data_);
}

TEST_F(BinaryTreeTest, find_parent38)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* child = find(root, 38);
    ASSERT_TRUE(child);
    Node* parent = find_parent(root, child);
    ASSERT_TRUE(parent);
    ASSERT_EQ(40, parent->data_);
}

TEST_F(BinaryTreeTest, find_parent31)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* child = find(root, 31);
    ASSERT_TRUE(child);
    Node* parent = find_parent(root, child);
    ASSERT_TRUE(parent == NULL);
}

TEST_F(BinaryTreeTest, find_successor33)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* node = find(root, 33);
    ASSERT_TRUE(node);
    Node* successor = find_successor(root, node);
    ASSERT_TRUE(successor);

    ASSERT_EQ(38, successor->data_);
}

TEST_F(BinaryTreeTest, find_successor45)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);
    
    Node* node = find(root, 45);
    ASSERT_TRUE(node);
    Node* successor = find_successor(root, node);
    ASSERT_TRUE(successor);
    ASSERT_EQ(51, successor->data_);
}

TEST_F(BinaryTreeTest, find_successor29)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);
    
    Node* node = find(root, 29);
    ASSERT_TRUE(node);
    Node* successor = find_successor(root, node);
    ASSERT_TRUE(successor);
    ASSERT_EQ(30, successor->data_);
}

TEST_F(BinaryTreeTest, remove29)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* node = find(root, 29);
    root = remove(root, node);
    ASSERT_TRUE(root);
    ASSERT_EQ(root->data_, 31);
    ASSERT_EQ(node->data_, 30);
    ASSERT_EQ(node->left_->data_, 28);
    ASSERT_TRUE(node->right_ == nullptr);
}

TEST_F(BinaryTreeTest, removeRoot)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    Node* node = find(root, 31);
    root = remove(root, node);
    ASSERT_TRUE(root);
    ASSERT_EQ(root->data_, 32);
    ASSERT_EQ(root->left_->data_, 27);
    ASSERT_EQ(root->right_->data_, 33);
}

TEST_F(BinaryTreeTest, tree_size1)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);
    
    size_t size = tree_size(root);

    ASSERT_EQ(size, first_tree.size());

    std::cout << first_tree.size() << std::endl;
}

TEST_F(BinaryTreeTest, tree_size2)
{
    for (size_t i = 0; i < second_tree.size(); ++i)
        root = insert(root, second_tree[i]);
    ASSERT_TRUE(root);
    
    size_t size = tree_size(root);

    ASSERT_EQ(size, second_tree.size());

    std::cout << second_tree.size() << std::endl;
}

TEST_F(BinaryTreeTest, tree_size_iter1)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);
    
    size_t size = tree_size_iter(root);

    ASSERT_EQ(size, first_tree.size());

    std::cout << first_tree.size() << std::endl;
}

TEST_F(BinaryTreeTest, tree_size_iter2)
{
    for (size_t i = 0; i < second_tree.size(); ++i)
        root = insert(root, second_tree[i]);
    ASSERT_TRUE(root);
    
    size_t size = tree_size_iter(root);

    ASSERT_EQ(size, second_tree.size());

    std::cout << second_tree.size() << std::endl;
}

TEST_F(BinaryTreeTest, depth)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    size_t d = depth(root);

    ASSERT_EQ(5, d);
}

TEST_F(BinaryTreeTest, depth_iter)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    size_t d = depth_iter(root);

    ASSERT_EQ(5, d);
}

TEST_F(BinaryTreeTest, balanced_brute1)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    bool result = balanced_brute(root);
    ASSERT_FALSE(result);
}


TEST_F(BinaryTreeTest, balanced_brute2)
{
    for (size_t i = 0; i < second_tree.size(); ++i)
        root = insert(root, second_tree[i]);
    ASSERT_TRUE(root);

    bool result = balanced_brute(root);
    ASSERT_TRUE(result);
}

TEST_F(BinaryTreeTest, balanced1)
{
    for (size_t i = 0; i < first_tree.size(); ++i)
        root = insert(root, first_tree[i]);
    ASSERT_TRUE(root);

    size_t depth = 0;
    bool result = balanced(root, depth);
    ASSERT_FALSE(result);
}

TEST_F(BinaryTreeTest, balanced2)
{
    for (size_t i = 0; i < second_tree.size(); ++i)
        root = insert(root, second_tree[i]);
    ASSERT_TRUE(root);

    size_t depth = 0;
    bool result = balanced(root, depth);
    ASSERT_TRUE(result);
}

TEST_F(BinaryTreeTest, find_path)
{
	ASSERT_TRUE(false);
}

TEST_F(BinaryTreeTest, lowest_common_ancestor)
{
	ASSERT_TRUE(false);
}

TEST_F(BinaryTreeTest, lowest_common_ancestor_recur)
{
	ASSERT_TRUE(false);
}

TEST_F(BinaryTreeTest, lowest_common_ancestor_iter)
{
	ASSERT_TRUE(false);
}

TEST_F(BinaryTreeTest, reconstruct_tree)
{
	ASSERT_TRUE(false);
}

TEST_F(BinaryTreeTest, huffman_code)
{
	ASSERT_TRUE(false);
}



} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
