#include <vector>
#include <string>

namespace str {

size_t strlen(const char* str);

size_t strlen_rec(const char* str);

void strcpy(char* dest, const char* src, size_t len);

char strtok(char* str, const char* delim);

std::vector<std::string> split(const std::string& str,
                                const std::string& delim);

int strcmp(const char* str1, const char* str2);

bool rot_equiv(const char* a, const char* b);

bool rot_equiv_queue(const char* a, const char* b);

bool rot_equiv(const char* a, const char* b);

bool rot_equiv_simple(const char* a, const char* b);


bool anagrams(const char* a, const char* b);

bool anagrams_sorting(const char* a, const char* b);

bool match(const char* str, const char* expr);

bool unique(char* str);

void remove_duplicates(char* str);

void reverse(char* str, size_t len);

void reverse_words(char* str);

std::pair<size_t, size_t> counts(const char* str);

void justify(char* str);

void prepare_to_justify(char* buf, size_t len);

void justify_paragraph(char* str, size_t len);

int atoi(const char* str);

float atof(const char* str);

// substring search
size_t find_simple(const std::string& str, const std::string& sub);

void build_kmp_table(const std::string& sub, std::vector<int>& table);

size_t find_kmp(const std::string& str, const std::string& sub);

void make_bad_character_table(const std::string& sub, 
                                std::vector<int>& table);

void make_good_suffix_table(const std::string& sub,
                                std::vector<int>& table);

int find(const std::string& str, const std::string& sub);

size_t find_rk(const std::string& str, std::string& sub);

} // end namespace str
