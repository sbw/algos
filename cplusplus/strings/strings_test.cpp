#include "strings.h"
#include "gtest/gtest.h"

namespace {

class StringsTest : public ::testing::Test {
protected:
    StringsTest() {}
    virtual ~StringsTest() {}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(StringsTest, strlen14)
{
    char input[] = "This is a test";
    ASSERT_EQ(14, str::strlen(input));
}

TEST_F(StringsTest, strlen1)
{
    char input[] = "T";
    ASSERT_EQ(1, str::strlen(input));
}

TEST_F(StringsTest, strlen0)
{
    char input[] = "";
    ASSERT_EQ(0, str::strlen(input));
}

TEST_F(StringsTest, strlen_rec14)
{
    char input[] = "This is a test";
    ASSERT_EQ(14, str::strlen_rec(input));
}

TEST_F(StringsTest, strlenEmpty)
{
    char input[] = "";
    ASSERT_EQ(0, str::strlen(input));
}

TEST_F(StringsTest, strlen_recursiveEmpty)
{
    char input[] = "";
    ASSERT_EQ(0, str::strlen_rec(input));
}

TEST_F(StringsTest, strcpy)
{
    char src[] = "This is a test string";
    size_t len = sizeof(src)/sizeof(char);
    char dest[len+10];

    str::strcpy(dest, src, len+10);

    ASSERT_STREQ(src, dest);
}

TEST_F(StringsTest, strcpyNull)
{
    const char src[] = "This is a test string";
    size_t len = sizeof(src)/sizeof(char);
    char* dest = NULL;

    str::strcpy(dest, src, len);

    ASSERT_TRUE(dest == NULL);
}

TEST_F(StringsTest, strcpyLong)
{
    const char src[] = "This is a test string that is over length";
    const char src2[] = "This is a test";
    size_t len = sizeof(src2)/sizeof(char);
    char dest[len];

    str::strcpy(dest, src, len-1);

    ASSERT_STRNE(src, dest);
    ASSERT_STREQ(src2, dest);
}

TEST_F(StringsTest, reverse)
{
    char src[] = "abcdefg hijklmnop qrs tuv w x y z";
    size_t len = sizeof(src)/sizeof(char);
    char expected[] = "z y x w vut srq ponmlkjih gfedcba";
    str::reverse(src, len-1);
    ASSERT_STREQ(src, expected);
}

TEST_F(StringsTest, reverse_words)
{
    char src[] = "abcdefg hijklmnop qrs tuv w x y z";
    size_t len = sizeof(src)/sizeof(char);
    char expected[] = "z y x w tuv qrs hijklmnop abcdefg";
    str::reverse_words(src);
    ASSERT_STREQ(src, expected);
}

TEST_F(StringsTest, rot_equiv_queueTRUE)
{
    char a[] = "abcdefghijk";
    char b[] = "hijkabcdefg";
    ASSERT_TRUE(str::rot_equiv_queue(a, b));
}

TEST_F(StringsTest, rot_equiv_queueDifferentLengths)
{
    char a[] = "abcdefg";
    char b[] = "hijkabcdefg";
    ASSERT_FALSE(str::rot_equiv_queue(a, b));
}

TEST_F(StringsTest, rot_equiv_queueFALSE)
{
    char a[] = "abcdefghijk";
    char b[] = "hijkabzzzzz";
    ASSERT_FALSE(str::rot_equiv_queue(a, b));
}

TEST_F(StringsTest, rot_equivTRUE)
{
    char a[] = "abcdefghijk";
    char b[] = "hijkabcdefg";
    ASSERT_TRUE(str::rot_equiv(a, b));
}

TEST_F(StringsTest, rot_equiv_DifferentLengths)
{
    char a[] = "abcdefg";
    char b[] = "hijkabcdefg";
    ASSERT_FALSE(str::rot_equiv(a, b));
}

TEST_F(StringsTest, rot_equivFALSE)
{
    char a[] = "abcdefghijk";
    char b[] = "hijkabzzzzz";
    ASSERT_FALSE(str::rot_equiv(a, b));
}

TEST_F(StringsTest, rot_equiv_simpleTRUE)
{
    char a[] = "abcdefghijk";
    char b[] = "hijkabcdefg";
    ASSERT_TRUE(str::rot_equiv_simple(a, b));
}

TEST_F(StringsTest, rot_equiv_simpleFALSE)
{
    char a[] = "abcdefghijk";
    char b[] = "hijkabzzzzz";
    ASSERT_FALSE(str::rot_equiv_simple(a, b));
}

TEST_F(StringsTest, strcmpSame)
{
    char a[] = "abcdefghijk";
    char b[] = "abcdefghijk";
    ASSERT_EQ(0, str::strcmp(a, b));
}

TEST_F(StringsTest, strcmpSamePrefixDifferentLength)
{
    char a[] = "abcdefghijklmnop";
    char b[] = "abcdefghijk";
    ASSERT_TRUE(0 < str::strcmp(a, b));
}

TEST_F(StringsTest, strcmpNeg)
{
    char a[] = "abcdefghijk";
    char b[] = "abcdefgzzzz";
    ASSERT_TRUE(0 > str::strcmp(a, b));
}

TEST_F(StringsTest, strcmpPos)
{
    char a[] = "abcdefgzzzz";
    char b[] = "abcdefghijk";
    ASSERT_TRUE(0 < str::strcmp(a, b));
}

TEST_F(StringsTest, anagramsTRUE)
{
    char a[] = "gfabcdabcdfg";
    char b[] = "ddccbbaaffgg";
    ASSERT_TRUE(str::anagrams(a, b));
}

TEST_F(StringsTest, anagramsFALSE)
{
    char a[] = "gfabcdabcdfg";
    char b[] = "ddccbbaaffggzz";
    ASSERT_FALSE(str::anagrams(a, b));
}

TEST_F(StringsTest, anagrams_sortingTRUE)
{
    char a[] = "gfabcdabcdfg";
    char b[] = "ddccbbaaffgg";
    ASSERT_TRUE(str::anagrams_sorting(a, b));
}

TEST_F(StringsTest, anagrams_sortingFALSE)
{
    char a[] = "gfabcdabcdfg";
    char b[] = "zzddccbbaaffgg";
    ASSERT_FALSE(str::anagrams_sorting(a, b));
}

TEST_F(StringsTest, atoi)
{
    ASSERT_EQ(-153, atoi("-153"));
    ASSERT_EQ(978, atoi("978"));
}

TEST_F(StringsTest, findABC)
{
    ASSERT_EQ(2, str::find("XXABCDE", "ABC"));
}

TEST_F(StringsTest, findABC2nd)
{
    ASSERT_EQ(7, str::find("XXABYYYABCD", "ABC"));
}

TEST_F(StringsTest, findTooShort)
{
	ASSERT_EQ(-1, str::find("XX", "ABC"));
}

TEST_F(StringsTest, findPartial)
{
	ASSERT_EQ(-1, str::find("XXABYY", "ABC"));
}

TEST_F(StringsTest, findPartialEnd)
{
	ASSERT_EQ(-1, str::find("XXYYAB", "ABC"));
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
