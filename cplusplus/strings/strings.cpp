#include "strings.h"
#include <deque>
#include <iostream>
#include <algorithm>

namespace str {

/* C-string length */
size_t strlen(const char* str)
{ 
    return 0;
}

size_t strlen_rec(const char* str)
{
    return 0;
}

/* C-string copy with length limit */
void strcpy(char* dest, const char* src, size_t len)
{

}

char strtok(char* str, const char* delim);

std::vector<std::string> split(const std::string& str,
                                const std::string& delim);
/*  strcmp 
    a == b  0
    a < b   -1
    a > b   1 */
int strcmp(const char* a, const char* b)
{
    return 0;
}

/* Rotational equivalence */
bool rot_equiv_queue(const char* a, const char* b)
{
	return true;
}

bool rot_equiv(const char* a, const char* b)
{
    return false;
}

bool rot_equiv_simple(const char* a, const char* b)
{
    return true;
}

/* anagrams */
bool anagrams(const char* a, const char* b)
{
    return true;
}

bool anagrams_sorting(const char* a, const char* b)
{
    return true;
}

bool match(const char* str, const char* expr)
{
	return true;
}

/* string only contains unique characters */
bool unique(char* str)
{
	return true;
}

/* reverse C-string */
void reverse(char* str, size_t len)
{
    if (len <= 1) return;
    
    for (char* rev = str+len-1; str < rev; ++str, --rev) {
        char temp = *str;
        *str = *rev;
        *rev = temp;
    }
}

void reverse_words(char* str)
{

}

std::pair<size_t, size_t> counts(const char* str);

void justify(char* str);

void prepare_to_justify(char* buf, size_t len);

void justify_paragraph(char* str, size_t len);

int atoi(const char* str)
{
    return 0;
}

float atof(const char* str);

// substring search
size_t find_simple(const std::string& str, const std::string& sub);

void build_kmp_table(const std::string& sub, std::vector<int>& table);

size_t find_kmp(const std::string& str, const std::string& sub);

void make_bad_character_table(const std::string& sub, 
                                std::vector<int>& table);

void make_good_suffix_table(const std::string& sub,
                                std::vector<int>& table);

int find(const std::string& str, const std::string& sub)
{
	int pos = -1;

	for (int i = 0; i < (str.size()-sub.size()+1); ++i) {
		bool match = true;
		for (int k = 0; k < sub.size(); ++k) {
			if (str[i+k] != sub[k]) {
				match = false;
				break;
			}
		}
		if (match) pos = i;
	}

	return pos;
}

size_t find_rk(const std::string& str, std::string& sub);

} //end namespace str
