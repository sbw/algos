#include "strings.h"
#include <deque>
#include <iostream>
#include <algorithm>

namespace str {

/* C-string length */
size_t strlen(const char* str)
{ 
    if (!str) return 0;
    size_t len = 0; 

    while (*str++) ++len;

    return len;
}

size_t strlen_rec(const char* str)
{
    if (!str || !*str) return 0;
    return 1 + strlen_rec(str+1);
}

/* C-string copy with length limit */
void strcpy(char* dest, const char* src, size_t len)
{
    if (src == dest) return;
    if (!src || !dest) return;
    size_t i = 0;
    for (; i < len && src[i]; ++i)
        dest[i] = src[i];
    dest[i] = '\0';
}

char strtok(char* str, const char* delim);

std::vector<std::string> split(const std::string& str,
                                const std::string& delim);
/*  strcmp 
    a == b  0
    a < b   -1
    a > b   1 */
int strcmp(const char* a, const char* b)
{
    if (!a && !b) return 0;
    if (!a) return -1;
    if (!b) return 1;

    while (*a && *b) 
        if (*a++ != *b++) break;
     
    return *a - *b;
}

/* Rotational equivalence */
bool rot_equiv_queue(const char* a, const char* b)
{
 
}

bool rot_equiv(const char* a, const char* b)
{
    return false;
}

bool rot_equiv_simple(const char* a, const char* b)
{
    if (a == b) return true;
    if (!a || !b) return false;
    if (strlen(a) != strlen(b)) 
        return false;

    std::string bstr(b);
    bstr += b;
    return std::string::npos != bstr.find(a);
}

/* anagrams */
bool anagrams(const char* a, const char* b)
{
    if (a == b) return true;
    if (!a || !b) return false;

    std::vector<int> histo(128);
    while (*a) histo[*a++] += 1;
    while (*b) histo[*b++] -= 1;

    for (auto& extra : histo)
        if (extra) return false;

    return true;
}

bool anagrams_sorting(const char* a, const char* b)
{
    if (a == b) return true;
    if (!a || !b) return false;
    
    std::vector<char> av(a, a + strlen(a));
    std::vector<char> bv(b, b + strlen(b));
    if (av.size() != bv.size()) return false;

    std::sort(av.begin(), av.end());
    std::sort(bv.begin(), bv.end());
    return av == bv;
}

bool match(const char* str, const char* expr);

/* string only contains unique characters */
bool unique(char* str);

/* reverse C-string */
void reverse(char* str, size_t len)
{
    if (len <= 1) return;
    
    for (char* rev = str+len-1; str < rev; ++str, --rev) {
        char temp = *str;
        *str = *rev;
        *rev = temp;
    }
}

void reverse_words(char* str)
{
    if (!str) return;
    char* front = str;
    char* rear = front;
    while (*rear) {
        while (*front == ' ') ++front;
        rear = front;
        while (*front && *front != ' ') ++front;
        reverse(rear, front-rear);
        rear = front;
    }

    reverse(str, rear-str);
}

std::pair<size_t, size_t> counts(const char* str);

void justify(char* str);

void prepare_to_justify(char* buf, size_t len);

void justify_paragraph(char* str, size_t len);

int atoi(const char* str)
{
    if (!str) return 0;
    int sign = 1;
    if (*str == '-') {
        ++str;
        sign = -1;
    }

    int n = 0;
    while (*str >= '0' && *str <= '9') {
        n = 10*n + (int)(*str - '0');
        ++str;
    }

    return sign*n;
}

float atof(const char* str);

// substring search
size_t find_simple(const std::string& str, const std::string& sub);

void build_kmp_table(const std::string& sub, std::vector<int>& table);

size_t find_kmp(const std::string& str, const std::string& sub);

void make_bad_character_table(const std::string& sub, 
                                std::vector<int>& table);

void make_good_suffix_table(const std::string& sub,
                                std::vector<int>& table);

size_t find_bm(const std::string& str, std::string& sub);

size_t find_rk(const std::string& str, std::string& sub);

} //end namespace str
