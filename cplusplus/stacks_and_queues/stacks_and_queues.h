#include <vector>
#include <stack>
#include <map>
#include <set>
#include <queue>
#include <algorithm>
#include <iostream>

template <typename T>
class stack {
public:
    bool empty() const
    { 
        return data.empty(); 
    }

    void push(const T& item)
    {
        data.push_back(item);
    }

    void pop()
    {
        data.pop_back();
    }

    T top() const
    {
        return data.back();
    }
   
private:
    std::vector<T> data;
};

template <typename T>
class queue {
public:
    bool empty() const
    {
        return data.empty();
    }
    
    void enqueue(const T& item)
    {
        data.push_back(item);
    }

    void dequeue()
    {
        data.erase(data.begin(), data.begin()+1);
    }

    T front() const
    {
        return data.front();
    }

private:
    std::vector<T> data;
};

template <typename T>
class stackFromQueue {
public:
    bool empty()
    {
        return queue_out.empty();
    }

    void push(T item)
    {
        queue_in.push(item);
        transfer();
    }

    void pop()
    {
        queue_out.pop();
    }

    T top()
    {
        return queue_out.front();
    }

private:
    void transfer()
    {
        while (!queue_out.empty()) {
            queue_in.push(queue_out.front());
            queue_out.pop();
        }
        
        while (!queue_in.empty()) {
            queue_out.push(queue_in.front());
            queue_in.pop();
        }
    }

    std::queue<T> queue_in;
    std::queue<T> queue_out;
};

template <typename T>
class queueFromStack {
public:
    bool empty()
    {
        return in_stack.empty() && out_stack.empty();
    }

    void enqueue(T item)
    {
        in_stack.push(item);
    }

    void dequeue()
    {
        if (out_stack.empty())
            transfer();

        out_stack.pop();
    }

    T front()
    {
        if (out_stack.empty())
            transfer();

        return out_stack.top();
    }


private:
    void transfer()
    {
        while (!in_stack.empty()) {
            out_stack.push(in_stack.top());
            in_stack.pop();
        }
    }

    std::stack<T> in_stack;
    std::stack<T> out_stack;
};

// ----------------------- Core Stack and Queue ---------------------


// ---------------------- Stack and Queue Problems ------------------
bool balanced_parenthesis(const std::string& s);

bool balanced_symbols(const std::string& s,
                            const std::vector<std::pair<char, char> > & pairs);
int hot_potato(int circle, int k);

std::string decimal_to_base(int n, int base);

int evaluate_postfix(const char* expr);

void infix_2_postfix(const char* expr, char* rpn);

int plus(int x, int y);

int times(int x, int y);

//operation paren = NULL;

//void apply_top(std::stack<operation>* ops, std::stack<int>* vals);

int evaluate(const char* expr);

//template <typename T>
//void level_traversal(Node<T> root, std::ostream& out);

//template <typename T>
//void level_traversal_tokens(Node<T> root, std::ostream& out); 

size_t largest_area(const std::vector<unsigned>& histogram);

// balanced_parenthesis()
// balanced_symbols()
// hot_potato
