#include "stacks_and_queues.h"

// ----------------------- Core Stack and Queue ---------------------


// ---------------------- Stack and Queue Problems ------------------
bool balanced_parenthesis(const std::string& s)
{
    std::stack<char> stack;

    for (size_t i = 0; i < s.size(); ++i) {
        if (s[i] == '(') 
        {
            stack.push('(');
        }
        else if (s[i] == ')')
        {
            if (stack.empty()) return false;
            stack.pop();
        }
    }
    
    return stack.empty();
}

bool balanced_symbols(const std::string& s, 
                            const std::vector<std::pair<char, char> >& pairs)
{
    std::stack<char> stack;
    std::map<char, char> symbols;
    std::set<char> closing;

    for (size_t i = 0; i < pairs.size(); ++i) {
        symbols[pairs[i].first] = pairs[i].second;
        closing.insert(pairs[i].second);
    }
    
    for (size_t i = 0; i < s.size(); ++i) {
        if (symbols.find(s[i]) != symbols.end()) 
        {        
            stack.push(s[i]);
        }
        else if (closing.find(s[i]) != closing.end())
        {
            if (stack.empty()) return false;
            if (symbols[stack.top()] != s[i]) return false;
            stack.pop();
        }
    }
   
    return stack.empty();
}

int hot_potato(int circle, int k)
{
    if (circle < 1) return -1;
    if (circle == 1) return 1;
    
    std::queue<int> queue;
    for (int i = 1; i <= circle; ++i)
        queue.push(i);

    int count = 1;
    while (queue.size() > 1) {
        if (count++ != k)
            queue.push(queue.front());
        else 
            count = 1;
        queue.pop();
    }

    return queue.front();
}

std::string decimal_to_base(int n, int base)
{
    char tmp[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f' }; 
    std::vector<char> trans(tmp, tmp + sizeof(tmp)/sizeof(char));

    std::stack<int> bdigit;

    while (n != 0) {
        bdigit.push(n % base);
        n /= base;
    }

    std::string result;
    while (!bdigit.empty()) {
        result += trans[bdigit.top()];
        bdigit.pop();
    }

    return result;
}

int evaluate_postfix(const char* expr)
{
    std::stack<int> stack;
    char o[] = { '*', '/', '+', '-', '%' };
    std::set<char> op(o, o + sizeof(o)/sizeof(char));
    int result = 0;

    for (const char* c = expr; *c; ++c) {
        if (op.find(*c) == op.end())
        {
            stack.push(*c-'0');
            continue;
        }
        result = stack.top();
        stack.pop();
        switch (*c) {
        case '*':
            result = stack.top() * result;
            break;
        case '/':
            result = stack.top() / result;
            break;
        case '+':
            result = stack.top() + result;
            break;
        case '-':
            result = stack.top() - result;
            break;
        case '%':
            result = stack.top() % result;
            break;
        }
        stack.pop();
        stack.push(result);
    }
    
    return stack.top();

}

void infix_2_postfix(const char* expr, char* rpn);

int plus(int x, int y);

int times(int x, int y);

//void apply_top(std::stack<operation>* ops, std::stack<int>* vals);

int evaluate(const char* expr);

//template <typename T>
//void level_traversal(Node<T> root, std::ostream& out);

//template <typename T>
//void level_traversal_tokens(Node<T> root, std::ostream& out); 

size_t largest_area(const std::vector<unsigned>& histogram)
{
    if (histogram.empty()) return 0;

    size_t max_area = 0;
    std::map<unsigned, unsigned> boxes;
    std::map<unsigned, unsigned>::iterator iter;

    for (size_t i = 0; i < histogram.size(); ++i) {
        if (boxes.find(histogram[i]) == boxes.end()) 
            boxes[histogram[i]] = i;

        if (i+1 < histogram.size() && histogram[i] > histogram[i+1]) 
        {
            iter = boxes.begin();
            while (iter->first < histogram[i+1]) ++iter;
            size_t start_pos = iter->second;
            
            while (iter != boxes.end()) {
                max_area = std::max(max_area, iter->first * (i-iter->second+1));
                std::map<unsigned, unsigned>::iterator temp = iter++;
                boxes.erase(temp);
            }
            
            if (boxes.find(histogram[i+1]) == boxes.end())
                boxes[histogram[i+1]] = start_pos;
        }
    }

    for (iter = boxes.begin(); iter != boxes.end(); ++iter) 
        max_area = std::max(max_area, 
                    iter->first * (histogram.size()-iter->second));
     
    return max_area;
}





























