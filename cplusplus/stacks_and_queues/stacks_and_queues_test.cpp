#include "stacks_and_queues.h"
#include "gtest/gtest.h"

namespace {

class SQTest : public ::testing::Test {
protected:
    SQTest() {}
    virtual ~SQTest() {}

    virtual void SetUp() 
    {
       
    }

    virtual void TearDown() 
    {
    }

};

TEST_F(SQTest, basicStack)
{
    char data[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
    std::vector<char> vec(data, data + sizeof(data)/sizeof(char));
    std::vector<char> expected(vec.begin(), vec.end());
    std::reverse(expected.begin(), expected.end());

    stack<char> s;

    for (size_t i = 0; i < vec.size(); ++i) 
        s.push(vec[i]);
    
    vec.clear();

    while (!s.empty()) {
        vec.push_back(s.top());
        s.pop();
    }

    ASSERT_EQ(vec.size(), expected.size());
    ASSERT_TRUE(std::equal(vec.begin(), vec.end(), expected.begin()));
}

TEST_F(SQTest, basicQueue)
{
    char data[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
    std::vector<char> vec(data, data + sizeof(data)/sizeof(char));
    std::vector<char> expected(vec.begin(), vec.end());

    queue<char> q;

    for (size_t i = 0; i < vec.size(); ++i) 
        q.enqueue(vec[i]);
    
    vec.clear();

    while (!q.empty()) {
        vec.push_back(q.front());
        q.dequeue();
    }

    ASSERT_EQ(vec.size(), expected.size());
    ASSERT_TRUE(std::equal(vec.begin(), vec.end(), expected.begin()));
}

TEST_F(SQTest, queueFromStack)
{
    char data[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
    std::vector<char> vec(data, data + sizeof(data)/sizeof(char));
    std::vector<char> expected(vec.begin(), vec.end());

    queueFromStack<char> q;

    for (size_t i = 0; i < vec.size(); ++i) 
        q.enqueue(vec[i]);
    
    vec.clear();

    while (!q.empty()) {
        vec.push_back(q.front());
        q.dequeue();
    }

    ASSERT_EQ(vec.size(), expected.size());
    ASSERT_TRUE(std::equal(vec.begin(), vec.end(), expected.begin()));
}

TEST_F(SQTest, stackFromqueue)
{
    char data[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
    std::vector<char> vec(data, data + sizeof(data)/sizeof(char));
    std::vector<char> expected(vec.begin(), vec.end());
    std::reverse(expected.begin(), expected.end());

    stackFromQueue<char> s;

    for (size_t i = 0; i < vec.size(); ++i) 
        s.push(vec[i]);
    
    vec.clear();

    while (!s.empty()) {
        vec.push_back(s.top());
        s.pop();
    }

    ASSERT_EQ(vec.size(), expected.size());
    ASSERT_TRUE(std::equal(vec.begin(), vec.end(), expected.begin()));
}

TEST_F(SQTest, largest_area1)
{
    unsigned input[] = { 6, 2, 5, 4, 5, 1, 6, 1, 1, 1, 2, 1 };
    std::vector<unsigned> histo(input, input + sizeof(input)/sizeof(unsigned));
    
    ASSERT_EQ(12, largest_area(histo));
}

TEST_F(SQTest, largest_area2)
{
    unsigned input[] = { 6, 2, 5, 4, 5, 2, 6, 1, 1, 1, 2, 1, 1 };
    std::vector<unsigned> histo(input, input + sizeof(input)/sizeof(unsigned));
    
    ASSERT_EQ(14, largest_area(histo));
}

TEST_F(SQTest, largest_area3)
{
    unsigned input[] = { 6, 2, 5, 4, 5, 1, 6, 1, 1, 1, 2, 1, 1 };
    std::vector<unsigned> histo(input, input + sizeof(input)/sizeof(unsigned));
    
    ASSERT_EQ(13, largest_area(histo));
}

TEST_F(SQTest, balanced_parenthesis)
{
    ASSERT_TRUE(balanced_parenthesis("(()()()())"));
    ASSERT_TRUE(balanced_parenthesis("(((())))"));
    ASSERT_TRUE(balanced_parenthesis("(()((())()))"));

    ASSERT_TRUE(balanced_parenthesis("((a)(b)(c)d(e))"));
    ASSERT_TRUE(balanced_parenthesis("((a(b(c)d)))"));
    ASSERT_TRUE(balanced_parenthesis("((a)((b())(c)))"));
    
    ASSERT_FALSE(balanced_parenthesis("((((((())"));
    ASSERT_FALSE(balanced_parenthesis("()))"));
    ASSERT_FALSE(balanced_parenthesis("(()()(()"));
}

TEST_F(SQTest, balanced_symbols)
{
    char opening[] = { '(', '[', '{' };
    char closing[] = { ')', ']', '}' };
    std::vector<std::pair<char, char> > pairs;
    for (size_t i = 0; i < 3; ++i) 
        pairs.push_back(std::make_pair(opening[i], closing[i]));
        
    ASSERT_TRUE(balanced_symbols("(()()()()) { { ( [ ] [ ] ) } ( ) }", pairs));
    ASSERT_TRUE(balanced_symbols("[ [ { { ( ( ) ) } } ] ]", pairs));
    ASSERT_TRUE(balanced_symbols("[ ] [ ] [ ] ( ) { }", pairs));
    
    ASSERT_TRUE(balanced_symbols("((e)(d)(c)()) f{ { ( [ ] [b] ) } ( ) }", pairs));
    ASSERT_TRUE(balanced_symbols("[ [ { { ( (bcd) ) } } ] ]", pairs));
    ASSERT_TRUE(balanced_symbols("((((a))))", pairs));

    ASSERT_FALSE(balanced_symbols("( [ ) ]", pairs));
    ASSERT_FALSE(balanced_symbols("( ( ( ) ] ) )", pairs));
    ASSERT_FALSE(balanced_symbols("[ { ( ) ]", pairs));
}

TEST_F(SQTest, hot_potato)
{
    ASSERT_EQ(17, (hot_potato(40, 2)));
    ASSERT_EQ(28, (hot_potato(40, 3)));
    ASSERT_EQ(31, (hot_potato(41, 3)));
}

TEST_F(SQTest, base_to_binary)
{ 
    ASSERT_EQ("f", decimal_to_base(15, 16));
    ASSERT_EQ("11111111", decimal_to_base(255, 2));
    ASSERT_EQ("10000001", decimal_to_base(129, 2));
    ASSERT_EQ("1a", decimal_to_base(26, 16));
    ASSERT_EQ("1000000", decimal_to_base(64, 2));
    ASSERT_EQ("1010", decimal_to_base(10, 2));
    ASSERT_EQ("100", decimal_to_base(64, 8));
}

TEST_F(SQTest, evaluate_postfix)
{ 
    ASSERT_EQ(4, evaluate_postfix("38*6/"));
    ASSERT_EQ(-5, evaluate_postfix("762*-"));
    ASSERT_EQ(16, evaluate_postfix("2222***"));
}



} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
