#include "problems_linked_list.h"
#include "singly_linked_list.h"
#include "gtest/gtest.h"
#include "node.h"

namespace {

class ProblemsLinkedListTest : public ::testing::Test {
protected:
    ProblemsLinkedListTest() {}
    ~ProblemsLinkedListTest() {}

    void SetUp() override
    {
        head = nullptr;
    }

    void TearDown() override
    {
        while (head)
        {
            sll::Node* temp = head;
            head = head->next_;
            delete temp;
        }
    }

    sll::Node* head;
};

TEST_F(ProblemsLinkedListTest, midNull)
{
    sll::Node* midpoint = mid(head);
    ASSERT_TRUE(midpoint == nullptr);
}

TEST_F(ProblemsLinkedListTest, midOne)
{
    char input[] = { 3 };
    size_t size = sizeof(input) / sizeof(char);
    
    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);

    sll::Node* midpoint = mid(head);
    ASSERT_TRUE(midpoint);
    ASSERT_EQ(3, midpoint->data_);
}

TEST_F(ProblemsLinkedListTest, midThree)
{
    char input[] = { 4, 2, 3 };
    size_t size = sizeof(input) / sizeof(char);
    
    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);

    sll::Node* midpoint = mid(head);
    ASSERT_TRUE(midpoint);
    ASSERT_EQ(2, midpoint->data_);
}

TEST_F(ProblemsLinkedListTest, midFour)
{
    char input[] = { 11, 4, 2, 3 };
    size_t size = sizeof(input)/sizeof(char);
    
    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);

    sll::Node* midpoint = mid(head);
    ASSERT_TRUE(midpoint);
    ASSERT_EQ(4, midpoint->data_);
}

TEST_F(ProblemsLinkedListTest, 4th_from_end)
{
    char input[] = { 11, 47, 74, 13, 9, 14, 5, 19, 34, 89 };
    size_t size = sizeof(input)/sizeof(char);

    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);

    sll::Node* kthNode = kth_from_end(head, 4);
    ASSERT_TRUE(kthNode);
    ASSERT_EQ(9, kthNode->data_);
}

TEST_F(ProblemsLinkedListTest, kth_from_endNULL)
{
    sll::Node* kthNode = kth_from_end(head, 4);
    ASSERT_TRUE(kthNode == nullptr);
}

TEST_F(ProblemsLinkedListTest, 0th_from_end)
{
    char input[] = { 11, 47, 74, 13, 9, 14, 5, 19, 34, 89 };
    size_t size = sizeof(input)/sizeof(char);

    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);

    sll::Node* kthNode = kth_from_end(head, 0);
    ASSERT_TRUE(kthNode);
    ASSERT_EQ(11, kthNode->data_);
}

TEST_F(ProblemsLinkedListTest, removeValues)
{
    char input[] = { 1, 2, 2, 5, 4, 1, 2, 2 };
    char output[] = { 1, 4, 5, 1 };
    size_t size = sizeof(input)/sizeof(char);
    size_t size_out = sizeof(output)/sizeof(char);

    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);

    head = remove_values(head, 2);
    ASSERT_TRUE(head);

    sll::Node* current = head;
    for (size_t i = 0; i < size_out; ++i) {
        ASSERT_EQ(output[i], current->data_);
        current = current->next_;
    }

    ASSERT_TRUE(current == nullptr);
}

TEST_F(ProblemsLinkedListTest, reverseNull)
{
    head = reverse(head);
    ASSERT_TRUE(head == nullptr);
}

TEST_F(ProblemsLinkedListTest, reverseList)
{
    char input[] = { 11, 17, 3, 47, 31 };
    size_t size = sizeof(input)/sizeof(char);

    size_t i = 0;
    for (; i < size; ++i)
        head = sll::insert(head, input[i]);

    head = reverse(head);
    ASSERT_TRUE(head);

    sll::Node* current = head;
    i = 0;
    while (current && i < size) {
        ASSERT_EQ(current->data_, input[i]);
        ++i;
        current = current->next_;
    }
}

TEST_F(ProblemsLinkedListTest, equalsTrue)
{

    char input_a[] = { 1, 2, 3, 4, 5 };
    char input_b[] = { 1, 2, 3, 4, 5 };

    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;
    size_t size = sizeof(input_a)/sizeof(char);

    for (size_t i = 0; i < size; ++i) {
        head_a = sll::insert(head_a, input_a[i]);
        head_b = sll::insert(head_b, input_b[i]);
    }

    bool result = equals(head_a, head_b);

    ASSERT_TRUE(result);
}

TEST_F(ProblemsLinkedListTest, equalsNot)
{

    char input_a[] = { 1, 2, 3, 4, 5 };
    char input_b[] = { 1, 2, 3, 4 };

    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;
    size_t size_a = sizeof(input_a)/sizeof(char);
    size_t size_b = sizeof(input_b)/sizeof(char);

    for (size_t i = 0; i < size_a; ++i) 
        head_a = sll::insert(head_a, input_a[i]);
    
    for (size_t i = 0; i < size_b; ++i) 
        head_b = sll::insert(head_b, input_b[i]);

    bool result = equals(head_a, head_b);

    ASSERT_FALSE(result);
}

TEST_F(ProblemsLinkedListTest, equalsNotNull)
{

    char input_a[] = { 1, 2, 3, 4, 5 };

    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;
    size_t size = sizeof(input_a)/sizeof(char);

    for (size_t i = 0; i < size; ++i) 
        head_a = sll::insert(head_a, input_a[i]);
    
    bool result = equals(head_a, head_b);

    ASSERT_FALSE(result);
}

TEST_F(ProblemsLinkedListTest, equalsBothNull)
{
    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;

    bool result = equals(head_a, head_b);

    ASSERT_TRUE(result);
}

TEST_F(ProblemsLinkedListTest, mergeABFull)
{
    char input_a[] = { 5, 4, 3, 2, 1 };
    char input_b[] = { 10, 9, 8, 7, 6 };

    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;
    size_t size = sizeof(input_a)/sizeof(char);

    for (size_t i = 0; i < size; ++i) {
        head_a = sll::insert(head_a, input_a[i]);
        head_b = sll::insert(head_b, input_b[i]);
    }

    head = merge(head_a, head_b);
    ASSERT_TRUE(head);

    size_t i = 1;
    sll::Node* current = head;
    while (current && i <= 10) {
        ASSERT_EQ(i, current->data_);
        current = current->next_;
        ++i;
    }
}

TEST_F(ProblemsLinkedListTest, mergeAOnly)
{
    char input_a[] = { 5, 4, 3, 2, 1 };

    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;
    size_t size = sizeof(input_a)/sizeof(char);

    for (size_t i = 0; i < size; ++i) {
        head_a = sll::insert(head_a, input_a[i]);
    }

    head = merge(head_a, head_b);
    ASSERT_TRUE(head);

    size_t i = 1;
    sll::Node* current = head;
    while (current && i <= 10) {
        ASSERT_EQ(i, current->data_);
        current = current->next_;
        ++i;
    }
}

TEST_F(ProblemsLinkedListTest, mergeBOnly)
{
    char input_b[] = { 10, 9, 8, 7, 6 };

    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;
    size_t size = sizeof(input_b)/sizeof(char);

    for (size_t i = 0; i < size; ++i) {
        head_b = sll::insert(head_b, input_b[i]);
    }

    head = merge(head_a, head_b);
    ASSERT_TRUE(head);

    size_t i = 6;
    sll::Node* current = head;
    while (current && i <= 10) {
        ASSERT_EQ(i, current->data_);
        current = current->next_;
        ++i;
    }
}
TEST_F(ProblemsLinkedListTest, mergeBothNull)
{
    sll::Node* head_a = nullptr;
    sll::Node* head_b = nullptr;
    head = merge(head_a, head_b);
    ASSERT_TRUE(head == nullptr);
}

TEST_F(ProblemsLinkedListTest, is_palindromeTrue)
{
    char input[] = { 1, 2, 3, 3, 2, 1 };
    size_t size = sizeof(input)/sizeof(char);
    
    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);
        
    bool result = is_palindrome(head);
    
    ASSERT_TRUE(result);   
}

TEST_F(ProblemsLinkedListTest, is_palindromeFalse)
{
    char input[] = { 1, 2, 3, 4, 2, 1 };
    size_t size = sizeof(input)/sizeof(char);
    
    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);
        
    bool result = is_palindrome(head);
    
    ASSERT_FALSE(result);   
}

TEST_F(ProblemsLinkedListTest, detect_loopFalse)
{
    char input[] = { 1, 2, 3, 4, 5, 6 };
    size_t size = sizeof(input)/sizeof(char);
    
    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);
        
    bool result = detect_loop(head);
    
    ASSERT_FALSE(result);   
}

TEST_F(ProblemsLinkedListTest, detect_loopTrue)
{
    char input[] = { 6, 5, 4, 3, 2, 1 };
    size_t size = sizeof(input)/sizeof(char);
    
    for (size_t i = 0; i < size; ++i)
        head = sll::insert(head, input[i]);
        
    sll::Node* loop = sll::find(head, 3);
    sll::Node* end = sll::find(head, 6);

    ASSERT_EQ(3, loop->data_);
    ASSERT_EQ(6, end->data_);

    end->next_ = loop;

    bool result = detect_loop(head);
    
    ASSERT_TRUE(result);
    
    end->next_ = nullptr;
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
