#include "node.h"

namespace csll {

template <typename T>
Node<T>* insert_n(Node<T>* head, T data);

template <typename T>
Node<T>* insert(Node<T>* head, T data);

template <typename T>
Node<T>* find(Node<T>* head, T data);

template <typename T>
Node<T>* remove(Node<T>* head, T data);

template <typename T>
void print_list(Node<T>* head);

} // end namespace csll
