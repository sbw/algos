#include "circular_doubly_linked_list.h"
#include "gtest/gtest.h"

namespace {

class CircularDoublyLinkedList : public ::testing::Test {
protected:
    CircularDoublyLinkedList() {}
    virtual ~CircularDoublyLinkedList() {}

    virtual void SetUp()
    {
        head = NULL;
        input = "DCBA";
        sorted = "ABCD";
    }

    virtual void TearDown()
    {
        if (!head)
            return;

        Dnode<char>* current = head;
        do {
            Dnode<char>* temp = current;
            current = current->next;
            delete temp;
        } while (current != head);
    }

    Dnode<char>* head;
    std::string input;
    std::string sorted;
};

TEST_F(CircularDoublyLinkedList, Insert)
{
    for (const char* c = input.c_str(); *c; ++c) 
        head = cdll::insert(head, *c);
    
    Dnode<char>* current = head;
    size_t i = 0;

    do {
        ASSERT_EQ(current->data, sorted[i]); 
        current = current->next;
        ++i;
    } while (current != head && i < sorted.size());
}

TEST_F(CircularDoublyLinkedList, findEmpty)
{
    input = "";
    for (const char* c = input.c_str(); *c; ++c)
        head = cdll::insert(head, *c);
        
    head = cdll::find(head, 'A');
    ASSERT_TRUE(head == NULL);    
}

TEST_F(CircularDoublyLinkedList, findHead)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = cdll::insert(head, *c);
        
    head = cdll::find(head, 'A');
    ASSERT_EQ('A', head->data);    
}

TEST_F(CircularDoublyLinkedList, findRear)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = cdll::insert(head, *c);
        
    head = cdll::find(head, 'D');
    ASSERT_EQ('D', head->data);    
}

TEST_F(CircularDoublyLinkedList, removeNull)
{
    input = "";
    for (const char* c = input.c_str(); *c; ++c)
        head = cdll::insert(head, *c);
    
    head = cdll::remove(head, 'A');

    ASSERT_TRUE(head == NULL);
}

TEST_F(CircularDoublyLinkedList, removeSingle)
{
    input = "A";
    for (const char* c = input.c_str(); *c; ++c)
        head = cdll::insert(head, *c);
    
    head = cdll::remove(head, 'A');

    ASSERT_TRUE(head == NULL);
}

TEST_F(CircularDoublyLinkedList, removeFront)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = cdll::insert(head, *c);
    
    head = cdll::remove(head, 'A');

    ASSERT_EQ(head->data, 'B');
}

TEST_F(CircularDoublyLinkedList, removeRear)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = cdll::insert(head, *c);
    
    head = cdll::remove(head, 'D');

    ASSERT_EQ(head->data, 'A');
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

