#include <cstdlib>

namespace sll {

class Node;

Node* mid(Node* head);

Node* kth_from_end(Node* head, size_t k);

Node* remove_values(Node* head, char data);

Node* reverse(Node* head);

bool equals(Node* head_a, Node* head_b);

Node* merge(Node* head_a, Node* head_b);

bool is_palindrome(Node* head);

bool detect_loop(Node* head);

} // end namespace sll
