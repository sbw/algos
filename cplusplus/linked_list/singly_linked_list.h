namespace sll {

class Node;

Node* insert(Node* head, char data);

Node* insert_sorted(Node* head, char data);

Node* find(Node* head, char data);

Node* remove(Node* head, char data);

} // end namespace sll 
