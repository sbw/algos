#ifndef DNODE_H
#define DNODE_H

template <typename T>
struct Dnode {
    Dnode(Dnode* prev, Dnode* next, T data) : 
        prev(prev), next(next), data(data) {}

    Dnode* prev;
    Dnode* next;
    T data;
};

#endif
