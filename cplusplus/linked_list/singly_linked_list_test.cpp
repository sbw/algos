#include "singly_linked_list.h"
#include "node.h"
#include "gtest/gtest.h"

namespace {

class SinglyLinkedTest : public ::testing::Test {
protected:
    SinglyLinkedTest() {}
    ~SinglyLinkedTest() {}

    void SetUp() override
    { 
        head = nullptr;
        input = "DEGAFBCIKJLH";
        sorted = "ABCDEFGHIJKL";
    }

    void TearDown() override
    {
        while (head) {
            sll::Node* temp = head;
            head = head->next_;
            delete temp;
        }    
    }
    
    sll::Node* head;
    std::string input;
    std::string sorted;
};

TEST_F(SinglyLinkedTest, Insert) 
{
    for (const char* c = input.c_str(); *c; ++c)
        head = sll::insert(head, *c);
    ASSERT_TRUE(head);
    
    sll::Node* current = head;
    size_t i = input.size() - 1;

    while (current && i >= 0) { 
        ASSERT_EQ(input[i], current->data_);
        current = current->next_;
        --i;
    }
}

TEST_F(SinglyLinkedTest, InsertSorted)
{
    const char* c = input.c_str();
    for (; *c; ++c)
        head = sll::insert_sorted(head, *c);
    ASSERT_TRUE(head);

    sll::Node* current = head;
    c = sorted.c_str();
    while (current && *c) {
        ASSERT_EQ(*c, current->data_);
        current = current->next_;
        ++c;
    }
}

TEST_F(SinglyLinkedTest,Find)
{  
    for (const char* c = input.c_str(); *c; ++c)
        head = sll::insert(head, *c);
    ASSERT_TRUE(head);

    sll::Node* n = sll::find(head, 'C');
    ASSERT_TRUE(n != NULL);
    ASSERT_EQ('C', n->data_);

    n = sll::find(head, 'Z');
    ASSERT_TRUE(n == NULL);
}

TEST_F(SinglyLinkedTest, RemoveEmpty)
{
    input = "";
    sll::remove(head, 'A');
    ASSERT_TRUE(head == NULL);
}

TEST_F(SinglyLinkedTest, RemoveHead)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = sll::insert(head, *c);
    ASSERT_TRUE(head);
    ASSERT_EQ('H', head->data_);

    head = sll::remove(head, 'H');
    ASSERT_TRUE(head);
    ASSERT_EQ('L', head->data_);
}

TEST_F(SinglyLinkedTest, RemoveEnd)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = sll::insert(head, *c);
    ASSERT_TRUE(head);
    ASSERT_EQ('H', head->data_);

    head = sll::remove(head, 'D');
    ASSERT_TRUE(head);

    sll::Node* current = head;
    while (current->next_)
        current = current->next_;

    ASSERT_EQ('E', current->data_);
}

TEST_F(SinglyLinkedTest, RemoveNotInList)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = sll::insert(head, *c);
    ASSERT_TRUE(head);
    ASSERT_EQ('H', head->data_);
    
    head = sll::remove(head, 'Z');

    ASSERT_TRUE(head);
    sll::Node* current = head;
    size_t i = input.size() - 1;
    while (current && i >= 0) {
        ASSERT_EQ(input[i], current->data_);
        current = current->next_;
        --i;
    }
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
