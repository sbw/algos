#pragma once

namespace sll {

struct Node {
	Node(Node* next, char data) : next_(next), data_(data) {}
	Node* next_;
	char data_;
};

} // end namespace sll
