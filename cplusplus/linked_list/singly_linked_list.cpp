#include "node.h"
#include "singly_linked_list.h"

namespace sll {

Node* insert(Node* head, char data)
{
	return new Node(head, data);
}

Node* insert_sorted(Node* head, char data)
{
	if (!head || head->data_ > data) {
		return new Node(head, data);
	}

	Node* current = head;
	while (current->next_ && current->next_->data_ < data) {
		current = current->next_;
	}

	current->next_ = new Node(current->next_, data);
	return head;
}

Node* find(Node* head, char data)
{
	while (head && head->data_ != data)
		head = head->next_;

	return head;
}

Node* remove(Node* head, char data)
{
	auto rn = [](Node* node) -> Node* {
		Node* temp = node;
		node = node->next_;
		delete temp;
		return node;
	};

	if (!head) return head;
	if (head->data_ == data)
		return rn(head);

	Node* current = head;
	while (current->next_ && current->next_->data_ != data)
		current = current->next_;
	if (current->next_) current->next_ = rn(current->next_);

	return head;
}

} // end namespace sll 
