namespace sll {

Node* mid(Node* head)
{
	return nullptr;
}

Node* kth_from_end(Node* head, size_t k)
{
	return nullptr;
}

Node* remove_values(Node* head, char data)
{
	return nullptr;
}

Node* reverse(Node* head)
{
	return nullptr;
}

bool equals(Node* head_a, Node* head_b)
{
	return true;
}

Node* merge(Node* head_a, Node* head_b)
{
	return nullptr;
}

bool is_palindrome(Node* head)
{
	return true;
}

bool detect_loop(Node* head)
{
	return nullptr;
}

} // end namespace sll
