SLL=singly_linked_list
SLL_TEST=$(SLL)_test
SLL_OBJS=$(SLL).o $(SLL_TEST).o

SLL_PROBLEM=problems_linked_list
SLL_PROBLEM_TEST=$(SLL_PROBLEM)_test
SLL_PROBLEM_OBJS=$(SLL_PROBLEM).o $(SLL_PROBLEM_TEST).o $(SLL).o

all : $(SLL_TEST) $(SLL_PROBLEM_TEST)

$(SLL_TEST) : $(SLL_OBJS)
	$(CC) $(CFLAGS) $(SLL_OBJS) $(LDFLAGS) $(GTEST_LIB) -o $(TEST_DIR)/$(SLL_TEST)

$(SLL_PROBLEM_TEST) : $(SLL_PROBLEM_OBJS)
	$(CC) $(CFLAGS) $(SLL_PROBLEM_OBJS) $(LDFLAGS) $(GTEST_LIB) -o $(TEST_DIR)/$(SLL_PROBLEM_TEST)

%.o : %.cpp
	mkdir -p $(TEST_DIR)
	$(CC) $(CFLAGS) -o $@ -c $<

clean : 
	rm -f *.o
	rm -f $(TEST_DIR)/*



