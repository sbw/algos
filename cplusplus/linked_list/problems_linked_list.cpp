#include "node.h"
#include "problems_linked_list.h"
#include <iostream>

namespace sll {

Node* mid(Node* head)
{
	Node* mid = head;
	while (head) {
		head = head->next_;
		if (head) {
			head = head->next_;
			mid = mid->next_;
		}
	}

	return mid;
}

Node* kth_from_end(Node* head, size_t k)
{
	if (!head) return nullptr;

	Node* trail = head;
	while (head && k) {
		head = head->next_;
		--k;
	}

	while (head && head->next_) {
		head = head->next_;
		trail = trail->next_;
	}

	return trail;
}

Node* remove_values(Node* head, char data)
{
	auto del_node = [](Node* node) -> Node* {
		Node* temp = node;
		node = node->next_;
		delete temp;
		return node;
	};

	if (!head) return nullptr;

	while (head && head->data_ == data)
		head = del_node(head);

	Node* current = head;
	while (current->next_) {
		if (current->next_->data_ == data)
			current->next_ = del_node(current->next_);
		else
			current = current->next_;
	}

	return head;
}

Node* reverse(Node* head)
{
	if (!head) return nullptr;
	Node* prev = nullptr;

	while (head) {
		Node* temp = head->next_;
		head->next_ = prev;
		prev = head;
		head = temp;
	}

	return prev;
}

bool equals(Node* head_a, Node* head_b)
{
	if (!head_a && !head_b)
		return true;
	else if (!head_a)
		return false;
	else if (!head_b)
		return false;

	while (head_a && head_b) {
		if (head_a->data_ != head_b->data_) return false;
		head_a = head_a->next_;
		head_b = head_b->next_;
	}

	return true;
}

Node* merge(Node* head_a, Node* head_b)
{
	if (!head_a) return head_b;
	if (!head_b) return head_a;

	Node* tail_a = head_a;
	while (tail_a->next_)
		tail_a = tail_a->next_;
	tail_a->next_ = head_b;

	return head_a;
}

bool is_palindrome(Node* head)
{
	return true;
}

bool detect_loop(Node* head)
{
	if (!head) return false;

	Node* trail = head;

	while (head) {
		head = head->next_;
		if (head == trail) return true;
		trail = trail->next_;
		if (head) head = head->next_;
	}

	return false;
}



} // end namespace sll
