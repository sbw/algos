#include "dnode.h"

namespace cdll {

template <typename T>
Dnode<T>* insert(Dnode<T>* head, T data);

template <typename T>
Dnode<T>* find(Dnode<T>* head, T data);

template <typename T>
Dnode<T>* remove(Dnode<T>* head, T data);

template <typename T>
void print_list(Dnode<T>* head, bool forward = true);

} // end namespace cdll
