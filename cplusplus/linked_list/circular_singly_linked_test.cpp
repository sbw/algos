#include "circular_singly_linked_list.h"
#include "gtest/gtest.h"

namespace {

class CircularSinglyLinkedList : public ::testing::Test {
protected:
    CircularSinglyLinkedList() {}
    virtual ~CircularSinglyLinkedList() {}

    virtual void SetUp()
    {
        head = NULL;
        input = "DCBA";
        sorted = "ABCD";
    }

    virtual void TearDown()
    {
        if (!head)
            return;

        Node<char>* current = head;
        do {
            Node<char>* temp = current;
            current = current->next;
            delete temp;
        } while (current != head);
    }

    Node<char>* head;
    std::string input;
    std::string sorted;
};

TEST_F(CircularSinglyLinkedList, InsertN)
{
    for (const char* c = input.c_str(); *c; ++c) 
        head = csll::insert_n(head, *c);
    
    Node<char>* current = head;
    size_t i = 0;

    do {
        ASSERT_EQ(current->data, sorted[i]); 
        current = current->next;
        ++i;
    } while (current != head && i < sorted.size());
}

TEST_F(CircularSinglyLinkedList, Insert)
{
    for (const char* c = input.c_str(); *c; ++c) 
        head = csll::insert(head, *c);
    
    Node<char>* current = head;
    size_t i = 0;

    do {
        ASSERT_EQ(current->data, sorted[i]); 
        current = current->next;
        ++i;
    } while (current != head && i < sorted.size());
}

TEST_F(CircularSinglyLinkedList, findEmpty)
{
    input = "";
    for (const char* c = input.c_str(); *c; ++c)
        head = csll::insert(head, *c);
        
    head = csll::find(head, 'A');
    ASSERT_TRUE(head == NULL);    
}

TEST_F(CircularSinglyLinkedList, findHead)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = csll::insert(head, *c);
        
    head = csll::find(head, 'A');
    ASSERT_EQ('A', head->data);    
}

TEST_F(CircularSinglyLinkedList, findRear)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = csll::insert(head, *c);
        
    head = csll::find(head, 'D');
    ASSERT_EQ('D', head->data);    
}

TEST_F(CircularSinglyLinkedList, removeNull)
{
    input = "";
    for (const char* c = input.c_str(); *c; ++c)
        head = csll::insert(head, *c);
    
    head = csll::remove(head, 'A');

    ASSERT_TRUE(head == NULL);
}

TEST_F(CircularSinglyLinkedList, removeSingle)
{
    input = "A";
    for (const char* c = input.c_str(); *c; ++c)
        head = csll::insert(head, *c);
    
    head = csll::remove(head, 'A');

    ASSERT_TRUE(head == NULL);
}

TEST_F(CircularSinglyLinkedList, removeFront)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = csll::insert(head, *c);
    
    head = csll::remove(head, 'A');

    ASSERT_TRUE(head->data == 'B');
}

TEST_F(CircularSinglyLinkedList, removeRear)
{
    for (const char* c = input.c_str(); *c; ++c)
        head = csll::insert(head, *c);
    
    head = csll::remove(head, 'D');

    ASSERT_TRUE(head->data == 'A');
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

