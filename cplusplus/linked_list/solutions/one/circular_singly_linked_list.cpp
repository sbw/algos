#include <iostream>
#include "circular_singly_linked_list.h"

namespace csll {

/*  Cases for insert
    List is empty
    List has 1 element
    Everything else
*/

template <typename T>
Node<T>* insert_n(Node<T>* head, T data)
{
    if (!head)
    {   // O(1)
        head = new Node<T>(head, data);
        head->next = head;
    }
    else if (head == head->next)
    {   // O(1)
        head = new Node<T>(head, data);
        head->next->next = head;
    }
    else
    {   // O(N)
        head = new Node<T>(head, data);
        Node<T>* temp = head->next;
        while (temp->next != head->next)
            temp = temp->next;
        temp->next = head;
    }

    return head;
}

template <typename T>
Node<T>* insert(Node<T>* head, T data)
{
    Node<T>* temp = new Node<T>(NULL, data);

    if (!head)
    {   // O(1)
        head = temp;
        head->next = head;

    }
    else if (head == head->next)
    {   // O(1)
        head->next = temp;
        temp->next = head;
        head = temp;
    }
    else 
    {   // O(1)
        temp->next = head->next;
        head->next = temp;
        T temp_data = temp->data;
        temp->data = head->data;
        head->data = temp_data;
    }

    return head;
}

/*  Cases for find/not find
    Return true/false or node address/NULL
*/
template <typename T>
Node<T>* find(Node<T>* head, T data)
{
    if (!head || head->data == data)
        return head;
    
    // O(N)
    Node<T>* current = head;
    current = current->next;
    while (current != head) {
        if (current->data == data)
            return current;
        current = current->next;
    }

    return NULL;
}

template <typename T>
Node<T>* remove(Node<T>* head, T data)
{
    // List is empty
    if (!head)
        return NULL;
        
    if (head == head->next && head->data == data)
    {
        delete head;
        return NULL;
    }

    //All other cases
    Node<T>* current = head;
    do {
        if (current->data == data)
        {
            Node<T>* temp = current->next;
            current->next = temp->next;
            current->data = temp->data;
            if (temp == head)
                head = current;
            delete temp;
            break;
        }
        current = current->next;
    } while (current != head);   
    
    return head;
}

template <typename T>
void print_list(Node<T>* head)
{
    if (!head)
        return;

    Node<T>* current = head;
    do {
        std::cout << current->data << '\t';
        current = current->next;
    } while (current != head);

    std::cout << std::endl;
}

} // end namespace csll

template Node<char>* csll::insert_n(Node<char>* head, char data);
template Node<char>* csll::insert(Node<char>* head, char data);
template Node<char>* csll::find(Node<char>* head, char data);
template Node<char>* csll::remove(Node<char>* head, char data);
template void csll::print_list(Node<char>* head);
