#include <iostream>
#include "singly_linked_list.h"





int main(int argc, char* argv[])
{
    const char* input = "ABCDEFGH";
    const char input2[] = "ABCDEFGH";

    Node<char>* head = NULL;

    for (const char* c = input; *c; ++c)
        head = sll::insert(head, *c);
    
    while (head) 
    {
        std::cout << head->data << std::endl;
        Node<char>* temp = head;
        head = head->next;
        delete temp;
    }

    head = NULL;
    size_t len = sizeof(input2) - 1;
    for (size_t i = 0; i < len; ++i)
        head = sll::insert_c(head, input2[i]);    

    while (head)
    {
        std::cout << head->data << std::endl;
        Node<char>* temp = head;
        head = head->next;
        delete temp;
    }

    return 0;
}
