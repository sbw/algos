#include <iostream>
#include "circular_doubly_linked_list.h"

namespace cdll {

template <typename T>
Dnode<T>* insert(Dnode<T>* head, T data)
{
    Dnode<T>* new_head = new Dnode<T>(NULL, NULL, data);

    if (!head)
    {
        head = new_head;
        head->prev = new_head;
        head->next = new_head;    
    }

    new_head->next = head;
    new_head->prev = head->prev;
    head->prev->next = new_head;
    head->prev = new_head;

   return new_head;
}

template <typename T>
Dnode<T>* find(Dnode<T>* head, T data)
{
    if (!head) return NULL;

    Dnode<T>* current = head;
    do {
        if (current->data == data)
            return current;
        current = current->next;
    } while (current != head);

    return NULL;
}

template <typename T>
Dnode<T>* remove(Dnode<T>* head, T data)
{
    if (!head) return NULL;

    if (head == head->next && head->data == data)
    {
        delete head;
        return NULL;
    }

    Dnode<T>* current = head;
    do {
        if (current->data == data)
        {
            Dnode<T>* temp = current;
            current->prev->next = current->next;
            current->next->prev = current->prev;
            if (head == current)
                head = current->next;
            delete current;
            break;
        }
        current = current->next;
    } while (current != head);

    return head;
}

template <typename T>
void print_list(Dnode<T>* head, bool forward)
{
    if (!head) return;
    
    Dnode<T>* current = head;
    do {
        std::cout << current->data << '\t';
        if (forward)
            current = current->next;
        else
            current = current->prev;
    } while (current != head);
    
    std::cout << std::endl;
}

} // end namespace cdll


template Dnode<char>* cdll::insert(Dnode<char>* head, char data);
template Dnode<char>* cdll::find(Dnode<char>* head, char data);
template Dnode<char>* cdll::remove(Dnode<char>* head, char data);
template void cdll::print_list(Dnode<char>* head, bool forward);
