#include "problems_linked_list.h"
#include <stack>

template <typename T>
Node<T>* mid(Node<T>* head)
{
    Node<T>* mid = head;
    while (head) {
        head = head->next;
        if (head)
        {
            head = head->next;
            mid = mid->next;
        }
    }

    return mid;
}

template <typename T>
Node<T>* kth_from_end(Node<T>* head, size_t k)
{
    Node<T>* kthNode = head;
    while (head && k) {
        head = head->next;
        --k;
    }

    while (head && head->next) {
        head = head->next;
        kthNode = kthNode->next;
    }

    return kthNode;
}

template <typename T>
Node<T>* remove_values(Node<T>* head, T data)
{
    while (head && head->data == data) {
        Node<T>* temp = head;
        head = head->next;
        delete temp;       
    }
    
    Node<T>* current = head;
    while (current) {
        while (current->next && current->next->data == data) {
            Node<T>* temp = current->next;
            current->next = temp->next;
            delete temp;
        }
        current = current->next;
    }

    return head;
}

template <typename T>
Node<T>* reverse(Node<T>* head)
{
    Node<T>* prev = NULL;

    while (head) {
        Node<T>* temp = head->next;
        head->next = prev;
        prev = head;
        head = temp;
    }

    return prev;
}

template <typename T>
bool equals(Node<T>* head_a, Node<T>* head_b)
{
    while (head_a && head_b) {
        if (head_a->data != head_b->data)
            return false;
        head_a = head_a->next;
        head_b = head_b->next;
    }

    if (head_a != NULL || head_b != NULL)
        return false;
        
    return true;
}

template <typename T>
Node<T>* merge(Node<T>* head_a, Node<T>* head_b)
{
    if (!head_a || !head_b)
        return (head_a) ? head_a : head_b;
 
    Node<T>* tail_a = head_a;
    while (tail_a->next)
        tail_a = tail_a->next;
    tail_a->next = head_b;

    return head_a;
}

template <typename T>
bool is_palindrome(Node<T>* head)
{
    std::stack<T> stack;
    Node<T>* current = head;
    while (current) {
        stack.push(current->data);
        current = current->next;
    }

    while (!stack.empty()) {
        T data = stack.top();
        stack.pop();
        if (head->data != data)
            return false;
        head = head->next;        
    }

    return true;
}

template <typename T>
bool detect_loop(Node<T>* head)
{
    Node<T>* trail = head;
    while (head) {
        head = head->next;
        if (head == trail)
            return true;        
        trail = trail->next;
        if (head)
            head = head->next;
    }

    return false;
}

template Node<int>* mid(Node<int>* head);
template Node<int>* kth_from_end(Node<int>* head, size_t k);
template Node<int>* reverse(Node<int>* head);
template Node<int>* merge(Node<int>* a, Node<int>* b);
template bool equals(Node<int>* a, Node<int>* b);
template bool is_palindrome(Node<int>* head);
template bool detect_loop(Node<int>* head);
template Node<int>* remove_values(Node<int>* head, int value);
