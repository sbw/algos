#include "singly_linked_list.h"

/*  Cases for insert:
        List is empty
        List is not empty
*/

template <typename T>
Node<T>* sll::insert(Node<T>* head, T data)
{
    return new Node<T>(head, data);
}

template <typename T>
Node<T>* sll::insert_c(Node<T>* head, T data)
{
    Node<T>* new_head = new Node<T>;
    new_head->next = head;
    new_head->data = data;

    return new_head;
}

/* Cases for insert sorted:
    List is empty
    List not empty, new item first
    List not empty, new item last
    All else
*/


template <typename T>
Node<T>* sll::insert_sorted(Node<T>* head, T data)
{
    if (!head || data <= head->data)
        return new Node<T>(head, data);

    Node<T>* current = head;
    while (current->next && current->next->data < data)
        current = current->next;
    current->next = new Node<T>(current->next, data); 
    
    return head;
}

template <typename T>
Node<T>* sll::insert_sorted_c(Node<T>* head, T data)
{
    Node<T>* new_node = new Node<T>;
    new_node->next = head;
    new_node->data = data;

    if (!head || data <= head->data) 
        return new_node;

    Node<T>* current = head;
    while (current->next && current->next->data < data)
        current = current->next;
    new_node->next = current->next;
    current->next = new_node;
    return head;
}

/*  Cases for find:
    What to return? True/False or actual pointer to node?
*/

template <typename T>
Node<T>* sll::find(Node<T>* head, T data)
{
    while (head && head->data != data)
        head = head->next;

    return head;
}

/* Cases for removal:
    Assumptions: is it known the item is in the list?
    If not, have to care about:
        List is empty
        Item not in list
    What to return? head of list
    
    List is size 1
    Remove front node
    Remove rear node
*/

template <typename T>
Node<T>* sll::remove(Node<T>* head, T data)
{
    if (!head)
        return head;

    Node<T>* current = head;
    if (head->data == data)
    {
        head = head->next;
        delete current;
        return head;
    }

    while (current->next && current->next->data != data)
        current = current->next;

    Node<T>* temp = current->next;
    if (temp)
    {
        current->next = temp->next;
        delete temp;
    }

    return head;
}

//
template Node<char>* sll::insert(Node<char>* head, char data);
template Node<int>* sll::insert(Node<int>* head, int data);
template Node<char>* sll::insert_c(Node<char>* head, char data);
template Node<char>* sll::insert_sorted(Node<char>* head, char data);
template Node<char>* sll::insert_sorted_c(Node<char>* head, char data);
template Node<char>* sll::find(Node<char>* head, char data);
template Node<int>* sll::find(Node<int>* head, int data);
template Node<char>* sll::remove(Node<char>* head, char data);
