#include <iostream>
#include "recursion.h"

unsigned sum_to_n(unsigned n) 
{
    if (n == 0) return 0;
	return n + sum_to_n(n-1);
}

unsigned sum_to_n_tail(unsigned n)
{
    unsigned sum = 0;
    return sum_helper(n, sum);
}

unsigned sum_helper(unsigned n, unsigned sum)
{
    if (n == 0) return sum;
    return sum_helper(n-1, sum+n);
}

unsigned factorial(unsigned n)
{   
    if (n == 0) return 1;
    return n * factorial(n-1);
}

unsigned factorial_tail(unsigned n)
{
    unsigned p = 1;
    return factorial_helper(n, p);
}

unsigned factorial_helper(unsigned n, unsigned p)
{
    if (n == 0) return p;
    return factorial_helper(n-1, n*p);
}

bool balanced_parens_helper(const char* s, size_t left, size_t right)
{
	if (!s || !*s) return (left-right) == 0;
	if (right > left) return false;

	if (*s == '(')
		return balanced_parens_helper(s+1, left+1, right);
	else
		return balanced_parens_helper(s+1, left, right+1);
}

bool balanced_parens(const std::string& s)
{
	return balanced_parens_helper(s.c_str(), 0, 0);
}

void powerset_output(std::vector<std::vector<char>>& pset)
{

}

void powerset(std::vector<std::vector<char>>& pset, std::vector<char>& s)
{

}

void perm(std::deque<char>& prefix, std::deque<char>& current)
{

}

void move(int from, int to)
{
	std::cout << "from: " << from << " to: " << to << std::endl;
}

void hanoi(int n, int src, int dest, int temp)
{
	if (n == 0) return;

	hanoi(n-1, src, temp, dest);
	move(src, dest);
	hanoi(n-1, temp, dest, src);
}

std::pair<int, int> normalize_downward(int row, int col);

std::pair<int, int> normalize_upward(int row, int col, int dim);

void nqueens(NQueensState* state, 
    std::set<std::set<std::pair<int, int> > > solutions,
    std::set<std::pair<int, int> > solution);

//Knights Tour

