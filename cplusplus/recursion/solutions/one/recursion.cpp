#include <iostream>
#include "recursion.h"

unsigned sum_to_n(unsigned n) 
{
    if (n == 0) return 0;
    return n + sum_to_n(n-1);
}

unsigned sum_to_n_tail(unsigned n)
{
    return sum_helper(n, 0);
}

unsigned sum_helper(unsigned n, unsigned sum)
{
    if (n == 0) return sum;
    return sum_helper(n-1, n+sum);
}

unsigned factorial(unsigned n)
{   
    if (n == 0) return 1;
    return n * factorial(n-1);
}

unsigned factorial_tail(unsigned n)
{
    return factorial_helper(n, 1);
}

unsigned factorial_helper(unsigned n, unsigned p)
{
    if (n == 0) return p;
    return factorial_helper(n-1, n*p);
}

void balanced_parens(std::string prefix, int num_left, int num_right)
{
    if (num_left == 0 && num_right == 0) {
        std::cout << prefix << std::endl;   
        return;
    }

    if (num_left > 0) 
        balanced_parens(prefix + '(', num_left-1, num_right);
    
    if (num_right > num_left)
        balanced_parens(prefix + ')', num_left, num_right-1);
}

void perm(std::deque<char>& prefix, std::deque<char>& current)
{
    if (current.empty()) {
        for (auto& item : prefix)
            std::cout << item << ' ';
        std::cout << std::endl;
        return;
    }

    for (size_t i = 0; i < current.size(); ++i) {
        char c = current.front();
        current.pop_front();
        prefix.push_back(c);
        perm(prefix, current);
        prefix.pop_back();
        current.push_back(c);
    }
}

void powerset_output(std::vector<std::vector<char>>& pset)
{
        for (size_t i = 0; i < pset.size(); ++i) {
            std::cout << "{ ";
            size_t j = 0;
            for (; j < pset[i].size(); ++j) { 
                std::cout << pset[i][j];
                if (j < pset[i].size()-1)
                    std::cout << ", ";
            }
            std::cout << " }" << std::endl;
        }
}

void powerset(std::vector<std::vector<char>>& pset, std::vector<char>& s)
{
    if (s.empty()) {
        pset.push_back(std::vector<char>());
        powerset_output(pset);
        return;    
    }

    char new_elem = s.back();
    s.pop_back();
    std::vector<std::vector<char>> set_of_sets = pset;
    for (auto& set : set_of_sets) 
        set.push_back(new_elem);
    pset.push_back(std::vector<char>(1, new_elem));
    pset.insert(pset.end(), set_of_sets.begin(), set_of_sets.end());

    powerset(pset, s);
}

void move(int from, int to)
{
    std::cout << "from: " << from << " to: " << to << std::endl;
}

void hanoi(int n, int src, int dest, int temp)
{
    if (n == 0) return;   
    
    hanoi(n-1, src, temp, dest);
    move(src, dest);
    hanoi(n-1, temp, dest, src);
}

std::pair<int, int> normalize_downward(int row, int col);

std::pair<int, int> normalize_upward(int row, int col, int dim);

void nqueens(NQueensState* state, 
    std::set<std::set<std::pair<int, int> > > solutions,
    std::set<std::pair<int, int> > solution);

//Knights Tour

