#include "recursion.h"
#include "gtest/gtest.h"

namespace {

class RecursionTest : public ::testing::Test {
protected:
    RecursionTest() {}
    virtual ~RecursionTest() {}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

TEST_F(RecursionTest, sum_to_n100)
{
    ASSERT_EQ(5050, sum_to_n(100));
}

TEST_F(RecursionTest, sum_to_n_tail100)
{
    ASSERT_EQ(5050, sum_to_n_tail(100));
}

TEST_F(RecursionTest, factorial6)
{
    ASSERT_EQ(720, factorial(6));
}

TEST_F(RecursionTest, factorial_tail6)
{
    ASSERT_EQ(720, factorial_tail(6));
}

TEST_F(RecursionTest, balanced_parens1)
{
    ASSERT_TRUE(balanced_parens("(()()()())"));
}

TEST_F(RecursionTest, balanced_parens2)
{
	ASSERT_TRUE(balanced_parens("(((())))"));
}

TEST_F(RecursionTest, balanced_parens3)
{
	ASSERT_TRUE(balanced_parens("(()((())()))"));
}

TEST_F(RecursionTest, balanced_parens4)
{
	ASSERT_FALSE(balanced_parens("((((((())"));
}

TEST_F(RecursionTest, balanced_parens5)
{
	ASSERT_FALSE(balanced_parens("()))"));
}

TEST_F(RecursionTest, balanced_parens6)
{
	ASSERT_FALSE(balanced_parens("(()()(()"));
}

TEST_F(RecursionTest, balanced_parens7)
{
	ASSERT_FALSE(balanced_parens("))(("));
}

TEST_F(RecursionTest, TowerOfHanoi)
{
    hanoi(3, 1, 3, 2);
}

TEST_F(RecursionTest, Permutation)
{
    std::deque<char> start;
    start.push_back('a');
    start.push_back('b');
    start.push_back('c');
    start.push_back('d');

    std::deque<char> perms;

    perm(perms, start);
}

TEST_F(RecursionTest, Powerset)
{
    std::vector<std::vector<char>> pset;
    std::vector<char> s = { 'z', 'y', 'x' };
    powerset(pset, s);
}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
