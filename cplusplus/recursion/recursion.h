#include <set>
#include <deque>
#include <vector>
#include <string>


unsigned sum_to_n(unsigned n);

unsigned sum_to_n_tail(unsigned n);

unsigned sum_helper(unsigned n, unsigned sum);

unsigned factorial(unsigned n);

unsigned factorial_tail(unsigned n);

unsigned factorial_helper(unsigned n, unsigned p);

bool balanced_parens_helper(const char*, size_t, size_t);

bool balanced_parens(const std::string&);

void move(int from, int to);

void hanoi(int num, int src, int dest, int temp);

// N-Queens
struct NQueensState {
    NQueensState(int n)
    {
        dim = n;
        for (int i = 0; i < n; ++i) {
            rows.insert(i);
            cols.insert(i);
        }
    }
    
    int dim;
    std::set<int> rows;
    std::set<int> cols;
    std::set<std::pair<int, int> > down_diag;
    std::set<std::pair<int, int> > up_diag;
};

std::pair<int, int> normalize_downward(int row, int col);

std::pair<int, int> normalize_upward(int row, int col, int dim);

void nqueens(NQueensState* state, 
    std::set<std::set<std::pair<int, int> > > solutions,
    std::set<std::pair<int, int> > solution);

//Knights Tour


void perm(std::deque<char>& prefix, std::deque<char>& current);
void powerset(std::vector<std::vector<char>>& pset, std::vector<char>& set);
