#include <iostream>
#include "recursion.h"

unsigned sum_to_n(unsigned n) 
{

}

unsigned sum_to_n_tail(unsigned n)
{

}

unsigned sum_helper(unsigned n, unsigned sum)
{

}

unsigned factorial(unsigned n)
{   

}

unsigned factorial_tail(unsigned n)
{

}

unsigned factorial_helper(unsigned n, unsigned p)
{

}

bool balanced_parens_helper(const char* s, size_t left, size_t right)
{

}

bool balanced_parens(const std::string& s)
{

}

void powerset_output(std::vector<std::vector<char>>& pset)
{

}

void powerset(std::vector<std::vector<char>>& pset, std::vector<char>& s)
{

}

void perm(std::deque<char>& prefix, std::deque<char>& current)
{

}

void move(int from, int to)
{
	std::cout << "from: " << from << " to: " << to << std::endl;
}

void hanoi(int n, int src, int dest, int temp)
{

}

std::pair<int, int> normalize_downward(int row, int col);

std::pair<int, int> normalize_upward(int row, int col, int dim);

void nqueens(NQueensState* state, 
    std::set<std::set<std::pair<int, int> > > solutions,
    std::set<std::pair<int, int> > solution);

//Knights Tour

