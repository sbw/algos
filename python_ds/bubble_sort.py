#!/usr/bin/env python3

def bubbleSort(arr):
    upper = len(arr) - 1

    while upper > 0:
        for k in range(upper):
            if arr[k] > arr[k + 1]:
                temp = arr[k]
                arr[k] = arr[k + 1]
                arr[k + 1] = temp
        upper -= 1
        print(arr)

    return arr

def bubbleSort2(arr):
    upper = len(arr) - 1
    anySwapped = True

    while upper > 0 and anySwapped:
        anySwapped = False
        for k in range(upper):
            if arr[k] > arr[k + 1]:
                temp = arr[k]
                arr[k] = arr[k + 1]
                arr[k + 1] = temp
                anySwapped = True
        upper -= 1
        print(upper)
        print(arr)
        
    return arr 


def main():
    
    arr = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    a1 = bubbleSort2(arr[:])
    bubbleSort2(a1)

#
main()
