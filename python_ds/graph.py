#

import enum

class Color(enum.Enum):
    white = 0
    gray = 1
    black = 2

# ----- GRAPH Class -----
class Graph():
    def __init__(self, directed = False):
        self.vertices = {}
        self.directed = directed

    def getVertices(self):
        return self.vertices

    def addVertex(self, name):
        self.vertices[name] = Vertex(name)

    def getVertex(self, name):
        if name in self.vertices:
            return self.vertices[name]
        else:
            return

    def addEdge(self, source, dest, weight = 0):
        if source not in self.vertices:
            self.addVertex(source)
        if dest not in self.vertices:
            self.addVertex(dest)
        s = self.getVertex(source)
        d = self.getVertex(dest)
        s.addNeighbor(d, weight)

        if not self.directed:
            d.addNeighbor(s, weight)

    def display(self):
        backArrow = ' '
        if not self.directed:
            backArrow = ' <'

        for vertex in self.vertices.values():
            print('vertex {}:\t'.format(vertex.key), end = '')
            for n in vertex.getNeighbors():
                print('{}{}--{}--> {}\t'.format(
                    vertex.key, backArrow, vertex.getEdgeWeight(n), n), end = '')
            print()

    def resetVertices(self):
        for vertex in self.vertices.values():
            vertex.reset()

# ------ VERTEX Class -----
class Vertex():
    def __init__(self, key, color = Color.white, 
        distance = 0, predecessor = None):
        self.key = key
        self.neighbors = {}
        self.weights = {}
        self.color = color
        self.distance = distance
        self.predecessor = predecessor

    def addNeighbor(self, neighbor, weight = 0):
        self.neighbors[neighbor.key] = neighbor
        self.weights[neighbor.key] = weight

    def getNeighbors(self):
        return self.neighbors

    def getEdgeWeight(self, neighbor):
        return self.weights[neighbor]

    def setColor(self, color):
        self.color = color

    def getColor(self):
        return self.color

    def setDistance(self, distance):
        self.distance = distance

    def getDistance(self):
        return self.distance

    def setPredecessor(self, precedessor):
        self.predecessor = precedessor

    def getPredecessor(self):
        return self.predecessor

    def reset(self, color = Color.white, distance = 0, predecessor = None):
        self.color = color
        self.distance = distance
        self.predecessor = predecessor

    def __lt__(self, rhs):
        return self.distance < rhs.distance

    def __eq__(self, rhs):
        return self.distance == rhs.distance

if __name__ == '__main__':

    nodes = {
        'a': [('b', 1)],
        'b': [('c', 2)],
        'c': [('a', 0), ('d', 0)],
        'd': [('b', 1)],
    }

    print('Testing directed, weighted')    
    graph = Graph(True)
    for vertex, neighbors in nodes.items(): 
        for dest, weight in neighbors:
            graph.addEdge(vertex, dest, weight)
    
    graph.addEdge('e', 'f', 3)
    graph.addEdge('b', 'e', 2)
    graph.addEdge('a', 'c', 4)

    graph.display()
    
    print('Testing undirected, not weighted')

    graph = Graph()
    for vertex, neighbors in nodes.items(): 
        for dest, weight in neighbors:
            graph.addEdge(vertex, dest)
    
    graph.addEdge('e', 'f')
    graph.addEdge('b', 'e')
    graph.addEdge('a', 'c')

    graph.display()
