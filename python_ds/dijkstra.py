#

from heapq import *
from graph import *


def dijkstra(start):
    heap = []
 
    start.setColor(Color.gray)
    start.setDistance(0.0)
    heappush(heap, start)
    
    while len(heap) != 0:
        current = heap[0]
        
        for neighbor in current.getNeighbors().values():
            if neighbor.getColor() == Color.white:
                weight = current.getEdgeWeight(neighbor.key) 
                neighbor.setDistance(weight + current.distance)   
                neighbor.setColor(Color.gray)            
                heappush(heap, neighbor)
                print('\tPushing into heap ' + \
                            'coloring gray: {} '.format(neighbor.key))
        top = heappop(heap)
        top.setColor(Color.black)
        print('Popping {} off heap, coloring black'.format(top.key))

if __name__ == '__main__':

    data = [
        (4,5,0.35),
        (5,4,0.35),
        (4,7,0.37),
        (5,7,0.28),
        (7,5,0.28),
        (5,1,0.32),
        (0,4,0.38),
        (0,2,0.26),
        (7,3,0.39),
        (1,3,0.29),
        (2,7,0.34),
        (6,2,0.40),
        (3,6,0.52),
        (6,0,0.58),
        (6,4,0.93)
    ]
    g = Graph(True)

    for source, dest, weight in data:
        g.addEdge(source, dest, weight)

    for vertex in g.getVertices().values():
        vertex.setDistance(999999.0)

    g.display()
    start = g.getVertex(0)
    dijkstra(start)
    
    print('Distance from vertex: {} '.format(start.key))
    for vertex in g.getVertices().values():
        print('{} ---> {} : {} '.format(start.key, 
                    vertex.key, vertex.getDistance()))
        
