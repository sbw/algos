#!/usr/bin/env python3

import argparse
import stack

parser = argparse.ArgumentParser(description = 'balanced parenthesis')
parser.add_argument('input_string', metavar = 'a', type = str)
args = parser.parse_args()

print(args.input_string) 

isBalanced = True

mystack = stack.Stack()

left_symbols  = [ c for c in '<{[(']
right_symbols = [ c for c in '>}])']

print(left_symbols)
print(right_symbols)

mystack = stack.Stack()

for c in args.input_string:
    if c in left_symbols:
        mystack.push(c)
    elif c in right_symbols: 
        if mystack.isEmpty():
            isBalanced = False
            break
        i = right_symbols.index(c)
        if left_symbols[i] == mystack.peek():
            mystack.pop()
        else:
            isBalanced = False
            break
        
if isBalanced == True and mystack.isEmpty():
    print('Balanced')
else:
    print ('Not balanced')

