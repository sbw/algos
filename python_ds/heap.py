# Heap

import enum

class HeapType(enum.Enum):
    MinHeap = 1
    MaxHeap = 2

class Heap():
    def __init__(self, heapType):
        self.heap = []
        self.heapType = heapType

    def findParent(self, child):
        parent = 0
        if child != 0:
            parent = (child - 1) // 2
        return parent

    def findLeft(self, parent):
        return 2 * parent + 1

    def findRight(self, parent):
        return 2 * parent + 2

    def heapify(self):
        c = len(self.heap) - 1
        p = self.findParent(c)

        if self.heapType == HeapType.MinHeap:
            while self.heap[c] < self.heap[p]:
                self.heap[c], self.heap[p] = self.heap[p], self.heap[c]
                c = p
                p = self.findParent(c)
        elif self.heapType == HeapType.MaxHeap:
            while self.heap[c] > self.heap[p]:
                self.heap[c], self.heap[p] = self.heap[p], self.heap[c]
                c = p
                p = self.findParent(c)

    def remove(self):
        top = self.heap[0]
        self.heap[0] = self.heap[-1]
        self.heap.pop()

        return top

    def findChild(self, parent):
        left = self.findLeft(parent)
        right = self.findRight(parent)

        if right > (len(self.heap) - 1):
            return left

        if self.heapType == HeapType.MinHeap:
            if self.heap[left] < self.heap[right]:
                return left
        elif self.heapType == HeapType.MaxHeap:
            if self.heap[left] > self.heap[right]:
                return left

        return right

    def minChild(self, parent):
        return self.findChild(parent)

    def maxChild(self, parent):
        return self.findChild(parent)

    def reheap(self):
        parent = 0
        while self.findLeft(parent) < len(self.heap):

            if self.heapType == HeapType.MinHeap:                
                mc = self.minChild(parent)
                if self.heap[parent] > self.heap[mc]:
                    self.heap[parent], self.heap[mc] = \
                        self.heap[mc], self.heap[parent]
            elif self.heapType == HeapType.MaxHeap:
                mc = self.maxChild(parent)
                if self.heap[parent] < self.heap[mc]:
                    self.heap[parent], self.heap[mc] = \
                        self.heap[mc], self.heap[parent]
            parent = mc

    def insert(self, data):
        self.heap.append(data)
        self.heapify()

    def removeMin(self):
        top = self.remove()
        self.reheap()
        return top

    def removeMax(self):
        top = self.remove()
        self.reheap()
        return top

    def isEmpty(self):
        return self.heap == []

if __name__ == '__main__':
    arr = [ 27, 19, 14, 33, 9, 18, 17, 5, 21, 11]
    print('Testing minheap')
    print('Test values inserted: 27 19 14 33 9 18 17 5 21 11')
    minHeap = Heap(HeapType.MinHeap)
    for x in arr:
        minHeap.insert(x)
    while not minHeap.isEmpty():
        top = minHeap.removeMin()
        print('{} '.format(top), end = '')
    print('\n')
    
    print('Testing maxheap')
    print('Test values inserted: 27 19 14 33 9 18 17 5 21 11')
    maxHeap = Heap(HeapType.MaxHeap)
    for x in arr:
        maxHeap.insert(x)
    while not maxHeap.isEmpty():
        top = maxHeap.removeMax()
        print('{} '.format(top), end = '')
    print('\n')

