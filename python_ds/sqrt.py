#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description = 'Find square root.')
parser.add_argument('integer', metavar = 'n', type = int,
                    help = 'find square root of integer')
args = parser.parse_args()


def squareroot(n):
    root = n/2
    for k in range(20):
        print("root: {}  n/root: {}".format(root, n/root))
        root = (1/2) * (root + (n / root))
    return root

print(squareroot(args.integer))
