#!/usr/bin/env python3

def swap(a, b):
    temp = a
    a = b
    b = temp

def partition(arr, left, right):
    pivot = left
    left += 1

    while True:
        while arr[left] < arr[pivot] and left <= right:
            left += 1
        while arr[right] > arr[pivot] and left <= right:
            right -= 1

        if left < right:
            arr[left], arr[right] = arr[right], arr[left]
            left += 1
            right -= 1
            print(arr)
        else:
            break

    arr[pivot], arr[right] = arr[right], arr[pivot]
    print(arr)
    return right

def quicksort(arr, left, right):
    if right - left < 1:
        return

    pivot = partition(arr, left, right)
    print(pivot)
    quicksort(arr, left, pivot - 1)
    quicksort(arr, pivot + 1, right)

def main():
    arr = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    arr2 = [10, 10, 10, 10, 10, 10]
    quicksort(arr, 0, len(arr) - 1)
    quicksort(arr2, 0, len(arr2) - 1)

#----Program----
main()

