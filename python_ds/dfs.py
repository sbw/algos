#
from collections import deque
from graph import *

def dfs(start, finish = None):
    result = []

    end = finish.key if finish else 'None'
    print('-------- start: {} finish: {} --------'.format(start.key, end))

    stack = deque()
    start.setColor(Color.gray)
    stack.append(start)
    print('Pushing {} on to stack, coloring gray'.format(start.key))

    while len(stack) != 0:
        current = stack[-1]
        old = len(stack)
        #
        if finish is not None and current.key == finish.key:
            print('Found...')
            while current is not None:
                result.append(current.key)
                current = current.getPredecessor()
            result.reverse()
            return result
                
        print('\tPushing on to stack, coloring gray: ', end = '')
        for neighbor in current.getNeighbors().values():
            if neighbor.getColor() == Color.white:
                print(' {} '.format(neighbor.key), end = '')
                neighbor.setColor(Color.gray)
                neighbor.setDistance(current.distance + 1)
                neighbor.setPredecessor(current)
                stack.append(neighbor)
        print()
        if old == len(stack):
            print('Popping {} off stack, coloring black'.format(current.key))
            top = stack.pop()
            top.setColor(Color.black)
            

if __name__ == '__main__':
    print('Testing dfs with graph:')
    print('                       ')
    print('         A   G         ')
    print('        / \ / \        ')
    print('       B   C   |       ')
    print('        \ /    |       ')
    print('         D     |       ')
    print('          \    |       ')
    print('           E   |       ')
    print('            \ /        ')
    print('             F         ')
    print('            /          ')
    print('           H           ')
    
    nodes = 'ABCDEFGH'
    g = Graph()
    for node in nodes:
        g.addVertex(node)
    g.addEdge('A', 'B')
    g.addEdge('A', 'C')
    g.addEdge('B', 'D')
    g.addEdge('C', 'D')
    g.addEdge('C', 'G')
    g.addEdge('D', 'E')
    g.addEdge('E', 'F')
    g.addEdge('F', 'G')
    g.addEdge('F', 'H')

    g.display()

    print(dfs(g.getVertex('A')))
    g.resetVertices()
    print(dfs(g.getVertex('A'), g.getVertex('A')))
    g.resetVertices()
    print(dfs(g.getVertex('A'), g.getVertex('H')))
    g.resetVertices()
    print(dfs(g.getVertex('A'), g.getVertex('F')))
    g.resetVertices()
    print(dfs(g.getVertex('A'), g.getVertex('E')))
    g.resetVertices()
    print(dfs(g.getVertex('A'), g.getVertex('D')))
    g.resetVertices()
    print(dfs(g.getVertex('A'), g.getVertex('Z')))
