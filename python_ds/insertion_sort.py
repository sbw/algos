#!/usr/bin/env python3

def insertionSort(arr):
    for upper in range(1, len(arr)):
       temp = arr[upper]        
       k = upper - 1
       while k > -1 and arr[k] > temp:
           arr[k + 1] = arr[k]
           k -= 1
       arr[k + 1] = temp
       print(arr)

    return arr


def main():
    arr = [54, 26, 93, 17, 77, 31, 44, 13, 20]
    insertionSort(arr)
    
#----Program Start----
main()
