
def powerset(s, pset):
    if not s:
        return

    current = s.pop()
    for subset in pset[:]:
        new_set = subset.copy()    
        new_set.add(current)
        pset.append(new_set)

    powerset(s, pset)
    return     

def powerset_iter(items):
    pset = [set()]

    for item in items:
        for subset in pset[:]:
            new_set = subset.copy()
            new_set.add(item)
            pset.append(new_set)

    return pset

def permutations(prefix, items):
    if not items:
        print(prefix)
        return

    for _ in items:
        current = items.pop(0)
        new_prefix = prefix.copy()
        new_prefix.append(current)
        permutations(new_prefix, items)
        items.append(current)
    return
    

    
