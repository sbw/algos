import unittest
import enumerations

class EnumTest(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_powerset(self):
        items = ['z', 'y', 'x']
        expected = [
            set(),
            {'x'},
            {'y'},
            {'x', 'y'},
            {'z'},
            {'x', 'z'},
            {'y', 'z'},
            {'x', 'y', 'z'}
        ]
        powerset = [set()]
        enumerations.powerset(items, powerset)
        for s in powerset:
            print(s)

    def test_powerset_iter(self):
        items = ['z', 'y', 'x']
        expected = [
            set(),
            {'x'},
            {'y'},
            {'x', 'y'},
            {'z'},
            {'x', 'z'},
            {'y', 'z'},
            {'x', 'y', 'z'}
        ]
        powerset = enumerations.powerset_iter(items)
        for s in powerset:
            print(s)

    def test_permutations(self):
        items = ['a', 'b', 'c', 'd']
        enumerations.permutations([], items)

        
if __name__ == '__main__':
    unittest.main()    
