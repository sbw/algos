#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description = 
            'Check if two strings are anagrams')
parser.add_argument('first', metavar = 'a', type = str)
parser.add_argument('second', metavar = 'b', type = str)
args = parser.parse_args()

print('strings to compare first:{} second: {}'.format(args.first, args.second))

def compareHistograms(histoA, histoB):
    for k, item in enumerate(histoA):
        print('a:{} b:{}'.format(item, histoB[k]))
        if item != histoB[k]:
            return False
    return True

def createHistogram(a):
    min = 0
    max = ord('z') - ord('a')

    histo = [0] * (max - min + 1)

    for c in a:
        i = ord(c) - ord('a')
        if min <= i <= max:
            histo[i] += 1

    return histo

histoA = createHistogram(args.first)
histoB = createHistogram(args.second)

print(histoA)
print(histoB)

result = compareHistograms(histoA, histoB)

if result == True:
    print('Strings are anagrams')
else:
    print('Strings are not anagrams')
