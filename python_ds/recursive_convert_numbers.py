#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(description = 'Convert integer to base x')
parser.add_argument('n', type = int)
parser.add_argument('base', type = int)
args = parser.parse_args()

def convert(n, base):
    digits = '0123456789ABCDEF'

    if n < base:
        return digits[n]

    return convert(n // base, base) + digits[n % base]

print(convert(args.n, args.base))
