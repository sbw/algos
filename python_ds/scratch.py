#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description = 'Sum numbers from 1 to n')
parser.add_argument('integer', metavar = 'n', type = int)
args = parser.parse_args()

def sum_to_n(n):
    sum = 0
    for i in range(1, n + 1):
        sum += i
    return sum

print(sum_to_n(args.integer))

