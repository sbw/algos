#!/usr/bin/env python3

def selectionSort(arr):
    upper = len(arr) - 1
    
    while upper > 0:
        
        m = 0
        for i in range(upper):
            if arr[i] > arr[m]:
                m = i
                
        temp = arr[m]
        arr[m] = arr[upper]
        arr[upper] = temp
         
        upper -= 1
        print(arr)
         
    return arr   

def main():
    arr = [26, 54, 93, 17, 77, 31, 44, 55, 20]

    print(arr)
    print(selectionSort(arr[:]))


#----Invoke program----
main()
