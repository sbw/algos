#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(description = 'Iterative binary search')
parser.add_argument('search', type = int)
args = parser.parse_args()

def binarySearch(arr, value):
    lower = 0
    upper = len(arr) - 1

    while lower <= upper:
        midpoint = lower + (upper - lower) // 2
        if arr[midpoint] == value:
            return True

        if value < arr[midpoint]:
            upper = midpoint - 1
        else:
            lower = midpoint + 1

    return False

def main():
    
    print('Searching for: {}'.format(args.search))
    sortedArray = [17, 20, 26, 31, 44, 54, 55, 65, 77, 93]

    result = binarySearch(sortedArray, args.search)

    if result == True:
        print('Found!')
    else:
        print('Not found')

#
main()

