#

class Deque():
    def __init__(self):
        self.items = []

    def addFront(self, item):
        self.items.append(item)

    def removeFront(self):
        return self.items.pop()

    def addRear(self, item):
        self.items.insert(0, item)

    def removeRear(self):
        return self.items.pop(0)

    def isEmpty(self):
        return self.items == []

    def size(self):
        return len(self.items)


if __name__ == '__main__':
    
    alpha = "abcdefghijk"

    myDeque = Deque()

    for c in alpha:
        myDeque.addFront(c)

    while not myDeque.isEmpty():
        print(myDeque.removeFront())

    for c in alpha:
        myDeque.addRear(c)

    while not myDeque.isEmpty():
        print(myDeque.removeFront())

