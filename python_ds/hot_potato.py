#!/usr/bin/env python3

import argparse
import queue

parser = argparse.ArgumentParser(description = 'Hot Potato')
parser.add_argument('step', type = int)
parser.add_argument('number', type = int)

args = parser.parse_args()

circle = [ x for x in range(1, args.number + 1) ]
print(circle)


def hotPotato(n, circle):
    myQueue = queue.Queue()

    for k in circle:
        myQueue.enqueue(k)

    while myQueue.size() > 1:
        for i in range(1, n):
            myQueue.enqueue(myQueue.dequeue())
        print('Removing: {}'.format(myQueue.dequeue()))

    return myQueue.dequeue()

print('Last man standing: {}'.format(hotPotato(args.step, circle)))
