#!/usr/bin/env python3

import argparse
import stack

parser = argparse.ArgumentParser(description = 'Convert from decimal to base x')
parser.add_argument('integer', type = int)
parser.add_argument('base', type = int)
args = parser.parse_args()

def convert(n, base):
    digits = "0123456789ABCDEF"
    mystack = stack.Stack()

    while n > 0:
        remainder = n % base
        n //= base
        mystack.push(remainder)

    result = ''
    while not mystack.isEmpty():
        result += digits[mystack.pop()]

    return result


print(convert(args.integer, args.base))
