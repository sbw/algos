#!/usr/bin/env python3

import argparse
import stack

parser = argparse.ArgumentParser(description = 'balanced parenthesis')
parser.add_argument('input_string', metavar = 'a', type = str)
args = parser.parse_args()

print(args.input_string) 

stack = stack.Stack()

isBalanced = True

for c in args.input_string:
    if c == '(':
        stack.push(c)
    elif c == ')':
        if stack.isEmpty():
            isBalanced = False
            break
        else:
            stack.pop()        

if isBalanced == True and stack.isEmpty():
    print('Balanced')
else:
    print ('Not balanced')

