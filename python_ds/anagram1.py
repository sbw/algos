#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description = 
            'Check if two strings are anagrams')
parser.add_argument('first', metavar = 'a', type = str)
parser.add_argument('second', metavar = 'b', type = str)
args = parser.parse_args()

print('strings to compare first:{} second: {}'.format(args.first, args.second))

def createHistogram(a):
    histo = {}
    for key in a:
        if key in histo:
            histo[key] += 1
        else:
            histo[key] = 1
    return histo

def compareDictionaries(dictA, dictB):
    keysA = sorted(dictA)
    keysB = sorted(dictB)

    if len(keysA) != len(keysB):
        return False

    for k in keysA:
        if k not in dictB:
            return False
        if dictA[k] != dictB[k]:
            return False

    return True


dictA = createHistogram(args.first)
dictB = createHistogram(args.second)

result = compareDictionaries(dictA, dictB)

if result == True:
    print('Strings are anagrams')
else:
    print('Strings are not anagrams')
