#!/usr/bin/env python3

def mergesort(arr):
    if len(arr) < 2:
        return

    mid = len(arr) // 2
    left = arr[:mid]
    right = arr[mid:]

    mergesort(left)
    mergesort(right)
    print(left)
    print(right)
    merge(left, right, arr)
    print(arr)
    print()


def merge(a, b, combo):
    i = len(a) - 1
    j = len(b) - 1
    k = i + j + 1

    while k >= 0:
        if i == -1:
            combo[k] = b[j]
            j -= 1
        elif j == -1:
            combo[k] = a[i]
            i -= 1
        elif a[i] > b[j]:
            combo[k] = a[i]
            i -= 1
        else:
            combo[k] = b[j]
            j -= 1
        k -= 1

def main():
    arr = [54, 26, 93, 17, 77, 31, 44, 55, 20]
    mergesort(arr)

    print('final')
    print(arr)

main()
