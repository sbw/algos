#
from collections import deque
from graph import *

def bfs(start, finish = None):
    result = []

    end = finish.key if finish else 'None'
    print('-------- Start: {} Finish: {} --------'.format(start.key, end))

    queue = deque()
    start.setColor(Color.gray)
    queue.append(start)
    print('Pushing into queue, coloring gray: {} '.format(start.key))

    while len(queue) != 0:
        current = queue[0]
        print('Looking at front of queue: {} '.format(current.key))

        #Only used for searching
        if finish is not None and current.key == finish.key:
            while current is not None:
                result.append(current.key)
                current = current.getPredecessor()
            result.reverse()
            return result
     
        print('\tPushing into queue, coloring gray: ', end = '')
        for neighbor in current.getNeighbors().values():
            if neighbor.getColor() == Color.white:
                print(' {} '.format(neighbor.key), end = '')
                neighbor.setColor(Color.gray)
                neighbor.setDistance(current.distance + 1)
                neighbor.setPredecessor(current)
                queue.append(neighbor)
        print()

        print('Dequeuing: {}, coloring black'.format(current.key))
        front = queue.popleft()
        front.setColor(Color.black)
    print()


if __name__ == '__main__':
    print('Testing bfs with graph:')
    print('                       ')
    print('         A   G         ')
    print('        / \ / \        ')
    print('       B   C   |       ')
    print('        \ /    |       ')
    print('         D     |       ')
    print('          \    |       ')
    print('           E   |       ')
    print('            \ /        ')
    print('             F         ')
    
    nodes = 'ABCDEFG'
    g = Graph()
    for node in nodes:
        g.addVertex(node)
    g.addEdge('A', 'B')
    g.addEdge('A', 'C')
    g.addEdge('B', 'D')
    g.addEdge('C', 'D')
    g.addEdge('C', 'G')
    g.addEdge('D', 'E')
    g.addEdge('E', 'F')
    g.addEdge('F', 'G')

    g.display()

    print(bfs(g.getVertex('A'), g.getVertex('F'))) 
    g.resetVertices()    
    print(bfs(g.getVertex('A')))
    g.resetVertices()
    print(bfs(g.getVertex('A'), g.getVertex('E')))      

