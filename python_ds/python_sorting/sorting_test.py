import unittest
import sorting


class SortingTest(unittest.TestCase):

    def setUp(self):
        self.udata = [45, 4, -16, 32, -9, -7, 9, 3, 5, 2] 
        self.sdata = sorted(self.udata)

    def tearDown(self):
        pass

    def test_merge1(self):
        a = [1, 2, 3, 4]
        b = [5, 6, 7, 8]
        c = [1, 2, 3, 4, 5, 6, 7, 8]
        combo = sorting.merge(a, b)
        self.assertEqual(combo, c)

    def test_merge2(self):
        b = [1, 2, 3, 4]
        a = [5, 6, 7, 8]
        c = [1, 2, 3, 4, 5, 6, 7, 8]
        combo = sorting.merge(a, b)
        self.assertEqual(combo, c)

    def test_merge3(self):
        a = [1, 3, 5, 7]
        b = [2, 4, 6, 8]
        c = [1, 2, 3, 4, 5, 6, 7, 8]
        combo = sorting.merge(a, b)
        self.assertEqual(combo, c)

    def test_mergesort(self):
        ans = sorting.mergesort(self.udata)
        self.assertEqual(ans, self.sdata) 
        
    def test_partition(self):
        pass

    def test_quicksort(self):
        sorting.quicksort(self.udata, 0, len(self.udata)-1)
        self.assertEqual(self.sdata, self.udata) 


if __name__ == '__main__':
    unittest.main()    
