def merge(a, b):  
    i = len(a)-1
    j = len(b)-1
    
    combo = [None] * (i+j+2)

    while i >= 0 and j >= 0:
        if a[i] < b[j]:
            combo[i+j+1] = b[j]
            j = j-1
        else:
            combo[i+j+1] = a[i]
            i = i-1

    while i >= 0:
        combo[i+j+1] = a[i]
        i = i-1

    while j >= 0:
        combo[i+j+1] = b[j]
        j = j-1

    return combo

def mergesort(arr):
    if len(arr) <= 1:
        return arr
    mid = len(arr)//2
    a = mergesort(arr[:mid])
    b = mergesort(arr[mid:])
    return merge(a, b) 

def partition(arr, left, right):
    i = left
    j = left

    while i < right:
        if arr[i] <= arr[right]:
            if i != j:
                arr[i], arr[j] = arr[j], arr[i]
            j = j+1
        i = i+1
    
    arr[right], arr[j] = arr[j], arr[right]

    return j

def quicksort(arr, left, right):
    if left >= right:
        return
    pivot = partition(arr, left, right)
    quicksort(arr, left, pivot-1)
    quicksort(arr, pivot+1, right)

    return

