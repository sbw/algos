# 

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

# Ordinary Tree
def insertLeft(root, data):
    if root.left == None:
        root.left = Node(data)
    else:
        temp = Node(data)
        temp.left = root.left
        root.left = temp

def insertRight(root, data):
    if root.right == None:
        root.right = Node(data)
    else:
        temp = Node(data)
        temp.right = root.right
        root.right = temp

# Binary Search Tree
def insert(root, value):
    if root == None:
        return Node(value)

    if value < root.data:
        root.left = insert(root.left, value)
    else:
        root.right = insert(root.right, value)

    return root

def find(root, value):
    if root == None:
        return None
    if root.data == value:
        return value
    if value < root.data:
        return find(root.left, value)
    return find(root.right, value)

# Traversal functions
def preorder(root):
    if root == None:
        return

    print('{} '.format(root.data), end = '')
    preorder(root.left)
    preorder(root.right)

def inorder(root):
    if root == None:
        return

    inorder(root.left)
    print('{} '.format(root.data), end = '')
    inorder(root.right)

def postorder(root):
    if root == None:
        return

    postorder(root.left)
    postorder(root.right)
    print('{} '.format(root.data), end = '')

if __name__ == '__main__':

    print('Testing Generic Binary Tree ')
    print('            a               ')
    print('           / \              ')
    print('          /   \             ')
    print('         b    c             ')
    print('          \  / \            ')
    print('          d e   f           ')

    root = Node('a')
    insertLeft(root, 'b')
    insertRight(root, 'c')
    insertRight(root.left, 'd')
    insertLeft(root.right, 'e')
    insertRight(root.right, 'f')
    
    print('Preorder:')
    preorder(root)
    print()
    
    print('Inorder:')
    inorder(root)
    print()

    print('postorder:')
    postorder(root)
    print()

    print('Testing Binary Search  Tree ')
    print('           70               ')
    print('           / \              ')
    print('          /   \             ')
    print('         31   93            ')
    print('        /    / \            ')
    print('       14   73  94          ')
    print('        \                   ')
    print('        23                  ')

    arr2 = [70, 31, 93, 94, 14, 23, 0, 73]

    root = None
    for n in arr2:
        root = insert(root, n)
    
    print('Inorder:')
    inorder(root)
    print()

    print(find(root, 0))
    print(find(root, 94))
    print(find(root, 33))
    print(find(root, 70))
    print(find(root, 31))
    print(find(root, 87))



