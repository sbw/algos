#

class Queue:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        self.items.append(item)

    def dequeue(self):
        return self.items.pop(0)

    def isEmpty(self):
        self.items == []

    def size(self):
        return len(self.items)

if __name__ == '__main__':
    
    print('Testing enqueue, dequeue, isEmpty')

    alpha = "abcdefg"
    queue = Queue()

    for c in alpha:
        queue.enqueue(c)

    while not queue.isEmpty():
        print(queue.dequeue())


