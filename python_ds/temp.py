#!/usr/bin/env python3

def dpMakeChange(coinValueList,change,minCoins,coinsUsed):
   for cents in range(change+1):
      coinCount = cents
      newCoin = 1
      for j in [c for c in coinValueList if c <= cents]:
          if minCoins[cents-j] + 1 < coinCount:
               coinCount = minCoins[cents-j]+1
               newCoin = j
      minCoins[cents] = coinCount
      coinsUsed[cents] = newCoin
   return minCoins[change]

def printCoins(coinsUsed,change):
   coin = change
   while coin > 0:
      thisCoin = coinsUsed[coin]
      print(thisCoin)
      coin = coin - thisCoin

def main():
    amnt = 63
    clist = [1,10,5,21,25]
    coinsUsed = [0]*(amnt+1)
    coinCount = [0]*(amnt+1)

    print("Making change for",amnt,"requires")
    print(dpMakeChange(clist,amnt,coinCount,coinsUsed),"coins")
    print("They are:")
    printCoins(coinsUsed,amnt)

    print('Minimum number of coins for an amount:')
    for i, n in enumerate(coinCount):
        print('{}:{}'.format(i, n), end = '\t')
        if (i + 1) % 10 == 0:
            print()
    print()
    
    print('Coins used')
    for i, n in enumerate(coinsUsed):
        print('{}:{}'.format(i, n), end='\t')
        if (i + 1) % 10 == 0:
            print()
    print()

main()
