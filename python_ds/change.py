#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(description = 'How to make change')
parser.add_argument('change', type = int)
args = parser.parse_args()

def findCoins(amount, cointypes, minimumCoins, lastCoinUsed):
    for change in range(1, amount + 1):
        numberOfCoins = change
        lastCoin = cointypes[0]

        for coin in cointypes:
            if coin <= change:
                if numberOfCoins > 1 + minimumCoins[change - coin]:
                    numberOfCoins = 1 + minimumCoins[change - coin]
                    lastCoin = coin
                
        minimumCoins[change] = numberOfCoins
        lastCoinUsed[change] = lastCoin

def printCoinsUsed(lastCoinUsed, change):
    
    while change > 0:
        coin = lastCoinUsed[change]
        change -= coin
        print(coin)    

def main():

    minimumCoins = []
    minimumCoins = [0] * (args.change + 1)
    lastCoinUsed = []
    lastCoinUsed = [0] * (args.change + 1)

    findCoins(args.change, [1, 10, 5, 21, 25], minimumCoins, lastCoinUsed)

    for i, minimum in enumerate(minimumCoins):
        print('{}:{}'.format(i, minimum), end = '\t')
        if (i + 1) % 10 == 0:
            print()
    print()

    
    for i, coin in enumerate(lastCoinUsed):
        print('{}:{}'.format(i, coin), end = '\t')
        if (i + 1) % 10 == 0:
            print()
    print()

    print('Change is {}'.format(args.change))
    printCoinsUsed(lastCoinUsed, args.change)


# And away we go
main()
