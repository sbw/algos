#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(description = 'Iterative binary search')
parser.add_argument('search', type = int)
args = parser.parse_args()

def binarySearch(arr, value, lower, upper):
    if lower > upper:
        return False
    
    midpoint = lower + (upper - lower) // 2
    if value == arr[midpoint]:
        return True

    if value < arr[midpoint]:
        binarySearch(arr, value, lower, midpoint - 1)
    else:
        binarySearch(arr, value, midpoint + 1, upper)


def main():
    
    print('Searching for: {}'.format(args.search))
    sortedArray = [17, 20, 26, 31, 44, 54, 55, 65, 77, 93]

    if binarySearch(sortedArray, args.search, 0, len(sortedArray) - 1)
        print('Found!')
    else:
        print('Not found')

#
main()

