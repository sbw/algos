#!/usr/bin/env python3

import argparse
import deque

parser = argparse.ArgumentParser(description = 'Palindrome Checker')
parser.add_argument('string', type = str)
args = parser.parse_args()

def isPalindrome(string):
    myDeque = deque.Deque()
    
    for c in string:
        myDeque.addRear(c)

    while myDeque.size() > 1:
        if myDeque.removeFront() != myDeque.removeRear(): 
            return False

    return True

result = isPalindrome(args.string)

if result == True:
    print('String is a palindrome')
else:
    print('String is not a palindrome')

