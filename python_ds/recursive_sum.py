#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description = 'Sum to N')
parser.add_argument('number', type = int)
args = parser.parse_args()

def sumToN(n):
    if n == 0:
        return 0
    
    return n + sumToN(n - 1)

print(sumToN(args.number))
