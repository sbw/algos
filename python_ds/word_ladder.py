#!/usr/bin/env python3

import argparse
from graph import *
from bfs import *

parser = argparse.ArgumentParser(description = 'Word Ladder Graph')
parser.add_argument('filename', type = str)
parser.add_argument('start', type = str)
parser.add_argument('finish', type = str)
args = parser.parse_args()

def buildWordBuckets(filename):
    buckets = {}

    wordfile = open(filename, 'r')
    for word in wordfile:
        word = word[:-1]
        for i in range(len(word)):
            w = list(word)
            w[i] = '_'
            key = ''.join(w)
            if key in buckets:
                buckets[key].append(word)
            else:
                buckets[key] = [word]

    wordfile.close()
    return buckets

def createWordGraph(wordBuckets):
    g = Graph()
    for bucket in wordBuckets.keys():
        for word1 in wordBuckets[bucket]:
            for word2 in wordBuckets[bucket]:
                if (word1 != word2):
                    g.addEdge(word1, word2)
    return g

def main():
    buckets = buildWordBuckets(args.filename)
    wordGraph = createWordGraph(buckets)
    
    wordGraph.display()
    start = wordGraph.getVertex(args.start)
    finish = wordGraph.getVertex(args.finish)
    
    bfs(start, finish)   
                


#----Start----
main()
