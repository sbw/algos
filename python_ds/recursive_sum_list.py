#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description = 'Sum list of numbers')
parser.add_argument('numbers', type = int, nargs = '+')
args = parser.parse_args()

def sumList(L):
    if len(L) == 1:
        return L[0]

    n = L.pop()
    return n + sumList(L)

print(sumList(args.numbers))

