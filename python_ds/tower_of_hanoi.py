#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(description = 'Tower of Hanoi')
parser.add_argument('n', type = int)
args = parser.parse_args()

def moveDisk(a, b):
    print('moving disk from pole {} to pole {}'.format(a, b))

def towerOfHanoi(n, orig, inter, final):
    if n == 1:
        return moveDisk(orig, final)

    towerOfHanoi(n - 1, orig, final, inter)
    towerOfHanoi(1, orig, inter, final)
    towerOfHanoi(n - 1, inter, orig, final)


towerOfHanoi(3, 'A', 'B', 'C')
