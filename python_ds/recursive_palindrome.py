#!/usr/bin/env python3

import argparse
parser = argparse.ArgumentParser(
    description = 'Determines if string is palindrome')
parser.add_argument('string', type = str)
args = parser.parse_args()

def isPalindrome(left, right, string):
    if left > right:
        return True
    if string[left] != string[right]:
        return False

    return isPalindrome(left + 1, right - 1, string)


def isPalindrome2(string):
    if len(string) < 2:
        return True
    if string[0] != string[-1]:
        return False

    return isPalindrome2(string[1:-1])

def showResult(result):
    if result == True:
        print('String is a palindrome')
    else:
        print('String is not a palindrome')



result = isPalindrome(0, len(args.string) - 1, args.string)
showResult(result)

result = isPalindrome2(args.string)
showResult(result)
