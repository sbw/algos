#include "basics.h"
#include "gtest/gtest.h"

namespace {

class BasicsTest : public ::testing::Test {
protected:
    BasicsTest() {}
    ~BasicsTest() {}

    void SetUp() override {}
    void TearDown() override {}
};

TEST_F(BasicsTest, StringToHex)
{
    std::string input = "abcabc";
    //std::string input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    //const char* ex[] = {"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"};
    std::string ans = hex_to_base64(input);
    std::cout << ans;

}

} // end anonymous namespace

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
