#include "basics.h"
#include <iostream>

const char base64set[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

u_char
hex_table(u_char n) {
    u_char real_n = n - '0';
    if (real_n > 9) {
        real_n -= 'A' - '0';
        if (real_n > 6) {
            real_n -= 'a' - 'A';
        }
        real_n += 10;
    }

    return real_n;
}

std::vector<u_char>
string_to_hex(const std::string& input)
{
    // range on input size?
    size_t len = input.size();
    size_t in_index = 0, out_index = 0;
    std::vector<u_char> hex_array((len+1)/2);

    // handle odd len hex number here
    if (len % 2 != 0) {
        hex_array[0] = input[0] - '0';
        in_index++;
        out_index++;
    }

    for (; out_index < len; in_index+=2,out_index+=1) {
        hex_array[out_index] = hex_table(input[in_index]) << 4;
        hex_array[out_index] |= hex_table(input[in_index+1]);
    }

    return hex_array;
}

std::string
hex_to_base64(const std::string& input)
{
    std::string output;
    std::vector<u_char> hex = string_to_hex(input);

    size_t i = 0;
    while (true) {
        if (i+3 > hex.size()) break;
        u_int32_t n = 0;
        n = (u_int32_t) hex[i+2];
        n |= (u_int32_t) hex[i+1] << 8;
        n |= (u_int32_t) hex[i] << 16;

        u_int32_t mask = 0x3f000000;

        for (int k = 0; k < 4; ++k) {
            mask >>= 6;
            u_int32_t temp = n & mask;
            temp >>= 6 * (3-k);
            output.push_back(base64set[temp]);
        }

        i += 3;
    }

    /*
    if (i != hex.size()) {
        if (i + 2 == hex.size()) {

        }
    }
    */

    return output;
}
