#pragma once

#include <vector>
#include <string>

std::vector<u_char> string_to_hex(const std::string&);
std::string hex_to_base64(const std::string&);
